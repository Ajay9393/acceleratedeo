import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
import os
import sys
import re
sys.path.append('/scratch/e/esargent/ajay15/')
import DataAnalysis as aj
from sklearn.model_selection import train_test_split
from skorch import NeuralNetClassifier, NeuralNet
from model_definitions import ConstantNet, HalvedNet
from skorch.callbacks import Callback
from sklearn.model_selection import GridSearchCV
import pickle
def tweet(msg):
    print( "~"*60)
    print("msg")
    print ("~"*60)



class AccuracyTweet(Callback):
    pass







x = np.load('/scratch/e/esargent/ajay15/MachineLearning/Robotic_Perovskite/Augmented_Data_With_Exp_Theta/features_augmented.npy')
x = x.astype('float')
targets = np.load('/scratch/e/esargent/ajay15/MachineLearning/Robotic_Perovskite/Augmented_Data_With_Exp_Theta/targets_augmented.npy')
##change targets to be (256,) dimension vector with 1's in place of an integer from 1 to 256
targets = targets.astype('int')
##not needed for CrossEntropyLoss--> expects a 1D vector size of batch with value b/w 0 and 255

'''y = np.zeros((targets.size, 256))
y[np.arange(targets.size), targets - 1] = 1 
y = y.astype('float')
'''
params = {
        'lr':[1e-1,1e-2,1e-3,1e-4],
        'max_epochs':[100,500,1000],
        'module__num_layers':[2,3,4,5,6,7],
        }


if __name__ == '__main__':   
         
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print (device)
    x = torch.from_numpy(x).float()
    y = torch.from_numpy(targets).long()
    print (x.shape, y.shape)

    print (y)

    net = NeuralNetClassifier(
            ConstantNet,
            device = device,
            iterator_train__shuffle = True,
            module__D_in = x.shape[1],
            module__D_out = 255,
            module__H = x.shape[1],
            module__device = device,
            #criterion = torch.nn.CrossEntropyLoss,
            )

    gs = GridSearchCV(net, params, refit = False, scoring = 'accuracy')

    gs.fit(x,y)
    with open('best_fit_constant_longer_time.pkl','wb') as f:
        pickle.dump(gs, f)
 





