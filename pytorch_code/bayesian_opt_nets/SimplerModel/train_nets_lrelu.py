import numpy as np
import torch
import pandas as pd
import os
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader, Dataset
from train_nets import TwoChannelConv
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print (device)




Y = np.load('../augmented_targets_normal2.npy')
Y = Y-1.

x = np.load('../augmented_features_normal2.npy')

try:
    mag = np.load('../augmented_magnitude_normal2.npy')
except FileNotFoundError:
    mag = np.fft.fft(x, axis = 1)
    mag = np.abs(mag)
    np.save('../augmented_magnitude_normal2.npy', mag)


X = np.zeros([x.shape[0], 2, x.shape[-1]])
X[:,0,:] = x
X[:,1,:] = mag


x_train, x_val, y_train, y_val = train_test_split(X,Y, random_state = 42, test_size = 0.2)
x_val, x_test, y_val, y_test = train_test_split(x_val,y_val, random_state = 42, test_size = 0.1)


print (x_train.shape)
def train_model(model, optimizer, x_train_re, x_val_re, y_train, y_val):
    train_data = []
    for i in range(len(x_train_re)):
        train_data.append([x_train_re[i], y_train[i]])

    val_data = []

    for i in range(len(x_val_re)):
        val_data.append([x_val_re[i], y_val[i]])

    b_size = 32
    loss_fn = torch.nn.CrossEntropyLoss()
    loader = DataLoader(train_data, batch_size = b_size)
    val_loader = DataLoader(val_data, batch_size = b_size)
    loss_train = []
    loss_val = []
    record_step = 10
    for t in range(100+1):
        tmp_train_loss = []
        for batch in loader:
            x,y = batch
            x = x.float().to(device)
            y = y.long().to(device)

            y_pred = model(x)
            loss = loss_fn(y_pred,y)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            tmp_train_loss.append(loss.item())

        loss_train.append(np.average(tmp_train_loss))
        tmp_val_loss = []
        for batch in val_loader:
            x,y = batch
            x = x.float().to(device)
            y = y.long().to(device)
            y_pred = model(x)
            loss = loss_fn(y_pred,y)
            tmp_val_loss.append(loss.item())

        loss_val.append(np.average(tmp_val_loss))


        if t%record_step == 0:
            print ("At Epoch {0} the training loss is {1:.2e} and the validation loss is {2:.2e}".format(t, loss_train[t],loss_val[t]))

    return model, optimizer, loss_train, loss_val

dict_map = {'ReLU':torch.nn.ReLU, 'LReLU':torch.nn.LeakyReLU, 'Tanh':torch.nn.Tanh}
conv_acts = ['LReLU', 'ReLU','Tanh']
kernel_size = [[50,25,25], [50,50,25], [25,25,25]]
lin_acts = ['LReLU', 'ReLU','Tanh']
lr = [1e-4, 1e-3, 1e-2]
weight_decay = [1e-3, 1e-2]
dropout = [0.3,0.5]

params = {
        'conv_act':conv_acts,
        'lin_act': lin_acts,
        'k_size': kernel_size,
        'lr':lr,
        'weight_decay':weight_decay,
        'dropout':dropout,
        }


save_dir = 'trained_nets/'
import itertools
for i, combo in enumerate(itertools.product(*params.values())):
    conv_act = combo[0]; lin_act = combo[1]; kernel_size = combo[2]; lr = combo[3]; weight_decay = combo[4]; dropout = combo[5]
    k_size = [str(x) for x in kernel_size]

    print ("Trying a new net with {0} conv act, {1} lin act, a learning rate of {2:.2e}, a weight_decay of {3:.2e} a dropout of {4:.1f} with kernels of {5} ".format(conv_act,lin_act,lr,weight_decay,dropout, '-'.join(k_size)))
    model =TwoChannelConv({'conv_activate':dict_map[conv_act], 'lin_activate':dict_map[lin_act], 'kernels':kernel_size}, device, dropout)
    model.cuda()
    optimizer = torch.optim.Adam(model.parameters(), lr = lr, weight_decay = weight_decay)

    model_trained, opt_trained, loss_train, loss_val = train_model(model, optimizer, x_train, x_val, y_train, y_val)

    torch.save({'epoch':100,
        'model_state_dict':model_trained.state_dict(),
        'optimizer_state_dict':opt_trained.state_dict(),
        'loss':loss_train[-1],
        },
        save_dir +'{0}_convact_{1}_linact_lr_{2:.2e}_weight_decay_{3:.2e}_dropout_{4:.1f}_ksize_{5}'.format(conv_act,lin_act,lr,weight_decay,dropout,'-'.join(k_size)))
