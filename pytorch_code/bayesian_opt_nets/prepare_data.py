from pymatgen.ext.matproj import MPRester
from pymatgen.symmetry import analyzer
from pymatgen.analysis.diffraction import xrd
from ase import Atoms
from ase.utils import xrdebye
from pymatgen.io.ase import AseAtomsAdaptor
from matplotlib import pyplot as plt
import numpy as np
import sys
sys.path.append('../../')
import DataAnalysis as aj
import pandas as pd
import pickle

def guassian(x, a, b, c):
    return a*np.exp(-(x - b)**2./(2.*c**2.))


def create_vector(peak_int,peak_pos,two_theta):
    intens = 0
    for i, peak in enumerate(peak_int):
        if peak_pos[i] > 37:
            pass
        else:
            intens+= guassian(two_theta, peak, peak_pos[i], 0.11)
    return intens


def prepare_data(df, two_theta):

    intens = np.zeros(len(two_theta))

    targets = np.zeros(len(df))
    features = np.zeros([len(df),len(two_theta)])
    counter = 0

    for i,row in df.iterrows():
        intensity = row['intensity']
        thetas = row['two_theta']
        targets[counter] = row['spacegroup']['number']
        features[counter,:] = create_vector(intensity, thetas, two_theta)
        counter+=1
        print(i)

    return targets, features



if __name__=='__main__':
    
    df = pickle.load(open('All_data.pkl','rb'))
    two_theta = np.linspace(10, 180, 10000)
    
    targets, features = prepare_data(df, two_theta)


    np.save('pytorch_code/bayesian_opt_nets/augmented_targets_normal2.npy', targets)
    np.save('pytorch_code/bayesian_opt_nets/augmented_features_normal2.npy',features)





