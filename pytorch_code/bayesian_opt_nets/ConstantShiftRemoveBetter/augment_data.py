from matplotlib import pyplot as plt
import numpy as np
import sys
sys.path.append('../../../')
import DataAnalysis as aj
import pandas as pd
import pickle
import prepare_data
import random




def scale_data(df, column):
    df_temp = df.copy(deep = False)
    for i, row in df_temp.iterrows():
        vect = np.random.rand(len(row[column]))
        row[column] = row[column]*vect      
    return df_temp




def shift_data(df, column, shift):
    df_temp = df.copy(deep = False)
    for i, row in df_temp.iterrows():
        #vect = np.random.uniform(low = -shift, high = shift, size = len(row[column]))
        shift = np.random.uniform(low = shift, high = shift)
        row[column] = row[column] + shift      
    return df_temp


def remove_peaks(df, column1, column2):
    df_temp = df.copy(deep = False)
    for i,row in df_temp.iterrows():
        delete = np.random.choice([0,1],p=[0.9,0.1], size = len(row[column1]))
        row[column1] = row[column1][delete==1]
        row[column2] = row[column2][delete==1]

    return df_temp






if __name__=='__main__':
    with open('../All_data.pkl','rb') as f:
        df = pickle.load(f)

    two_theta =  np.linspace(3, 90, 2500)

    df_scaled = scale_data(df, 'intensity')
    df_shifted = shift_data(df, 'two_theta', 0.1)
    df_removed = remove_peaks(df, 'intensity','two_theta')

    df_temp = df.append(df_scaled)
    df_temp2 = df_temp.append(df_shifted)
    idf_compiled = df_temp2.append(df_removed)

    df_temp2.to_pickle('All_data_augmented.pkl')

    targets, features = prepare_data.prepare_data(df_temp2, two_theta)

    np.save('augmented_targets_betterinput.npy',targets)
    np.save('augmented_features_betterinput.npy', features)




