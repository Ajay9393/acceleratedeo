import torch
import numpy as np
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
import os
import re
from train_nets_simple import TwoChannelConv
import pandas as pd
from torch.utils.data import DataLoader, Dataset
from sklearn.metrics import accuracy_score, f1_score
from sklearn.model_selection import train_test_split 
import sys


    
dict_map = {'ReLU':torch.nn.ReLU, 'LReLU':torch.nn.LeakyReLU, 'Tanh':torch.nn.Tanh}
def extract_params_from_filename(filename):
    conv_act = re.findall(r'(.+)_convact', filename)[0]
    lin_act = re.findall(r'convact_(.+)_linact', filename)[0]
    lr = re.findall(r'lr_(.+)_weight_decay', filename)[0]
    weight_decay = re.findall(r'weight_decay_(.+)_dropout', filename)[0]
    dropout = re.findall(r'dropout_(.+)_ksize', filename)[0]
    ksize = re.findall(r'ksize_(.+)', filename)[0]
    ksize = ksize.split('-')
    k_1=ksize[0][1:-1]
    k_2 = ksize[1][1:-1]
    k_1=[int(x) for x in k_1.split(',')]
    k_2=[int(x) for x in k_2.split(',')]
    return conv_act, lin_act, float(lr), float(weight_decay), float(dropout), (k_1,k_2)

def test_net(model, optimizer, val_data, y_val):
    pred_data = np.zeros((len(y_val), 230)) 
    b_size = 128
    val_loader = DataLoader(val_data, batch_size = b_size)
    tmp_val_loss = []
    loss_fn = torch.nn.CrossEntropyLoss()

    for i,batch in enumerate(val_loader):
        x,y = batch
        x = x.float().to(device)
        y = y.long().to(device)
        y_pred = model(x)
        loss = loss_fn(y_pred,y)
        tmp_val_loss.append(loss.item())
        pred_data[i*b_size:i*b_size + len(y_pred),:] = y_pred.cpu().detach().numpy()
    softmax = torch.nn.Softmax(dim=1)
    pred_data = softmax(torch.from_numpy(pred_data)).cpu().detach().numpy()
    pred_data = np.argmax(pred_data, axis = 1)
    return np.average(tmp_val_loss), pred_data


if __name__ == '__main__':
    sd = sys.argv[1]
    filepaths = os.listdir(sd)
    columns = ['Conv Act','Lin Act', 'lr','weight_decay','dropout','train_loss','val_loss','train_accuracy','val_accuracy']
    results_df = pd.DataFrame( np.zeros((len(filepaths),len(columns))), columns = columns)

    Y = np.load('augmented_targets_betterinput.npy')
    Y = Y-1.

    x = np.load('augmented_features_betterinput.npy')

    try:
        mag = np.load('augmented_magnitude_betterinput.npy')
    except FileNotFoundError:
        mag = np.fft.fft(x, axis = 1)
        mag = np.abs(mag)
        np.save('augmented_magnitude_betterinput.npy', mag)

    X = np.zeros([x.shape[0], 2, x.shape[-1]])
    X[:,0,:] = x
    X[:,1,:] = mag


    x_train, x_val, y_train, y_val = train_test_split(X,Y, random_state = 42, test_size = 0.2)
    x_val, x_test, y_val, y_test = train_test_split(x_val,y_val, random_state = 42, test_size = 0.1)
    val_data = []
    for i in range(len(x_val)):
            val_data.append([x_val[i], y_val[i]])
    
    for i,filename in enumerate(filepaths):
        print (filename)
        checkpoint = torch.load(sd + filename)
        conv_act, lin_act, lr, weight_decay, dropout, ksize = extract_params_from_filename(filename)
        model = TwoChannelConv({'conv_activate':dict_map[conv_act], 'lin_activate':dict_map[lin_act], 'kernels':ksize}, device, dropout)
        optimizer = torch.optim.Adam(model.parameters(), lr = lr, weight_decay = weight_decay)
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        model.cuda()
        model.eval()
        #val_loss, predictions = test_net(model, optimizer,val_data, y_val)
        #accuracy = accuracy_score(y_val, predictions)
        results_df.loc[i,:]  = [conv_act, lin_act, lr, weight_decay, dropout,\
                checkpoint['loss'][0][-1],checkpoint['loss'][1][-1],checkpoint['acc'][0][-1], checkpoint['acc'][1][-1]]
   
    results_df.sort_values(by = 'val_accuracy', axis = 0, ascending = False, inplace = True)
    results_df.to_csv(sd+'Results.csv')
    
        
    
