import torch
import numpy as np
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
import os
import re
from train_nets_simple_pool import TwoChannelConv
import pandas as pd
from torch.utils.data import DataLoader, Dataset
from sklearn.metrics import accuracy_score, f1_score
from sklearn.model_selection import train_test_split 
import sys


wd = '/project/e/esargent/ajay15/Robotic_Perovskite/pytorch_code/bayesian_opt_nets/BetterInput/'
   
dict_map = {'ReLU':torch.nn.ReLU, 'LReLU':torch.nn.LeakyReLU, 'Tanh':torch.nn.Tanh}
def extract_params_from_filename(filename):
    conv_act = re.findall(r'(.+)_convact', filename)[0]
    lin_act = re.findall(r'convact_(.+)_linact', filename)[0]
    lr = re.findall(r'lr_(.+)_weight_decay', filename)[0]
    weight_decay = re.findall(r'weight_decay_(.+)_dropout', filename)[0]
    dropout = re.findall(r'dropout_(.+)_ksize', filename)[0]
    ksize = re.findall(r'ksize_(.+)', filename)[0]
    
    return conv_act, lin_act, float(lr), float(weight_decay), float(dropout), [int(x) for x in ksize.split('-')]

def test_net(model, optimizer, val_data, y_val):
    model.eval()
    pred_data = np.zeros((len(y_val),1)).reshape(-1) 
    b_size = 128
    val_loader = DataLoader(val_data, batch_size = b_size)
    tmp_val_loss = []
    loss_fn = torch.nn.CrossEntropyLoss()

    for i,batch in enumerate(val_loader):
        x,y = batch
        x = x.float().to(device)
        y = y.long().to(device)
        y_pred = model(x)
        pred_data[i*b_size:i*b_size+ len(y_pred)] = y_pred.cpu().detach().numpy().reshape(-1)
    
    pred_data = 1/(1+np.exp(-pred_data))
    pred_data[pred_data>0.5] = 1
    pred_data[pred_data<0.5] = 0
    return  pred_data


if __name__ == '__main__':
    sd = sys.argv[1]
    filepaths = os.listdir(sd)
    columns = ['Conv Act','Lin Act', 'lr','weight_decay','dropout','ksize','train_loss','val_loss','train_accuracy','val_accuracy']
    results_df = pd.DataFrame( np.zeros((len(filepaths),len(columns))), columns = columns)

    x = np.load(wd+ 'augmented_features_betterinput.npy')
    targets = np.load(wd+'augmented_targets_betterinput.npy')
    noncentrosymm  = np.load(wd+'noncentrosymmetric.npy')

    Y = np.array([0 if sg in noncentrosymm else 1 for sg in targets]).reshape(-1,1)



    try:
        mag = np.load(wd+ 'augmented_magnitude_betterinput.npy')
    except FileNotFoundError:
        mag = np.fft.fft(x, axis = 1)
        mag = np.abs(mag)
        np.save('augmented_magnitude_betterinput.npy', mag)
    
    to_delete = []
    for i,arr in enumerate(x):
        if np.max(arr) == 0:
            to_delete.append(i)

    x = np.delete(x, to_delete, axis = 0)
    mag = np.delete(mag, to_delete, axis = 0)
    Y = np.delete(Y, to_delete)
    x = x/ np.amax(x, axis = 1).reshape(-1,1)
    mag = mag / np.amax(mag, axis = 1).reshape(-1,1)


    X = np.zeros([x.shape[0], 2, x.shape[-1]])
    X[:,0,:] = x
    X[:,1,:] = mag


    x_train, x_val, y_train, y_val = train_test_split(X,Y, random_state = 42, test_size = 0.2)
    x_val, x_test, y_val, y_test = train_test_split(x_val,y_val, random_state = 42, test_size = 0.1)
    val_data = []
    train_data = []
    for i in range(len(x_val)):
            val_data.append([x_val[i], y_val[i]])
    for i in range(len(x_train)):
            train_data.append([x_train[i], y_train[i]])
    

    for i,filename in enumerate(filepaths):
        print (filename)
        checkpoint = torch.load(sd + filename)
        conv_act, lin_act, lr, weight_decay, dropout, ksize = extract_params_from_filename(filename)
        model = TwoChannelConv({'conv_activate':dict_map[conv_act], 'lin_activate':dict_map[lin_act], 'kernels':ksize}, device, dropout)
        optimizer = torch.optim.Adam(model.parameters(), lr = lr, weight_decay = weight_decay)
    
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        model.cuda()
        model.eval()
        val_predictions = test_net(model, optimizer,val_data, y_val)
        train_predictions = test_net(model, optimizer, train_data, y_train)
        train_accuracy = accuracy_score(y_train, train_predictions)
        val_accuracy = accuracy_score(y_val, val_predictions)
        print ("The validation accuracy is {0}".format(val_accuracy))
        results_df.loc[i,:]  = [conv_act, lin_act, lr, weight_decay, dropout, '-'.join([str(x) for x in ksize]), checkpoint['loss'][0][-1],checkpoint['loss'][1][-1],train_accuracy,val_accuracy]

    results_df.sort_values(by = 'val_accuracy', axis = 0, ascending = False, inplace = True)
    results_df.to_csv(sd+'Results.csv')
    
        
    
