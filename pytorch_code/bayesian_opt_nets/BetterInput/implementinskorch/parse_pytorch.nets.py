import os
import re
import numpy as np
import torch
import skorch
import json
import sys
sys.path.append(r'/scratch/e/esargent/ajay15')
import DataAnalysis as aj
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams.update({'figure.autolayout':True})


nn_dir = 'trained_nets_2/'
save_up = False

list_of_all_nets = os.listdir(nn_dir)


ids = np.unique([re.findall(r'(.+_)\d+', x) for x in list_of_all_nets])

if ids.size == 0:
    ids = [r'\d+']

for id_ in ids:

    all_nets = [x for x in list_of_all_nets if re.findall(id_, x)]

    ##needed so that the constant_1 or constant_0 comes first as this one will be completed (to find initialization parameters)
    all_nets.sort()



    nets_to_remove = []
    last_train_loss = []
    last_val_loss = []
    last_val_acc = []


    with open(nn_dir+all_nets[0]+ r'/model_init_params.json','r') as f:
        params = json.load(f)
        params_names = list(params)



    params_dict = {}

    for name in params_names:
        params_dict[name] = []

    for net in all_nets:
        print (net)
        try:
            
            with open(nn_dir + net + r'/history.json','r') as f:
                history = json.load(f)
                history[-1]['valid_acc']
        except (FileNotFoundError, KeyError) as e:
            print('no file found - not complete!')
            nets_to_remove.append(net)
        
        else:
            print(history[-1].keys())
            last_train_loss.append(history[-1]['train_loss'])
            last_val_loss.append(history[-1]['valid_loss'])
            last_val_acc.append(history[-1]['valid_acc'])

            with open(nn_dir + net+ r'/model_init_params.json','r') as f:
                params = json.load(f)

            for key in list(params_dict):
                params_dict[key].append(params[key])



    for net in nets_to_remove:
        all_nets.remove(net)


    idx = np.argsort(last_val_acc)

    print (idx)

    last_train_loss = np.array(last_train_loss)[idx]
    last_val_loss = np.array(last_val_loss)[idx]
    last_val_acc = np.array(last_val_acc)[idx]
    all_nets = np.array(all_nets)[idx]

    for name in params_names:
        params_dict[name] = np.array(params_dict[name])[idx]


    labels = ['training_loss','validation_loss','validation_accuracy']
    var = [last_train_loss, last_val_loss, last_val_acc]

    for name in params_names:
        labels.append(name)
        var.append(params_dict[name])

    j = int(len(var)/2)

    f1, ax1 = plt.subplots(nrows =2 , ncols = j, figsize = [20,10])

    for i in range(0,2*j):
        ax1[int(i/j), int(i%j)].plot(var[i], 'r*', markersize = 16, linewidth = 4)
        aj.figure_quality_axes(ax1[int(i/j), int(i%j)], 'Trial',labels[i], '', legend = False)



    f2, ax2 = plt.subplots(nrows = 1, ncols = 1, figsize = [15,15])

    with open(nn_dir + all_nets[-1] + r'/history.json','r') as f:
        history = json.load(f)

    best_train_loss = [x['train_loss'] for x in history]
    best_val_loss = [x['valid_loss'] for x in history]



    ax2.plot(np.arange(len(best_train_loss)), best_train_loss, label = 'Training')
    ax2.plot(np.arange(len(best_train_loss)), best_val_loss, label = 'Validation')

    aj.figure_quality_axes(ax2, 'Epoch','Loss','Best Net: {}'.format(all_nets[-1]),legend = True)

    ax2.set_yscale('log')
    
    if save_up:

        f1.savefig(nn_dir + '../' + 'Training_Parameters_{}.png'.format(id_), format = 'png')
        f2.savefig(nn_dir + '../' +'Best_Net_History_{}.png'.format(id_), format = 'png')

    else:
        f1.savefig(nn_dir + 'Training_Parameters.png', format = 'png')
        f2.savefig(nn_dir + 'Best_Net_History.png', format = 'png')







