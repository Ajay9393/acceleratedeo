import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
import os
import sys
import re
sys.path.append('/scratch/e/esargent/ajay15/')
import DataAnalysis as aj
from sklearn.model_selection import train_test_split
from skorch import NeuralNetClassifier, NeuralNet
import torch.nn.functional as F

from torch.nn import  Conv1d, MaxPool1d, BatchNorm1d, Linear, Dropout
##attribute proxy is necessary so that you can add a dynamic number of layers each with a different weight
##it allows us to create add a list of modules: layer_1, layer_2... and rather than having to look at an index
##of a list (i.e [layer_1, layer_2]) it takes the name layer_1, and returns the attributes for layer_1, allowing
##layers to be created and referenced dynamically. This is because pytorch cannot use GPUs on lists
##taken from https://discuss.pytorch.org/t/list-of-nn-module-in-a-nn-module/219/3
class AttrProxy(object):
   """Translates idnex lookups into attribute lookups"""

   def __init__(self, module, prefix):
       self.module = module
       self.prefix = prefix


   def __getitem__(self,i):
       return getattr(self.module, self.prefix+str(i))


class HalvedNet(torch.nn.Module):
    def __init__(self, D_in, D_out,device = "cuda", act_fun = torch.nn.functional.sigmoid, dense_fun=torch.nn.functional.softmax):

        super(HalvedNet,self).__init__()

        self.device = device

        self.act_fun = act_fun
        self.dense_fun = dense_fun

        self.D_out = D_out
        self.D_in = D_in

        if D_out > 10:
            self.number_of_layers = int(-1*np.log2(D_out/D_in))
        else:
            self.number_of_layers = int(-1*np.log2(10/D_in))

        self.first_layer = torch.nn.Linear(int(D_in), int(D_in/2))

        for i in range(0,self.number_of_layers):
            self.add_module('half2half_' + str(i),torch.nn.Linear(int(D_in/(2**(i+1))), int(D_in/(2**(i+2)))))

        self.second_last_layer = torch.nn.Linear(int(D_in/2**(self.number_of_layers+1)), int(D_out))


        if int(D_out) != 1:
            self.output_layer = torch.nn.Linear(int(D_out), int(D_out))

        self.half2half = AttrProxy(self, 'half2half_')

    def forward(self, x):
        x.to(self.device)
        h = self.act_fun(self.first_layer(x))
        h.to(self.device)
        for i in range(0, self.number_of_layers):
            h = self.act_fun(self.half2half[i](h.to(self.device)))
            h.to(self.device)

        if int(self.D_out)!=1:
            h = self.act_fun(self.second_last_layer(h.to(self.device))).to(self.device)
            y_pred = self.dense_fun(self.output_layer(h.to(self.device))).to(self.device)

        else:
            y_pred = self.dense_fun(self.second_last_layer(h.to(self.device)))

        return y_pred



class ConstantNet(torch.nn.Module):
    def __init__(self, D_in, D_out, H, num_layers = 3, device = "cuda", act_fun = torch.nn.functional.sigmoid, dense_fun=torch.nn.functional.softmax):

        super(ConstantNet,self).__init__()

        self.device=device

        self.act_fun = act_fun
        self.dense_fun = dense_fun

        self.D_out = int(D_out)
        self.D_in = int(D_in)
        self.H = int(H)
        self.num_layers = int(num_layers)
        self.first_layer = torch.nn.Linear(self.D_in, self.H)

        for i in range(num_layers):
            self.add_module('h2h_' + str(i), torch.nn.Linear(self.H, self.H))

        self.output_layer = torch.nn.Linear(self.H, self.D_out)
        self.h2h = AttrProxy(self, 'h2h_')


    def forward(self, x):
        x.to(self.device)
        h = self.act_fun(self.first_layer(x)).to(self.device)


        for i in range(0,self.num_layers):
            h = self.act_fun(self.h2h[i](h)).to(self.device)

        y_pred = self.dense_fun(self.output_layer(h)).to(self.device)


        return y_pred

class ConstantThenHalvedNet(torch.nn.Module):
    def __init__(self, D_in, D_out, H, num_constant_layers, device = "cuda", act_fun = torch.nn.functional.sigmoid, dense_fun=torch.nn.functional.softmax):
        super(ConstantThenHalvedNet,self).__init__()
        self.device = device
        self.act_fun = act_fun
        self.dense_fun = dense_fun
        self.D_in = int(D_in)
        self.D_out = int(D_out)
        self.num_constant_layers = int(num_constant_layers)
        self.H = int(H)
        self.number_halving = int(-1*np.log2(D_out/D_in))

        self.first_layer = torch.nn.Linear(self.D_in, self.H)

        for i in range(0,self.num_constant_layers):
            self.add_module('h2h_' + str(i), torch.nn.Linear(self.H, self.H))

        self.h2h = AttrProxy(self, 'h2h_')

        self.connecting_layer = torch.nn.Linear(self.H, int(self.H/2))

        for i in range(0,self.number_halving):
            self.add_module('half2half_' + str(i), torch.nn.Linear(int(self.H/(2**(i+1))), int(self.H/(2**(i+2))) ) )

        self.second_last_layer = torch.nn.Linear(int(self.H/2**(self.number_halving +1)), self.D_out)

        self.output_layer = torch.nn.Linear(self.D_out, self.D_out)

        self.half2half = AttrProxy(self, 'half2half_')


    def forward(self, x):
        x.to(self.device)

        h = self.act_fun(self.first_layer(x))
        h.to(self.device)

        for i in range(0, self.num_constant_layers):
            h = self.act_fun(self.h2h[i](h)).to(self.device)

        h = self.act_fun(self.connecting_layer(h)).to(self.device)

        for i in range(0, self.number_halving):
            h = self.act_fun(self.half2half[i](h)).to(self.device)

        h = self.act_fun(self.second_last_layer(h)).to(self.device)

        y_pred = self.dense_fun(self.output_layer(h))

        return y_pred




class Conv1D(torch.nn.Module):
    def __init__(self, device = 'cuda', conv_act = F.relu, lin_act = torch.sigmoid):
        super(Conv1D, self).__init__()
        self.device = device
        self.conv_act = conv_act
        self.lin_act = lin_act
        self.conv1 = torch.nn.Conv1d(1, 64, kernel_size = 50,stride = 1)
        self.pool1 = torch.nn.MaxPool1d(kernel_size = 3,stride = 2)
        self.conv2 = torch.nn.Conv1d(64, 64, kernel_size = 25, stride = 1)
        self.pool2 = torch.nn.MaxPool1d(kernel_size = 2,stride = 2)
        ##works for input of 3779, can be made modular
        self.f1 = torch.nn.Linear(101*64, 2000)
        self.f2 = torch.nn.Linear(2000, 500)
        self.f3 = torch.nn.Linear(500, 256)


    def forward(self, x):
        x = self.pool1(self.conv_act(self.conv1(x))).to(self.device)
        x = self.pool2(self.conv_act(self.conv2(x))).to(self.device)
        x = x.view(-1, 101*64)
        x = self.lin_act(self.f1(x)).to(self.device)
        x = self.lin_act(self.f2(x)).to(self.device)
        x = F.softmax(self.f3(x).to(self.device))
        return (x)



class conv1D_CE(torch.nn.Module):
    def __init__(self, device = 'cuda', input_shape = 3779,conv_act = F.relu, lin_act = torch.sigmoid, output_shape = 256):
        super(conv1D_CE, self).__init__()
        self.device = device
        self.conv_act = conv_act
        self.lin_act = lin_act
        self.output_shape = output_shape
        self.input_shape = input_shape
        self.conv1 = torch.nn.Conv1d(1, 64, kernel_size = 3,stride = 1)
        self.pool1 = torch.nn.MaxPool1d(kernel_size = 4,stride = 2)
        self.conv2 = torch.nn.Conv1d(64, 64, kernel_size = 3, stride = 1)
        self.pool2 = torch.nn.MaxPool1d(kernel_size = 4,stride = 2)
        ##works for input of 3779, can be made modular

        size_c1 = ((self.input_shape + 2*0 -  1*(3-1)-1)/1) + 1
        print (size_c1)
        size_p1 = ((size_c1 + 2*0 - 1*(4-1) - 1 )/2) +1
        print (size_p1)
        size_c2 = ((size_p1 + 2*0 - 1*(3-1) - 1)/1) + 1
        print (size_c2)
        size_p2 = ((size_c2 + 2*0 - 1*(4-1) - 1)/2)+1
        print (size_p2)
        self.linear_size = int(size_p2*64)

        self.f1 = torch.nn.Linear(self.linear_size, 100)
        self.f2 = torch.nn.Linear(100, 100)
        self.f3 = torch.nn.Linear(100, self.output_shape)


    def forward(self, x):
        x = self.pool1(self.conv_act(self.conv1(x))).to(self.device)
        x = self.pool2(self.conv_act(self.conv2(x))).to(self.device)
        x = x.view(-1, self.linear_size)
        x = self.lin_act(self.f1(x)).to(self.device)
        x = self.lin_act(self.f2(x)).to(self.device)
        x = self.f3(x).to(self.device)
        return (x)


class general_conv1D_CE(torch.nn.Module):
    def __init__(self, device = 'cuda', conv_act = F.relu, lin_act = torch.sigmoid, input_shape = 10000, convk= (64,50,2,0), poolk=(3,2,0),output_shape = 255, num_channels = 64):
        super(general_conv1D_CE, self).__init__()
        self.input_shape = input_shape
        self.output_shape = output_shape
        self.convk = convk
        self.poolk = poolk
        self.device = device
        self.conv_act = conv_act
        self.lin_act = lin_act
        self.conv1 = torch.nn.Conv1d(1, self.convk[0], kernel_size = self.convk[1],stride = self.convk[2], padding = self.convk[3])
        self.norm1 = torch.nn.BatchNorm1d(convk[0])
        self.pool1 = torch.nn.MaxPool1d(kernel_size = self.poolk[0],stride = self.poolk[1], padding = self.poolk[2])
        self.conv2 = torch.nn.Conv1d(self.convk[0], self.convk[0], kernel_size = self.convk[1]//2,stride = self.convk[2]+1, padding = self.convk[3])
        self.norm2 = torch.nn.BatchNorm1d(convk[0])
        self.pool2 = torch.nn.MaxPool1d(kernel_size = self.poolk[0]-1,stride = self.poolk[1]+1, padding = self.poolk[2])

        sizec1 = ((self.input_shape + 2*self.convk[3] - 1 * (self.convk[1] - 1) -1)//self.convk[2]) + 1
        sizep1 = ((sizec1 + 2*self.poolk[2] - 1 * (self.poolk[0] - 1) -1)//self.poolk[1]) + 1
        ##divide kernal of conv by 2, increase stride by 1, decrease kernal of pool by 1, increase stride by 1
        sizec2 = ((sizep1 + 2*self.convk[3] - 1 * (self.convk[1]//2 - 1) -1)//(self.convk[2]+1)) + 1
        sizep2 = ((sizec2 + 2*self.poolk[2] - 1 * (self.poolk[0] - 2) -1)//(self.poolk[1]+1)) + 1

        linear_size = sizep2*self.convk[0]
        ##works for input of 3779, can be made modular
        self.f1 = torch.nn.Linear(linear_size, linear_size)
        self.f2 = torch.nn.Linear(linear_size, linear_size//2)
        self.f3 = torch.nn.Linear(linear_size//2, self.output_shape)


    def forward(self, x):
        x = self.pool1(self.conv_act(self.norm1(self.conv1(x)))).to(self.device)
        x = self.pool2(self.conv_act(self.norm1(self.conv2(x)))).to(self.device)
        x = x.view(x.size(0), -1)
        x = self.lin_act(self.f1(x)).to(self.device)
        x = self.lin_act(self.f2(x)).to(self.device)
        x = self.f3(x).to(self.device)
        return (x)

class general_conv1D_CE_nonorm(torch.nn.Module):
    def __init__(self, device = 'cuda', conv_act = F.relu, lin_act = torch.sigmoid, input_shape = 10000, convk= (64,50,2,0), poolk=(3,2,0),output_shape = 255, num_channels = 64):
        super(general_conv1D_CE_nonorm, self).__init__()
        self.input_shape = input_shape
        self.output_shape = output_shape
        self.convk = convk
        self.poolk = poolk
        self.device = device
        self.conv_act = conv_act
        self.lin_act = lin_act
        self.conv1= torch.nn.Conv1d(1, self.convk[0], kernel_size = self.convk[1],stride = self.convk[2], padding = self.convk[3])
        self.norm1= torch.nn.BatchNorm1d(self.convk[0])
        self.pool1 = torch.nn.MaxPool1d(kernel_size = self.poolk[0],stride = self.poolk[1], padding = self.poolk[2])
        self.conv2 = torch.nn.Conv1d(self.convk[0], self.convk[0], kernel_size = self.convk[1],stride = self.convk[2], padding = self.convk[3])
        self.norm2 = torch.nn.BatchNorm1d(self.convk[0])
        self.pool2 = torch.nn.MaxPool1d(kernel_size = self.poolk[0],stride = self.poolk[1], padding = self.poolk[2])

        sizec1 = ((self.input_shape + 2*self.convk[3] - 1 * (self.convk[1] - 1) -1)/self.convk[2]) + 1
        print (sizec1)
        sizep1 = ((sizec1 + 2*self.poolk[2] - 1 * (self.poolk[0] - 1) -1)/self.poolk[1]) + 1
        print(sizep1)
        ##divide kernal of conv by 2, increase stride by 1, decrease kernal of pool by 1, increase stride by 1
        sizec2 = ((sizep1 + 2*self.convk[3] - 1 * (self.convk[1] - 1) -1)/(self.convk[2])) + 1
        print (sizec2)
        sizep2 = ((sizec2 + 2*self.poolk[2] - 1 * (self.poolk[0] - 1) -1)/(self.poolk[1])) + 1
        print (sizep2)

        linear_size = int(sizep2*self.convk[0])

        self.f1 = torch.nn.Linear(linear_size, linear_size//2)
        self.f2 = torch.nn.Linear(linear_size//2, linear_size//4)
        self.f3 = torch.nn.Linear(linear_size//4, self.output_shape)


    def forward(self, x):
        x = self.pool1(self.conv_act(self.conv1(x))).to(self.device)
        x = self.pool2(self.conv_act(self.conv2(x))).to(self.device)
        x = x.view(x.size(0), -1)
        x = self.lin_act(self.f1(x)).to(self.device)
        x = self.lin_act(self.f2(x)).to(self.device)
        x = self.f3(x).to(self.device)
        return (x)



class simple_conv2D_CE(torch.nn.Module):
    def __init__(self, input_shape, output_dim, max_pooling_params = (2,2,0),kernel_size = 3, num_channels = 4, device = 'cuda', conv_act = F.relu, lin_act = torch.sigmoid):
        super(simple_conv2D_CE,self).__init__()
        self.device = device
        self.kernel_size = kernel_size
        self.conv_act  = conv_act
        self.mp = max_pooling_params
        self.output_dim = output_dim
        self.input_shape = input_shape
        self.lin_act = lin_act
        ##try and use odd integers for kernel size, can be made modular later
        self.padding = (self.kernel_size - 1)//2
        self.num_channels = num_channels
        self.c1 = torch.nn.Conv2d(1,self.num_channels, kernel_size = self.kernel_size, stride = 1, padding = self.padding)
        self.norm1 = torch.nn.BatchNorm2d(self.num_channels)
        self.pool1 = torch.nn.MaxPool2d(kernel_size = self.mp[0], stride = self.mp[1], padding = self.mp[2])
        self.c2 = torch.nn.Conv2d(self.num_channels, self.num_channels, kernel_size = self.kernel_size, stride = 1, padding = self.padding)
        self.norm2 = torch.nn.BatchNorm2d(self.num_channels)
        self.pool2 = torch.nn.MaxPool2d(kernel_size = self.mp[0], stride = self.mp[1], padding = self.mp[2])

        ##0 is the padding size of the maxpooling layer
        layer_size_1 = (self.input_shape + 2 * self.mp[2] - 1 * (self.mp[0] - 1) -1)//self.mp[1] + 1
        layer_size_2 = (layer_size_1 + 2 * self.mp[2] - 1 * (self.mp[0] - 1) -1)//self.mp[1] + 1

        linear_size = layer_size_2*layer_size_2*num_channels

        self.lin1 = torch.nn.Linear(linear_size, 500)
        self.lin2 = torch.nn.Linear(500,self.output_dim)

    def forward(self, x):
        x = self.pool1(self.conv_act(self.norm1(self.c1(x)))).to(self.device)
        x = self.pool2(self.conv_act(self.norm2(self.c2(x)))).to(self.device)
        x = x.view(x.size(0),-1)
        x = self.lin_act(self.lin1(x)).to(self.device)
        x = self.lin2(x).to(self.device)
        return x




class simple_conv2D_CE_nonorm(torch.nn.Module):
    def __init__(self, input_shape, output_dim, max_pooling_params = (2,2,0),kernel_size = 3, num_channels = 4, device = 'cuda', conv_act = F.relu, lin_act = torch.sigmoid):
        super(simple_conv2D_CE_nonorm,self).__init__()
        self.device = device
        self.kernel_size = kernel_size
        self.conv_act  = conv_act
        self.mp = max_pooling_params
        self.output_dim = output_dim
        self.input_shape = input_shape
        self.lin_act = lin_act
        ##try and use odd integers for kernel size, can be made modular later
        self.padding = (self.kernel_size - 1)//2
        self.num_channels = num_channels
        self.c1 = torch.nn.Conv2d(1,self.num_channels, kernel_size = self.kernel_size, stride = 1, padding = self.padding)
        self.pool1 = torch.nn.MaxPool2d(kernel_size = self.mp[0], stride = self.mp[1], padding = self.mp[2])
        self.c2 = torch.nn.Conv2d(self.num_channels, self.num_channels, kernel_size = self.kernel_size, stride = 1, padding = self.padding)
        self.pool2 = torch.nn.MaxPool2d(kernel_size = self.mp[0], stride = self.mp[1], padding = self.mp[2])

        ##0 is the padding size of the maxpooling layer
        layer_size_1 = (self.input_shape + 2 * self.mp[2] - 1 * (self.mp[0] - 1) -1)//self.mp[1] + 1
        layer_size_2 = (layer_size_1 + 2 * self.mp[2] - 1 * (self.mp[0] - 1) -1)//self.mp[1] + 1

        linear_size = layer_size_2*layer_size_2*num_channels

        self.lin1 = torch.nn.Linear(linear_size, 500)
        self.lin2 = torch.nn.Linear(500,self.output_dim)

    def forward(self, x):
        x = self.pool1(self.conv_act(self.c1(x))).to(self.device)
        x = self.pool2(self.conv_act(self.c2(x))).to(self.device)
        x = x.view(x.size(0),-1)
        x = self.lin_act(self.lin1(x)).to(self.device)
        x = self.lin2(x).to(self.device)
        return x






class three_channel_conv1D_CE(torch.nn.Module):
    def __init__(self, conv_act = F.relu, lin_act = torch.sigmoid, device = 'cuda', input_shape = 10000, convk= (64,50,2,0), poolk=(3,2,0),output_shape = 255, num_channels = 64):
        super(three_channel_conv1D_CE, self).__init__()
        self.input_shape = input_shape
        self.output_shape = output_shape
        self.convk = convk
        self.poolk = poolk
        self.device = device
        self.conv_act = conv_act
        self.lin_act = lin_act

        self.conv1_1 = torch.nn.Conv1d(1, self.convk[0], kernel_size = self.convk[1],stride = self.convk[2], padding = self.convk[3])
        self.norm1_1 = torch.nn.BatchNorm1d(self.convk[0])
        self.pool1_1 = torch.nn.MaxPool1d(kernel_size = self.poolk[0],stride = self.poolk[1], padding = self.poolk[2])
        self.conv2_1 = torch.nn.Conv1d(self.convk[0], self.convk[0], kernel_size = self.convk[1],stride = self.convk[2], padding = self.convk[3])
        self.norm2_1 = torch.nn.BatchNorm1d(self.convk[0])
        self.pool2_1 = torch.nn.MaxPool1d(kernel_size = self.poolk[0],stride = self.poolk[1], padding = self.poolk[2])

        self.conv1_2 = torch.nn.Conv1d(1, self.convk[0], kernel_size = self.convk[1],stride = self.convk[2], padding = self.convk[3])
        self.norm1_2 = torch.nn.BatchNorm1d(self.convk[0])
        self.pool1_2 = torch.nn.MaxPool1d(kernel_size = self.poolk[0],stride = self.poolk[1], padding = self.poolk[2])
        self.conv2_2 = torch.nn.Conv1d(self.convk[0], self.convk[0], kernel_size = self.convk[1],stride = self.convk[2], padding = self.convk[3])
        self.norm2_2 = torch.nn.BatchNorm1d(self.convk[0])
        self.pool2_2 = torch.nn.MaxPool1d(kernel_size = self.poolk[0],stride = self.poolk[1], padding = self.poolk[2])

        self.conv1_3 = torch.nn.Conv1d(1, self.convk[0], kernel_size = self.convk[1],stride = self.convk[2], padding = self.convk[3])
        self.norm1_3 = torch.nn.BatchNorm1d(self.convk[0])
        self.pool1_3 = torch.nn.MaxPool1d(kernel_size = self.poolk[0],stride = self.poolk[1], padding = self.poolk[2])
        self.conv2_3 = torch.nn.Conv1d(self.convk[0], self.convk[0], kernel_size = self.convk[1],stride = self.convk[2], padding = self.convk[3])
        self.norm2_3 = torch.nn.BatchNorm1d(self.convk[0])
        self.pool2_3 = torch.nn.MaxPool1d(kernel_size = self.poolk[0],stride = self.poolk[1], padding = self.poolk[2])




        sizec1 = ((self.input_shape + 2*self.convk[3] - 1 * (self.convk[1] - 1) -1)/self.convk[2]) + 1
        print (sizec1)
        sizep1 = ((sizec1 + 2*self.poolk[2] - 1 * (self.poolk[0] - 1) -1)/self.poolk[1]) + 1
        print(sizep1)
        ##divide kernal of conv by 2, increase stride by 1, decrease kernal of pool by 1, increase stride by 1
        sizec2 = ((sizep1 + 2*self.convk[3] - 1 * (self.convk[1] - 1) -1)/(self.convk[2])) + 1
        print (sizec2)
        sizep2 = ((sizec2 + 2*self.poolk[2] - 1 * (self.poolk[0] - 1) -1)/(self.poolk[1])) + 1
        print (sizep2)

        ## multiple by nbumber inputs (3)
        linear_size = int(sizep2*self.convk[0]*3)
        self.f1 = torch.nn.Linear(linear_size, linear_size//4)
        self.f2 = torch.nn.Linear(linear_size//4, linear_size//8)
        self.f3 = torch.nn.Linear(linear_size//8, self.output_shape)




    def forward(self, x):
        x1 = x[:,0,:].reshape(-1, 1, self.input_shape)
        x2 = x[:,1,:].reshape(-1, 1, self.input_shape)
        x3 = x[:,2,:].reshape(-1, 1, self.input_shape)

        x1 = self.pool2_1(self.conv_act(self.conv2_1(self.pool1_1(self.conv_act(self.conv1_1(x1))))))
        x2 = self.pool2_2(self.conv_act(self.conv2_2(self.pool1_2(self.conv_act(self.conv1_2(x2))))))
        x3 = self.pool2_3(self.conv_act(self.conv2_3(self.pool1_3(self.conv_act(self.conv1_3(x3))))))

        x1 = x1.view(x1.size(0),1, -1)
        x2 = x1.view(x2.size(0),1, -1)
        x3 = x1.view(x3.size(0),1, -1)

        x = torch.cat([x1,x2,x3], dim = 1)
        x = x.view(x.size(0),-1)
        x = self.lin_act(self.f2(self.lin_act(self.f1(x))))
        x = self.f3(x)
        return(x)


class general_Conv1D(torch.nn.Module):
    ## class to construct a general convolutional NN that takes a one-channel input and applies max_pooling and norm after each conv layer
    ##conv_params should contain, for each layer, a dictionary with the following parameters: c_out,k_size, padding, stride
    ##linear_params should contain, for each layer, a dictionary with the following parameters: input_size, output_size
    ##linear_params should agree (i.e the out of one layer should be equal to in of next layer, and last layer should be output shape)
    ##pool_params should contain, for each layer, a dictionary with the following parameters: k_size, padding, stride

    def __init__(self,conv_params, linear_params, pool_params, conv_act = F.relu, lin_act = torch.sigmoid, device='cuda'):
        super(general_Conv1D,self).__init__()
        self.device = device
        self.lin_act = lin_act
        self.conv_act = conv_act
        self.conv_params = conv_params
        self.linear_params = linear_params
        self.pool_params = pool_params

        ##define convolutional layers
        self.number_conv = len(self.conv_params)

        for i in range(0, self.number_conv):
            print(i)
            if i == 0:
                self.add_module('conv_' +str(i), Conv1d(1,
                                                        self.conv_params[i]['c_out'],
                                                        kernel_size = self.conv_params[i]['k_size'],
                                                        padding = self.conv_params[i]['padding'],
                                                        stride = self.conv_params[i]['stride'],
                                                       )
                               )
            else:
                self.add_module('conv_' +str(i), Conv1d(self.conv_params[i-1]['c_out'],
                                                        self.conv_params[i]['c_out'],
                                                        kernel_size = self.conv_params[i]['k_size'],
                                                        padding = self.conv_params[i]['padding'],
                                                        stride = self.conv_params[i]['stride'],
                                                       )
                               )

            self.add_module('norm_'+str(i), BatchNorm1d (self.conv_params[i]['c_out']))

            self.add_module('pool_'+str(i), MaxPool1d(kernel_size = self.pool_params[i]['k_size'],
                                                      padding = self.pool_params[i]['padding'],
                                                      stride = self.pool_params[i]['stride'],
                                                     )
                           )

        self.number_linear = len(self.linear_params)
        for i in range(0, self.number_linear):
            self.add_module('lin_'+str(i), Linear(linear_params[i]['in'],
                                                  linear_params[i]['out'],
                                                 )
                           )

        self.conv = AttrProxy(self, 'conv_')
        self.norm = AttrProxy(self, 'norm_')
        self.pool = AttrProxy(self, 'pool_')
        self.lin = AttrProxy(self, 'lin_')

    def forward(self, x):
        for i in range(0, self.number_conv):
            x = self.pool[i](self.conv_act(self.norm[i](self.conv[i](x)))).to(self.device)

        x = x.view(x.size(0), -1)


        for i in range(0, self.number_linear):
            if i + 1 < self.number_linear:
                x = self.lin_act(self.lin[i](x)).to(self.device)

            else:
                x = self.lin[i](x).to(self.device)

        return x



class general_Conv1D_multiChannel(torch.nn.Module):
    ## class to construct a general convolutional NN that takes a one-channel input and applies max_pooling and norm after each conv layer
    ##conv_params should contain, for each layer, a dictionary with the following parameters: c_out,k_size, padding, stride, groups
    ##linear_params should contain, for each layer, a dictionary with the following parameters: input_size, output_size
    ##linear_params should agree (i.e the out of one layer should be equal to in of next layer, and last layer should be output shape)
    ##pool_params should contain, for each layer, a dictionary with the following parameters: k_size, padding, stride
    ##the in and out channels of the conv1D network must be compatible the groups (must be evenly divisible)

    def __init__(self,conv_params, linear_params, pool_params, num_channels = 2, conv_act = F.relu, lin_act = torch.sigmoid, device='cuda'):
        super(general_Conv1D_multiChannel,self).__init__()
        self.device = device
        self.lin_act = lin_act
        self.conv_act = conv_act
        self.conv_params = conv_params
        self.linear_params = linear_params
        self.pool_params = pool_params
        self.num_channels = num_channels

        ##define convolutional layers
        self.number_conv = len(self.conv_params)

        for i in range(0, self.number_conv):
            print(i)
            if i == 0:
                self.add_module('conv_' +str(i), Conv1d(num_channels,
                                                        self.conv_params[i]['c_out'],
                                                        kernel_size = self.conv_params[i]['k_size'],
                                                        padding = self.conv_params[i]['padding'],
                                                        stride = self.conv_params[i]['stride'],
                                                        groups = self.conv_params[i]['groups'],
                                                       )
                               )
            else:
                self.add_module('conv_' +str(i), Conv1d(self.conv_params[i-1]['c_out'],
                                                        self.conv_params[i]['c_out'],
                                                        kernel_size = self.conv_params[i]['k_size'],
                                                        padding = self.conv_params[i]['padding'],
                                                        stride = self.conv_params[i]['stride'],
                                                        groups = self.conv_params[i]['groups'],
                                                       )
                               )

            self.add_module('norm_'+str(i), BatchNorm1d (self.conv_params[i]['c_out']))

            self.add_module('pool_'+str(i), MaxPool1d(kernel_size = self.pool_params[i]['k_size'],
                                                      padding = self.pool_params[i]['padding'],
                                                      stride = self.pool_params[i]['stride'],
                                                     )
                           )

        self.number_linear = len(self.linear_params)
        for i in range(0, self.number_linear):
            self.add_module('lin_'+str(i), Linear(linear_params[i]['in'],
                                                  linear_params[i]['out'],
                                                 )
                           )

        self.conv = AttrProxy(self, 'conv_')
        self.norm = AttrProxy(self, 'norm_')
        self.pool = AttrProxy(self, 'pool_')
        self.lin = AttrProxy(self, 'lin_')

    def forward(self, x):
        for i in range(0, self.number_conv):
            x = self.pool[i](self.conv_act(self.norm[i](self.conv[i](x)))).to(self.device)

        x = x.view(x.size(0), -1)


        for i in range(0, self.number_linear):
            if i + 1 < self.number_linear:
                x = self.lin_act(self.lin[i](x)).to(self.device)

            else:
                x = self.lin[i](x).to(self.device)

        return x


##for whatever reason, this WILL NOT train
##for identical size inputs, with identical convolutional layers, the size of the output is different
##not sure what is happening, will move to a pytorch-defined 2-channel input (i.e, input channel = 2)
class general_two_channelConv1D(torch.nn.Module):
    def __init__(self,conv_params_1, linear_params, pool_params_1,
                 conv_params_2, pool_params_2, input_shape = 10000,
                 conv_act = F.relu, lin_act = torch.sigmoid, device='cuda'):
        super(general_two_channelConv1D,self).__init__()
        self.device = device
        self.lin_act = lin_act
        self.conv_act = conv_act
        self.conv_params1 = conv_params_1
        self.linear_params = linear_params
        self.pool_params1 = pool_params_1
        self.conv_params2 = conv_params_2
        self.pool_params2 = conv_params_2
        self.input_shape = input_shape


        self.number_conv1 = len(self.conv_params1)

        for i in range(0, self.number_conv1):
            if i == 0:
                self.add_module('conv1_' +str(i), Conv1d(1,
                                                        self.conv_params1[i]['c_out'],
                                                        kernel_size = self.conv_params1[i]['k_size'],
                                                        padding = self.conv_params1[i]['padding'],
                                                        stride = self.conv_params1[i]['stride'],
                                                       )
                               )
            else:
                self.add_module('conv1_' +str(i), Conv1d(self.conv_params1[i-1]['c_out'],
                                                        self.conv_params1[i]['c_out'],
                                                        kernel_size = self.conv_params1[i]['k_size'],
                                                        padding = self.conv_params1[i]['padding'],
                                                        stride = self.conv_params1[i]['stride'],
                                                       )
                               )

            self.add_module('norm1_'+str(i), BatchNorm1d (self.conv_params1[i]['c_out']))

            self.add_module('pool1_'+str(i), MaxPool1d(kernel_size = self.pool_params1[i]['k_size'],
                                                      padding = self.pool_params1[i]['padding'],
                                                      stride = self.pool_params1[i]['stride'],
                                                     )
                           )


        self.number_conv2 = len(self.conv_params2)

        for i in range(0, self.number_conv2):
            if i == 0:
                self.add_module('conv2_' +str(i), Conv1d(1,
                                                        self.conv_params2[i]['c_out'],
                                                        kernel_size = self.conv_params2[i]['k_size'],
                                                        padding = self.conv_params2[i]['padding'],
                                                        stride = self.conv_params2[i]['stride'],
                                                       )
                               )
            else:
                self.add_module('conv2_' +str(i), Conv1d(self.conv_params2[i-1]['c_out'],
                                                        self.conv_params2[i]['c_out'],
                                                        kernel_size = self.conv_params2[i]['k_size'],
                                                        padding = self.conv_params2[i]['padding'],
                                                        stride = self.conv_params2[i]['stride'],
                                                       )
                               )

            self.add_module('norm2_'+str(i), BatchNorm1d (self.conv_params2[i]['c_out']))

            self.add_module('pool2_'+str(i), MaxPool1d(kernel_size = self.pool_params2[i]['k_size'],
                                                      padding = self.pool_params2[i]['padding'],
                                                      stride = self.pool_params2[i]['stride'],
                                                     )
                           )

        self.number_linear = len(self.linear_params)
        for i in range(0, self.number_linear):
            self.add_module('lin_'+str(i), Linear(linear_params[i]['in'],
                                                  linear_params[i]['out'],
                                                 )
                           )

        self.conv1 = AttrProxy(self, 'conv1_')
        self.norm1 = AttrProxy(self, 'norm1_')
        self.pool1 = AttrProxy(self, 'pool1_')
        self.conv2 = AttrProxy(self, 'conv1_')
        self.norm2 = AttrProxy(self, 'norm2_')
        self.pool2 = AttrProxy(self, 'pool2_')
        self.lin = AttrProxy(self, 'lin_')


    def forward(self, x):
        x1 = x[:,0,:].reshape(-1,1,self.input_shape)
        x2 = x[:,1,:].reshape(-1,1,self.input_shape)

        for i in range(0, self.number_conv1):
            x1 = self.pool1[i](self.conv_act(self.norm1[i](self.conv1[i](x1)))).to(self.device)

        x1 = x1.view(x2.size(0), -1)
        for i in range(0, self.number_conv2):
            x2 = self.pool2[i](self.conv_act(self.norm2[i](self.conv2[i](x2)))).to(self.device)

        x2 = x2.view(x2.size(0), -1)
        x = torch.cat([x1,x2], 1)
        x = x.view(x.size(0),-1)

        for i in range(0, self.number_linear):
            if i + 1 < self.number_linear:
                x = self.lin_act(self.lin[i](x)).to(self.device)

            else:
                x = self.lin[i](x).to(self.device)

        return x
       

##removing max-pooling layer seems to fix, will try using this
class general_two_channelConv1D_nopool(torch.nn.Module):
    def __init__(self,conv_params_1, linear_params, conv_params_2,
                dropout = 0.2, input_shape = 10000, conv_act = F.relu, lin_act = torch.sigmoid, device='cuda'):
        super(general_two_channelConv1D_nopool,self).__init__()
        self.device = device
        self.lin_act = lin_act
        self.conv_act = conv_act
        self.conv_params1 = conv_params_1
        self.linear_params = linear_params
        self.conv_params2 = conv_params_2
        self.input_shape = input_shape
        self.dropout = dropout

        self.number_conv1 = len(self.conv_params1)

        for i in range(0, self.number_conv1):
            if i == 0:
                self.add_module('conv1_' +str(i), Conv1d(1,
                                                        self.conv_params1[i]['c_out'],
                                                        kernel_size = self.conv_params1[i]['k_size'],
                                                        padding = self.conv_params1[i]['padding'],
                                                        stride = self.conv_params1[i]['stride'],
                                                       )
                               )
            else:
                self.add_module('conv1_' +str(i), Conv1d(self.conv_params1[i-1]['c_out'],
                                                        self.conv_params1[i]['c_out'],
                                                        kernel_size = self.conv_params1[i]['k_size'],
                                                        padding = self.conv_params1[i]['padding'],
                                                        stride = self.conv_params1[i]['stride'],
                                                       )
                               )

            self.add_module('norm1_'+str(i), BatchNorm1d (self.conv_params1[i]['c_out']))



        self.number_conv2 = len(self.conv_params2)

        for i in range(0, self.number_conv2):
            if i == 0:
                self.add_module('conv2_' +str(i), Conv1d(1,
                                                        self.conv_params2[i]['c_out'],
                                                        kernel_size = self.conv_params2[i]['k_size'],
                                                        padding = self.conv_params2[i]['padding'],
                                                        stride = self.conv_params2[i]['stride'],
                                                       )
                               )
            else:
                self.add_module('conv2_' +str(i), Conv1d(self.conv_params2[i-1]['c_out'],
                                                        self.conv_params2[i]['c_out'],
                                                        kernel_size = self.conv_params2[i]['k_size'],
                                                        padding = self.conv_params2[i]['padding'],
                                                        stride = self.conv_params2[i]['stride'],
                                                       )
                               )

            self.add_module('norm2_'+str(i), BatchNorm1d (self.conv_params2[i]['c_out']))

        self.number_linear = len(self.linear_params)
        for i in range(0, self.number_linear):
            self.add_module('lin_'+str(i), Linear(linear_params[i]['in'],
                                                  linear_params[i]['out'],
                                                 )
                           )
            self.add_module('drop_'+str(i), Dropout(self.dropout))

        self.conv1 = AttrProxy(self, 'conv1_')
        self.norm1 = AttrProxy(self, 'norm1_')
        self.conv2 = AttrProxy(self, 'conv1_')
        self.norm2 = AttrProxy(self, 'norm2_')
        self.lin = AttrProxy(self, 'lin_')
        self.drop = AttrProxy(self, 'drop_')


    def forward(self, x):
        x1 = x[:,0,:].reshape(-1,1,self.input_shape)
        x2 = x[:,1,:].reshape(-1,1,self.input_shape)

        for i in range(0, self.number_conv1):
            x1 = self.conv_act(self.norm1[i](self.conv1[i](x1))).to(self.device)

        x1 = x1.view(x.size(0), -1)
        for i in range(0, self.number_conv2):
            x2 = self.conv_act(self.norm2[i](self.conv2[i](x2))).to(self.device)

        x2 = x2.view(x.size(0), -1)
        X = torch.cat([x1,x2], 1)
        X = X.view(x.size(0),-1)

        for i in range(0, self.number_linear):
            if i + 1 < self.number_linear:
                X = self.lin[i](X).to(self.device)
                X = self.drop[i](X).to(self.device)
                X = self.lin_act(X).to(self.device)

            else:
                X = self.drop[i](self.lin[i](X)).to(self.device)

        return X
 
