import numpy as np
import torch
import pandas as pd
import os
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader, Dataset

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

class TwoChannelConv(torch.nn.Module):
    def __init__(self, net_params,device,p):
        super(TwoChannelConv,self).__init__()
        self.conv_activation = net_params['conv_activate']
        self.lin_activation = net_params['lin_activate']
        self.device = device
        self.dropout = torch.nn.Dropout(p=p)
        self.Seq_RGB = torch.nn.Sequential(
                torch.nn.Conv2d(3,32,3,stride = 2, padding = 1), #150x150
                torch.nn.BatchNorm2d(32),
                self.conv_activation(),
                #torch.nn.Conv2d(32,32,3,stride = 1, padding = 1),#75x75
                #torch.nn.BatchNorm2d(32),
                #self.conv_activation(),
                torch.nn.Conv2d(32,32,3,stride = 2, padding = 1),#75x75
                torch.nn.BatchNorm2d(32),
                self.conv_activation(),
                #torch.nn.Conv2d(32,32,3,stride = 1, padding = 1),#38x38
                #torch.nn.BatchNorm2d(32),
                #self.conv_activation(),
                torch.nn.Conv2d(32,32,3,stride = 2, padding = 1),#19x19
                torch.nn.BatchNorm2d(32),
                self.conv_activation(),
                torch.nn.Conv2d(32,32,3,stride = 2, padding = 1),#10x10
                torch.nn.BatchNorm2d(32),
                self.conv_activation(),
                torch.nn.Flatten(),
                
                torch.nn.Linear(3200,4),
                self.lin_activation(),
                self.dropout,
                )
                
        self.Seq_FT = torch.nn.Sequential(
                torch.nn.Conv2d(2,32,3,stride = 2, padding = 1), #150x150
                torch.nn.BatchNorm2d(32),
                self.conv_activation(),
                #torch.nn.Conv2d(32,32,3,stride = 1, padding = 1),#75x75
                #torch.nn.BatchNorm2d(32),
                #self.conv_activation(),
                torch.nn.Conv2d(32,32,3,stride = 2, padding = 1),#75x75
                torch.nn.BatchNorm2d(32),
                self.conv_activation(),
                #torch.nn.Conv2d(32,32,3,stride = 1, padding = 1),#38x38
                #torch.nn.BatchNorm2d(32),
                #self.conv_activation(),
                torch.nn.Conv2d(32,32,3,stride = 2, padding = 1),#19x19
                torch.nn.BatchNorm2d(32),
                self.conv_activation(),
                torch.nn.Conv2d(32,32,3,stride = 2, padding = 1),#10x10
                torch.nn.BatchNorm2d(32),
                self.conv_activation(),
                torch.nn.Flatten(),
                
                torch.nn.Linear(3200,4),
                self.lin_activation(),
                self.dropout,
                )

        self.last_lin = torch.nn.Linear(8,1)
 
    def forward(self,x):
        x= x.to(self.device)
        rbg = x[:,0:3,:,:]
        ft = x[:,3:,:,:]
            
        rgb_h = self.Seq_RGB(rbg).to(self.device)
        ft_h = self.Seq_FT(ft).to(self.device)
        h = torch.cat((rgb_h,ft_h), axis = 1)
        h = self.last_lin(h)
        return h



        
test = np.random.random((1,150,150,5)).reshape(1,5,150,150)
test = torch.from_numpy(test).float()
print (test.shape)

x_train = np.load('training_data.npy')
x_val = np.load('val_data.npy')

x_train_re = np.zeros(shape = (x_train.shape[0],5,150,150))
x_val_re = np.zeros(shape = (x_val.shape[0],5,150,150))

for i in range(x_train.shape[0]):
    x_train_re[i,0,:,:] = x_train[i,:,:,0]
    x_train_re[i,1,:,:] = x_train[i,:,:,1]
    x_train_re[i,2,:,:] = x_train[i,:,:,2] 
    x_train_re[i,3,:,:] = x_train[i,:,:,3]
    x_train_re[i,4,:,:] = x_train[i,:,:,4] 
for i in range(x_val.shape[0]):
    x_val_re[i,0,:,:] = x_val[i,:,:,0]
    x_val_re[i,1,:,:] = x_val[i,:,:,1]
    x_val_re[i,2,:,:] = x_val[i,:,:,2] 
    x_val_re[i,3,:,:] = x_val[i,:,:,3] 
    x_val_re[i,4,:,:] = x_val[i,:,:,4] 

y_train = np.load('training_labels.npy')
y_val = np.load('val_labels.npy')

def train_model(model, optimizer, x_train_re, x_val_re, y_train, y_val):
    train_data = []
    for i in range(len(x_train_re)):
        train_data.append([x_train_re[i], y_train[i]])

    val_data = []

    for i in range(len(x_val_re)):
        val_data.append([x_val_re[i], y_val[i]])


    b_size = 32
    loss_fn = torch.nn.BCEWithLogitsLoss()
    loader = DataLoader(train_data, batch_size = b_size)
    val_loader = DataLoader(val_data, batch_size = b_size)
    loss_train = []
    loss_val = []
    record_step = 10
    for t in range(100+1):
        tmp_train_loss = []
        for batch in loader:
            x,y = batch
            x = x.float().to(device)
            y = y.float().to(device)

            y_pred = model(x)
            loss = loss_fn(y_pred,y)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            tmp_train_loss.append(loss.item())

        loss_train.append(np.average(tmp_train_loss))
        tmp_val_loss = []
        for batch in val_loader:
            x,y = batch
            x = x.float().to(device)
            y = y.float().to(device)
            y_pred = model(x)
            loss = loss_fn(y_pred,y)
            tmp_val_loss.append(loss.item())
        
        loss_val.append(np.average(tmp_val_loss))

        
        if t%record_step == 0:
            print ("At Epoch {0} the training loss is {1:.2e} and the validation loss is {2:.2e}".format(t, loss_train[t],loss_val[t]))
    
    return model, optimizer, loss_train, loss_val

dict_map = {'ReLU':torch.nn.ReLU, 'LReLU':torch.nn.LeakyReLU, 'Tanh':torch.nn.Tanh}
conv_acts = ['LReLU', 'Tanh','ReLU'] 
lin_acts = ['LReLU', 'Tanh','ReLU'] 
lr = [1e-5, 1e-4, 1e-3, 1e-2]
weight_decay = [1e-5,1e-4, 1e-3, 1e-2]
dropout = [0.1,0.3,0.5]

params = {
        'conv_act':conv_acts,
        'lin_act': lin_acts,
        'lr':lr,
        'weight_decay':weight_decay,
        'dropout':dropout,
        }


save_dir = 'over_sample/trained_nets_simple_BatchNorm/'
import itertools
for i, combo in enumerate(itertools.product(*params.values())):
    conv_act = combo[0]; lin_act = combo[1]; lr = combo[2]; weight_decay = combo[3]; dropout = combo[4]
    print ("Trying a new net with {0} conv act, {1} lin act, a learning rate of {2:.2e}, a weight_decay of {3:.2e} and dropout of {4:.1f}".format(conv_act,lin_act,lr,weight_decay,dropout))

    model =TwoChannelConv({'conv_activate':dict_map[conv_act], 'lin_activate':dict_map[lin_act]}, device, dropout) 
    model.cuda()
    optimizer = torch.optim.Adam(model.parameters(), lr = lr, weight_decay = weight_decay)

    model_trained, opt_trained, loss_train, loss_val = train_model(model, optimizer, x_train_re, x_val_re, y_train, y_val)
    
    torch.save({'epoch':100,
        'model_state_dict':model_trained.state_dict(),
        'optimizer_state_dict':opt_trained.state_dict(),
        'loss':loss_train[-1],
        },
        save_dir +'{0}_convact_{1}_linact_lr_{2:.2e}_weight_decay_{3:.2e}_dropout_{4:.1f}'.format(conv_act,lin_act,lr,weight_decay,dropout))




