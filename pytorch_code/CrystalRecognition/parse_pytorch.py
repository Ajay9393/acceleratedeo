import torch
import numpy as np
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
import os
import re
from train_net_pool import TwoChannelConv
import pandas as pd
from torch.utils.data import DataLoader, Dataset
from sklearn.metrics import accuracy_score, f1_score
import sys


    
dict_map = {'ReLU':torch.nn.ReLU, 'LReLU':torch.nn.LeakyReLU, 'Tanh':torch.nn.Tanh}
def extract_params_from_filename(filename):
    conv_act = re.findall(r'(.+)_convact', filename)[0]
    lin_act = re.findall(r'convact_(.+)_linact', filename)[0]
    lr = re.findall(r'lr_(.+)_weight_decay', filename)[0]
    weight_decay = re.findall(r'weight_decay_(.+)_dropout', filename)[0]
    dropout = re.findall(r'dropout_(.+)', filename)[0]
    
    return conv_act, lin_act, float(lr), float(weight_decay), float(dropout)

def test_net(model, optimizer,val_data, y_val):
    pred_data = np.zeros(y_val.shape) 
    b_size = 128
    val_loader = DataLoader(val_data, batch_size = b_size)
    tmp_val_loss = []
    loss_fn = torch.nn.BCEWithLogitsLoss()

    for i,batch in enumerate(val_loader):
        x,y = batch
        x = x.float().to(device)
        y = y.float().to(device)
        y_pred = model(x)
        loss = loss_fn(y_pred,y)
        tmp_val_loss.append(loss.item())
        pred_data[i*b_size:i*b_size + len(y_pred)] = y_pred.cpu().detach().numpy()
    pred_data  = 1/(1+np.exp(-pred_data))
    pred_data[pred_data > 0.5] = 1
    pred_data[pred_data < 0.5] = 0
        
    return np.average(tmp_val_loss), pred_data


if __name__ == '__main__':
    sd = sys.argv[1]
    filepaths = os.listdir(sd)
    columns = ['Conv Act','Lin Act', 'lr','weight_decay','dropout','train_loss','val_loss','accuracy','f1_score']
    results_df = pd.DataFrame( np.zeros((len(filepaths),len(columns))), columns = columns)


    x_val = np.load('val_data.npy')
    x_val_re = np.zeros(shape = (x_val.shape[0],5,150,150))
    for i in range(x_val.shape[0]):
        x_val_re[i,0,:,:] = x_val[i,:,:,0]
        x_val_re[i,1,:,:] = x_val[i,:,:,1]
        x_val_re[i,2,:,:] = x_val[i,:,:,2] 
        x_val_re[i,3,:,:] = x_val[i,:,:,3] 
        x_val_re[i,4,:,:] = x_val[i,:,:,4] 

    y_val_num = np.load('val_labels.npy')
    x_val_re = torch.from_numpy(x_val_re).float().to(device)
    y_val = torch.from_numpy(y_val_num).float().to(device)
    val_data = []
    for i in range(len(x_val_re)):
        val_data.append([x_val_re[i], y_val[i]])
 
    for i,filename in enumerate(filepaths):
        print (filename)
        checkpoint = torch.load(sd + filename)
        conv_act, lin_act, lr, weight_decay, dropout = extract_params_from_filename(filename)
        model = TwoChannelConv({'conv_activate':dict_map[conv_act], 'lin_activate':dict_map[lin_act]}, device, dropout)
        optimizer = torch.optim.Adam(model.parameters(), lr = lr, weight_decay = weight_decay)
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        model.cuda()
        model.eval()
        
        val_loss, predictions = test_net(model, optimizer,val_data, y_val)
        accuracy = accuracy_score(y_val_num, predictions)
        f1 = f1_score(y_val_num, predictions)
        results_df.loc[i,:]  = [conv_act, lin_act, lr, weight_decay, dropout, checkpoint['loss'],val_loss,accuracy, f1]
   
    results_df.sort_values(by = 'f1_score', axis = 0, ascending = False, inplace = True)
    results_df.to_csv('Results_PoolNorm.csv')
    
        
    
