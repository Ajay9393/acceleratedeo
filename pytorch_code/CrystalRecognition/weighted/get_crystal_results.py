import torch
import numpy as np
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
import os
import re
import sys
sd = '/project/e/esargent/ajay15/Robotic_Perovskite/pytorch_code/CrystalRecognition/weighted/trained_nets_maxpool_2/'
from train_net_pool import TwoChannelConv
sys.path.insert(0,r'/project/e/esargent/ajay15/Robotic_Perovskite/Jeff_Code_BGQ/')
import DataAnalysis as aj
from image_parser import parse_images_from_folder
import data_and_calcs as dac
from torch.utils.data import DataLoader
import pandas as pd
import matplotlib.pyplot as plt
def extract_params_from_filename(filename):
    conv_act = re.findall(r'(.+)_convact', filename)[0]
    lin_act = re.findall(r'convact_(.+)_linact', filename)[0]
    lr = re.findall(r'lr_(.+)_weight_decay', filename)[0]
    weight_decay = re.findall(r'weight_decay_(.+)_dropout', filename)[0]
    dropout = re.findall(r'dropout_(.+)', filename)[0]

    return conv_act, lin_act, float(lr), float(weight_decay), float(dropout)



def return_formatted_input_data(folder):
    ##function is needed to extract meta data (well, drop, time) from each image, and then to properly normalize the images and format them for pytorch
    ##pytorch takes data as (batch, channels, dim x, dim y) rather than (batch, dim x, dim y, channels)
    image_data = parse_images_from_folder(folder+r'th/p01')
    meta_data = image_data[1]
    images = image_data[2]
    input_images = dac.prepare_data2(((images,),), shuffle = False, fft = True, gs = False, Jeff_dumb=True )[0][0]
    input_images_norm = np.zeros(input_images.shape)
    #normalize
    for dim in range(input_images.shape[-1]):
        input_images_norm[:,:,:,dim] = (input_images[:,:,:,dim] - np.min(input_images[:,:,:,dim]))/(np.max(input_images[:,:,:,dim]) - np.min(input_images[:,:,:,dim]))
    
    input_images_real = np.zeros(shape = (input_images_norm.shape[0],5,150,150))
    for i in range(input_images_norm.shape[0]):
        input_images_real[i,0,:,:] = input_images_norm[i,:,:,0]
        input_images_real[i,1,:,:] = input_images_norm[i,:,:,1]
        input_images_real[i,2,:,:] = input_images_norm[i,:,:,2]
        input_images_real[i,3,:,:] = input_images_norm[i,:,:,3]
        input_images_real[i,4,:,:] = input_images_norm[i,:,:,4]


    return meta_data, input_images_real



def evalutae_images(model, data, b_size = 128, device = 'cuda'):
    loader = DataLoader(data, batch_size = b_size)
    model.cuda()
    model.eval()
    y_pred = []
    for batch in loader:
        x = batch.float().to(device)
        temp = model(x).cpu().detach().numpy()
        y_pred = y_pred + temp.reshape(-1).tolist()
    y_pred = np.array(y_pred)
    y_pred = 1/(1+np.exp(-y_pred))
    return y_pred


def return_formatted_csv(meta_data, y_pred, csv_df):
    meta_data['Predictions'] = y_pred
    new_df = pd.DataFrame(columns = meta_data.columns)
    for i in range(1,96+1):
        for d in range(1,3+1):
            temp = meta_data.query('Well=={0:.1f} and Drop=={1:.1f}'.format(i,d))
            idm = temp['Predictions'].argmax()
            new_df = new_df.append(temp.iloc[idm])

    print (csv_df.columns) 
    csv_df.set_index('Well Number', inplace = True)
    new_df['Well Number'] = new_df['Well'].astype(int)
    new_df['Drop'] = new_df['Drop'].astype(int)
    new_df['Elapsed Time'] = new_df['dTime']
    new_df.set_index('Well Number', inplace = True)
    new_df.drop('Well', axis = 1, inplace = True)

    new_df = new_df.merge(csv_df, on = 'Well Number')
    useless_cols = ['dTime','Time','Timestep', 'Batch', 'Antisolvent Volume (uL)','Drop 1 Volume (nL)', 'Drop 2 Volume (nL)','Drop 3 Volume (nL)'] 
    for col in useless_cols:
        new_df.drop(col, inplace = True, axis = 1)


    return new_df




best_net = 'LReLU_convact_LReLU_linact_lr_1.00e-03_weight_decay_1.00e-03_dropout_0.3'
checkpoint = torch.load(sd + best_net)
conv_act, lin_act, lr, weight_decay, dropout = extract_params_from_filename(best_net)
dict_map = {'ReLU':torch.nn.ReLU, 'LReLU':torch.nn.LeakyReLU, 'Tanh':torch.nn.Tanh}
model = TwoChannelConv({'conv_activate':dict_map[conv_act], 'lin_activate':dict_map[lin_act]}, device, dropout)
model.load_state_dict(checkpoint['model_state_dict'])

path ='/project/e/esargent/ajay15/Robotic_Perovskite/pytorch_code/Crystal_Growth/'

folder =  sys.argv[1]


if not os.path.exists(path+folder + '/results'):
    os.makedirs(path+folder+ '/results')
meta_data, input_images_real = return_formatted_input_data(path+folder)
y_pred = evalutae_images(model,input_images_real)



csv_file = [x for x in os.listdir(path+folder) if re.findall(r'.csv', x)][0] 
csv_df = pd.read_csv(path+folder+csv_file)


results_df = return_formatted_csv(meta_data, y_pred, csv_df)
results_df.sort_values('Predictions', ascending = False, inplace = True)
f, ax = plt.subplots(nrows = 1, ncols = 1, figsize = [10,10])
#f.tight_layout()
ax.hist(results_df['Predictions'])
aj.figure_quality_axes(ax, 'Likelihood of Crystallization','Number of Experiments', '',legend = False)
f.savefig(path+folder+'/results/Experiment Histogram.png', format = 'png')
results_df.to_pickle(path+folder+'/results/crystal_results.pickle')
results_df.to_csv(path+folder+'/results/crystal_results.csv')
print ("The best 10 crystals as evaluated by the NN are: ", "\n")
print (results_df.head(10))




