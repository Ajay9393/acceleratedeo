# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 11:57:45 2020

@author: Andrew
"""

##create training and testing data from labelled images
##re-training NN since Niagara purged it
import numpy as np
import pandas as pd
import sys
import os
sys.path.append('../../Jeff_Code_BGQ/')
from image_parser import parse_images_from_folder
from data_and_calcs import prepare_data


training_data = ['1130/','1149/','1182/','1208/']

for folder in training_data:
    if 'image_data.npy' in os.listdir('Training Data/' + folder ):
        continue
    
    image_data = parse_images_from_folder('Training Data/' + folder + r'th/p01',random_noise = True)
    meta_data = image_data[1]
    images = image_data[2]
    
    
    np.save('Training Data/' + folder+'image_data.npy',images)
    meta_data.to_pickle('Training Data/' + folder+'image_meta_data.pickle')
    print ('Saved data from {0}!'.format(folder))
    

data_paths = ['Training Data/'+ x +'image_data.npy' for x in training_data]
target_paths = ['Training Data/'+ x +'_labels_best.npy' for x in ['1130','1149','1182','1208']]


test = prepare_data(data_paths, target_paths, over_sample=False, os_bins=2, val_frac=0.2, test_frac=0.1, by_well=True, trans_rot=False, trans_px=2, trans_rot_frac=0.3, fft=True, gs=False, new_transrot=True)


x_train = test[0][0]
y_train = test[0][1]
x_val = test[1][0]
y_val = test[1][1]
x_test = test[2][0]
y_test = test[2][1]


np.save('weighted/training_data.npy',x_train)
np.save('weighted/training_labels.npy',y_train)
np.save('weighted/val_data.npy',x_val)
np.save('weighted/val_labels.npy',y_val)
np.save('weighted/testing_data.npy',x_test)
np.save('weighted/testing_labels.npy',y_test)

x_train_n = np.zeros(x_train.shape)
x_test_n = np.zeros(x_test.shape)
x_val_n = np.zeros(x_val.shape)

for dim in range(x_train.shape[-1]):
    x_train_n[:,:,:,dim] = (x_train[:,:,:,dim] - np.min(x_train[:,:,:,dim]))/(np.max(x_train[:,:,:,dim]) - np.min(x_train[:,:,:,dim]))

for dim in range(x_train.shape[-1]):
    x_test_n[:,:,:,dim] = (x_test[:,:,:,dim] - np.min(x_test[:,:,:,dim]))/(np.max(x_test[:,:,:,dim]) - np.min(x_test[:,:,:,dim]))

for dim in range(x_train.shape[-1]):
    x_val_n[:,:,:,dim] = (x_val[:,:,:,dim] - np.min(x_val[:,:,:,dim]))/(np.max(x_val[:,:,:,dim]) - np.min(x_val[:,:,:,dim]))


np.save('weighted/training_data_normalized.npy', x_train_n)
np.save('weighted/val_data_normalized.npy', x_val_n)
np.save('weighted/testing_data_normalized.npy', x_test_n)

