import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
import os
import re
import sys
sys.path.append('/scratch/e/esargent/ajay15/')
import DataAnalysis as aj
from sklearn.model_selection import train_test_split
from skorch import NeuralNetClassifier, NeuralNet 
sys.path.append('../../')
from model_definitions import ConstantNet, HalvedNet, Conv1D, conv1D_CE, simple_conv2D_CE, general_conv1D_CE, three_channel_conv1D_CE,general_Conv1D,general_two_channelConv1D, general_Conv1D_multiChannel
from sklearn.model_selection import GridSearchCV
import pickle
import itertools
import json
from torchsummary import summary

from sklearn.datasets import fetch_openml
from torch.nn import  Conv1d, MaxPool1d, BatchNorm1d, Linear

def return_convolutional_layers(num_layers, channels, k_size, stride, padding, groups = None):
    if len(channels) != num_layers or len(k_size) != num_layers or len(stride) != num_layers or len(padding) != num_layers:
        print ("Wrong number of inputs! check that each argument is defined for each layer")
        return
    else:
        conv_layers = []
        for i in range(0, num_layers):
            if not groups:
                conv_layers.append({'c_out':channels[i], 'k_size':k_size[i], 'stride': stride[i], 'padding':padding[i]})
            else:
                conv_layers.append({'c_out':channels[i], 'k_size':k_size[i], 'stride': stride[i], 'padding':padding[i], 'groups':groups[i]})
        return conv_layers

def return_pooling_layers(num_layers,k_size, stride, padding):
    if len(k_size) != num_layers or len(stride) != num_layers or len(padding) != num_layers:
        print ("Wrong number of inputs! check that each argument is defined for each layer")
        return
    else:
        pool_layers = []
        for i in range(0, num_layers):
            pool_layers.append({'k_size':k_size[i], 'stride': stride[i], 'padding':padding[i]})
        
        return pool_layers


def return_linear_layers(num_layers, ins, outs):
   ##does not check to see that first ins and outs agree, should be done manually
    lin_layers = []

    for i in range(0, num_layers):
        lin_layers.append({'in':ins[i], 'out':outs[i]})

    return lin_layers


def check_model_summary(net, device = 'cuda',shape = (1,10000),**kwargs):

    model = net(**kwargs).to(device)

    p = summary(model, shape)
    del model
    return p


def get_linear_size(p, num_channels):
    ##assumes a max pooling layer is present after each convolution, and that the dimension of input decreases or stays the same at each pooling layer
    ##it finds the index where the summary from torchsummary.summary shows an increased pooling dimension: this is the start of a new channel
    keys = [x for x in list(p.keys()) if re.findall('MaxPool',x)]

    if num_channels == 1:
        return p[keys[-1]]['output_shape'][-1]*p[keys[-1]]['output_shape'][-2]

    else:
        outputs = [p[x]['output_shape'][-1]*p[x]['output_shape'][-2] for x in keys]

        ind = np.asarray(np.diff(np.array(outputs)) > 0).nonzero()[0]

        if len (ind) + 1 != num_channels:
            print ("Something wrong! there are more channels in summary than input into function")
        else:
            size = 0.
            for i in ind:
                size+= outputs[i]

            size+= outputs[-1]

            return size


if __name__ == '__main__':
    ##preparing 1D nets
    ##should be 'fourier' or 'XRD'
    data = sys.argv[1]
    
    if data == 'fourier':
        x = np.load('magnitude.npy')
        save_dir = 'FourierNets/'
    
    elif data == 'XRD':
        x = np.load('features_augmented.npy')
        save_dir = '1DXRDNets/'


    targets = np.load('targets_augmented.npy')
    uniques = np.unique(targets)
    x = x.astype('float')
    
    input_shape = x.shape[1]
    x = x.reshape(-1, 1, input_shape)
    y = np.array([np.where(uniques == sg)[0][0] for sg in targets])
    output_shape = len(np.unique(targets))
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    x = torch.from_numpy(x).float()
    y = torch.from_numpy(y).long()
    print (x.shape, y.shape)
    print (y)
    x = x.to(device)

    num_conv_layers = [1,2,3]
    num_linear_layers = 2


    params = {
            'num_conv_layers':[1,2,3],
            'k_size':[50,25,10],
            'num_conv_channels':[4,8,16],
            'num_linear_layers':[2,3,4],
            }
    
    combos = []
    for i, combo in enumerate(itertools.product(*params.values())):
        combos.append(combo)

    for i, combo in enumerate(combos[::-1]):
        if str(len(combos)-i) in os.listdir(save_dir):
            print ("Skipping net number {}, already done".format(str(len(combos) - i)))
            continue
        
        print ("Trying a new with {0:.0f} layers, a kernel of {0:.0f}, {0:.0f} channels and {0:.0f} linear layers".format(combo[0],combo[1],combo[2],combo[3],))
        num = combo[0]
        k_size = combo[1]
        channels = combo[2]
        num_linear_layers = combo[3]
        conv_layers = return_convolutional_layers(num_layers = num, k_size = [k_size]*num, channels = [channels]*num, stride = [2]*num, padding = [0]*num)
        pool_layers = return_pooling_layers(num, [3]*num, [2]*num, [0]*num)
        lin_layers = []
        
        p = check_model_summary(net = general_Conv1D,shape = (1,input_shape),conv_params = conv_layers, linear_params = lin_layers, pool_params= pool_layers,)


        lin_size = get_linear_size(p,1)
        print ("The linear size of this network is {0:.0f}".format(lin_size))
        
        ins = []
        outs = []
        ins.append(lin_size)
        for j in range(1,num_linear_layers):
            outs.append( int(lin_size - (lin_size - output_shape)/num_linear_layers*j)) 
            ins.append( int(lin_size - (lin_size - output_shape)/num_linear_layers*j))

        outs.append(output_shape)

        print(ins)
        print(outs)

        lin_layers = return_linear_layers(num_linear_layers, ins, outs)

        
        p = check_model_summary(net =general_Conv1D, shape = (1,input_shape), conv_params = conv_layers, linear_params = lin_layers, pool_params = pool_layers,)

        ID = save_dir + str(len(combos) - i)

        net = NeuralNetClassifier(
                general_Conv1D,
                criterion = torch.nn.CrossEntropyLoss,
                device = device,
                max_epochs = 2500,
                lr = 1e-3,
                iterator_train__shuffle = True,
                module__device = device,
                module__conv_params = conv_layers,
                module__linear_params = lin_layers,
                module__pool_params = pool_layers,
                )

        net.fit(x,y)
        if not os.path.exists(ID):
            os.makedirs(ID)

        print ("net has been fit! saving to {}".format(ID))
        net.save_params(
                f_params = ID+r'/model.pkl', f_optimizer =ID+ r'/opt.pkl', f_history = ID+r'/history.json'
                )


        model_init_params = {'num_conv_layers': combo[0], 'k_size': combo[1], 'num_conv_channels': combo[2], 'num_linear_layers':combo[3]}

        with open(ID + r'/model_init_params.json','w') as fp:
            json.dump(model_init_params, fp)
