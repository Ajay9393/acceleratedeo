import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
import os
import sys
import re
sys.path.append('/scratch/e/esargent/ajay15/')
import DataAnalysis as aj
from sklearn.model_selection import train_test_split
from skorch import NeuralNetClassifier, NeuralNet
sys.path.append('../../')
from model_definitions import ConstantNet, HalvedNet, Conv1D, conv1D_CE, simple_conv2D_CE, general_conv1D_CE, three_channel_conv1D_CE
from skorch.callbacks import Callback
from sklearn.model_selection import GridSearchCV
import pickle
import itertools
import json
##Manually search a grid of hyperparameters, save after each model is fit-- the gridsearchCV wrapper for sklearn does not finish within 24h, and has no way to save the models during the search


def tweet(msg):
    print( "~"*60)
    print("msg")
    print ("~"*60)



class AccuracyTweet(Callback):
    pass



wd = '/scratch/e/esargent/ajay15/MachineLearning/Robotic_Perovskite/pytorch_code/testing_features/RealData/'
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
x = np.load(wd + 'features_augmented.npy')
x = x.astype(np.float)
input_shape = x.shape[1]

targets = np.load(wd+'targets_augmented.npy')
targets = targets.astype(np.int)
uniques = np.unique(targets)

y = np.array([np.where(uniques == sg)[0][0] for sg in targets])
output_shape = len(np.unique(targets))
print(output_shape)
print(np.max(y))
mag = np.load(wd+'magnitude.npy')
angle = np.load(wd + 'angle.npy')

x = np.array([x, mag, angle]).reshape(-1, 3, input_shape)
       
x = torch.from_numpy(x).float()
y = torch.from_numpy(y).long()

print (x.shape, y.shape)

print (y)
x = x.to(device)
    
params = {
        'lr':[1e-2,1e-3,1e-4],
        'max_epochs':[250,500,750,1000],
        'loss_type':[torch.nn.CrossEntropyLoss],
        'num_channels':[32,16,8],
        }


if __name__ == '__main__':   
    id_ = 'threechanconv1dCE_'         

    print (device)
        ##assume last net was not finished
    num_folders_done = len([x for x in os.listdir(wd + 'trained_nets/') if re.findall(id_, x)]) - 1
    print ("Number of folders in the directory are {}".format(num_folders_done))

    for i,combo in enumerate(itertools.product(*params.values())):
        if i < num_folders_done:
            print ("Skipping net number {}, already done".format(str(i)))
            continue 
        print ("Trying a net with {0:.2f} learning rate and {1:.0f} maximum epochs and {2:.0f} channels".format(combo[0], combo[1], combo[3]))
        print ("We are using the {} loss function".format(combo[2]))
        
        save_folder = wd + 'trained_nets/' + id_ + str(i)

       
        net = NeuralNetClassifier(
            three_channel_conv1D_CE,
            batch_size = 64,
            device = device,
            max_epochs = combo[1],
            lr = combo[0],
            iterator_train__shuffle = True,
            module__device = device,
            criterion = combo[2],
            module__input_shape = input_shape,
            module__output_shape = output_shape,
            module__convk = (combo[3],50,2,0),
            )

        net.fit(x,y)
        if not os.path.exists(save_folder):
            os.makedirs(save_folder)
     
        print ("net has been fit! saving to {}".format(save_folder))
        net.save_params(
                f_params = save_folder+r'/model.pkl', f_optimizer =save_folder+ r'/opt.pkl', f_history = save_folder+r'/history.json'
                )
           
        model_init_params = {'lr':combo[0], 'max_epochs': combo[1],'num_channels': combo[3]}
        with open(save_folder+ r'/model_init_params.json','w') as fp:
            json.dump(model_init_params, fp)


      


