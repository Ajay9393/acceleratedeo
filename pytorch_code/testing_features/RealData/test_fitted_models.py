import json
import pickle
import skorch
import torch
import os
import re
import sys
import numpy as np
from torchsummary import summary
sys.path.append('/scratch/e/esargent/ajay15/')
import DataAnalysis as aj
sys.path.append('../../')
from model_definitions import ConstantNet, HalvedNet, Conv1D, conv1D_CE, simple_conv2D_CE, general_conv1D_CE, three_channel_conv1D_CE,general_Conv1D,general_two_channelConv1D, general_Conv1D_multiChannel
from skorch import NeuralNetClassifier


def return_convolutional_layers(num_layers, channels, k_size, stride, padding, groups = None):
    if len(channels) != num_layers or len(k_size) != num_layers or len(stride) != num_layers or len(padding) != num_layers:
        print ("Wrong number of inputs! check that each argument is defined for each layer")
        return
    else:
        conv_layers = []
        for i in range(0, num_layers):
            if not groups:
                conv_layers.append({'c_out':channels[i], 'k_size':k_size[i], 'stride': stride[i], 'padding':padding[i]})
            else:
                conv_layers.append({'c_out':channels[i], 'k_size':k_size[i], 'stride': stride[i], 'padding':padding[i], 'groups':groups[i]})
        return conv_layers

def return_pooling_layers(num_layers,k_size, stride, padding):
    if len(k_size) != num_layers or len(stride) != num_layers or len(padding) != num_layers:
        print ("Wrong number of inputs! check that each argument is defined for each layer")
        return
    else:
        pool_layers = []
        for i in range(0, num_layers):
            pool_layers.append({'k_size':k_size[i], 'stride': stride[i], 'padding':padding[i]})
        
        return pool_layers


def return_linear_layers(num_layers, ins, outs):
   ##does not check to see that first ins and outs agree, should be done manually
    lin_layers = []

    for i in range(0, num_layers):
        lin_layers.append({'in':ins[i], 'out':outs[i]})

    return lin_layers


def check_model_summary(net, device = 'cuda',shape = (1,10000),**kwargs):

    model = net(**kwargs).to(device)

    p = summary(model, shape)
    del model
    return p


def get_linear_size(p, num_channels):
    ##assumes a max pooling layer is present after each convolution, and that the dimension of input decreases or stays the same at each pooling layer
    ##it finds the index where the summary from torchsummary.summary shows an increased pooling dimension: this is the start of a new channel
    keys = [x for x in list(p.keys()) if re.findall('MaxPool',x)]

    if num_channels == 1:
        return p[keys[-1]]['output_shape'][-1]*p[keys[-1]]['output_shape'][-2]

    else:
        outputs = [p[x]['output_shape'][-1]*p[x]['output_shape'][-2] for x in keys]

        ind = np.asarray(np.diff(np.array(outputs)) > 0).nonzero()[0]

        if len (ind) + 1 != num_channels:
            print ("Something wrong! there are more channels in summary than input into function")
        else:
            size = 0.
            for i in ind:
                size+= outputs[i]

            size+= outputs[-1]

            return size


def return_json_params(net_path):
    with open(net_path + r'model_init_params.json','r') as fp:
        Data = fp.read()
    params = json.loads(Data)
    return params


def return_net(net_path, params, net_type, input_shape, output_shape, device):
    combo = params
    num = combo['num_conv_layers']
    k_size = combo['k_size']
    channels = combo['num_conv_channels']
    num_linear_layers = combo['num_linear_layers']
    conv_layers = return_convolutional_layers(num_layers = num, k_size = [k_size]*num, channels = [channels]*num, stride = [2]*num, padding = [0]*num)
    pool_layers = return_pooling_layers(num, [3]*num, [2]*num, [0]*num)
    lin_layers = []
    
    p = check_model_summary(net = general_Conv1D,shape = (1,input_shape),conv_params = conv_layers, linear_params = lin_layers, pool_params= pool_layers,)
    lin_size = get_linear_size(p,1)


    ins = []
    outs = []
    ins.append(lin_size)
    for j in range(1,num_linear_layers):
        outs.append( int(lin_size - (lin_size - output_shape)/num_linear_layers*j)) 
        ins.append( int(lin_size - (lin_size - output_shape)/num_linear_layers*j))

    outs.append(output_shape)

    lin_layers = return_linear_layers(num_linear_layers, ins, outs)

    net = NeuralNetClassifier(
            net_type,
            criterion = torch.nn.CrossEntropyLoss,
            device = device,
            max_epochs = 2500,
            lr = 1e-3,
            iterator_train__shuffle = True,
            module__device = device,
            module__conv_params = conv_layers,
            module__linear_params = lin_layers,
            module__pool_params = pool_layers,
            )
    net.initialize()
    net.load_params(net_path + 'model.pkl')

    return net


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
test_path = '1DXRDNets/6/'
net_type = general_Conv1D
params = return_json_params(test_path)
x = np.load('features_augmented.npy')
x = x.astype('float')
input_shape = x.shape[1]
x = x.reshape(-1,1, input_shape)
x = torch.from_numpy(x).float()
targets = np.load('targets_augmented.npy')
uniques = np.unique(targets)
y = np.array([np.where(uniques == sg)[0][0] for sg in targets])
output_shape = len(np.unique(targets))


net = return_net(test_path, params, net_type, input_shape, output_shape, device)
