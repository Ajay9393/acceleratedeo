import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
import os
import re
import sys
sys.path.append('/scratch/e/esargent/ajay15/')
import DataAnalysis as aj
from sklearn.model_selection import train_test_split
from skorch import NeuralNetClassifier, NeuralNet 
sys.path.append('../../')
from model_definitions import ConstantNet, HalvedNet, Conv1D, conv1D_CE, simple_conv2D_CE, general_conv1D_CE, three_channel_conv1D_CE,general_Conv1D,general_two_channelConv1D
from sklearn.model_selection import GridSearchCV
import pickle
import itertools
import json
from torchsummary import summary

from sklearn.datasets import fetch_openml
from torch.nn import  Conv1d, MaxPool1d, BatchNorm1d, Linear

def return_convolutional_layers(num_layers, channels, k_size, stride, padding):
    if len(channels) != num_layers or len(k_size) != num_layers or len(stride) != num_layers or len(padding) != num_layers:
        print ("Wrong number of inputs! check that each argument is defined for each layer")
        return
    else:
        conv_layers = []
        for i in range(0, num_layers):
            conv_layers.append({'c_out':channels[i], 'k_size':k_size[i], 'stride': stride[i], 'padding':padding[i]})
        return conv_layers

def return_pooling_layers(num_layers,k_size, stride, padding):
    if len(k_size) != num_layers or len(stride) != num_layers or len(padding) != num_layers:
        print ("Wrong number of inputs! check that each argument is defined for each layer")
        return
    else:
        pool_layers = []
        for i in range(0, num_layers):
            pool_layers.append({'k_size':k_size[i], 'stride': stride[i], 'padding':padding[i]})
        
        return pool_layers


def return_linear_layers(num_layers, ins, outs):
   ##does not check to see that first ins and outs agree, should be done manually
    lin_layers = []

    for i in range(0, num_layers):
        lin_layers.append({'in':ins[i], 'out':outs[i]})

    return lin_layers


def check_model_summary(net, device = 'cuda',shape = (1,10000),**kwargs):

    model = net(**kwargs).to(device)

    p = summary(model, shape)
    del model
    return p


def get_linear_size(p, num_channels):
    ##assumes a max pooling layer is present after each convolution, and that the dimension of input decreases or stays the same at each pooling layer
    ##it finds the index where the summary from torchsummary.summary shows an increased pooling dimension: this is the start of a new channel
    keys = [x for x in list(p.keys()) if re.findall('MaxPool',x)]

    if num_channels == 1:
        return p[keys[-1]]['output_shape'][-1]*p[keys[-1]]['output_shape'][-2]

    else:
        outputs = [p[x]['output_shape'][-1]*p[x]['output_shape'][-2] for x in keys]

        ind = np.asarray(np.diff(np.array(outputs)) > 0).nonzero()[0]

        if len (ind) + 1 != num_channels:
            print ("Something wrong! there are more channels in summary than input into function")
        else:
            size = 0.
            for i in ind:
                size+= outputs[i]

            size+= outputs[-1]

            return size







if __name__ == '__main__':
    ##preparing 1D nets 

    if not os.path.exists('MNIST_features.npy'):

        mnist = fetch_openml('mnist_784', cache=False)
        wd = 'scratch/e/esargent/ajay15/MachineLearning/Robotic_Perovskite/pytorch_code/testing_features/MNIST/'

        x = mnist.data.astype('float32')
        y = mnist.target.astype('int64')
        np.save('MNIST_features.npy',x)
        np.save('MNIST_targets.npy',y)

    else:
        x = np.load('MNIST_features.npy')
        y = np.load('MNIST_targets.npy')

    

    x /=255.0

    input_shape = x.shape[1]
    x = x.reshape(-1, 1, input_shape)
    output_shape = len(np.unique(y))
    
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    x = torch.from_numpy(x).float()
    y = torch.from_numpy(y).long()

    num_conv_layers = [1,2,3]
    num_linear_layers = 3
    for num in num_conv_layers:
        conv_layers = return_convolutional_layers(num, [16]*num, [5]*num, [2]*num, [0]*num)
        pool_layers = return_pooling_layers(num, [3]*num, [2]*num, [0]*num)
        lin_layers = []
        p = check_model_summary(net = general_Conv1D,shape = (1,input_shape), conv_params = conv_layers, linear_params = lin_layers, pool_params= pool_layers,)


        lin_size = get_linear_size(p,1)
        print ("The linear size of this network is {0:.0f}".format(lin_size))
        
        ins = []
        outs = []
        ins.append(lin_size)
        for i in range(1,num_linear_layers):
            outs.append( int(lin_size - (lin_size - output_shape)/num_linear_layers*i)) 
            ins.append( int(lin_size - (lin_size - output_shape)/num_linear_layers*i))

        outs.append(output_shape)

        print(ins)
        print(outs)

        lin_layers = return_linear_layers(num_linear_layers, ins, outs)

        p = check_model_summary(net =general_Conv1D, shape = (1,input_shape), conv_params = conv_layers, linear_params = lin_layers, pool_params = pool_layers,)

        net = NeuralNetClassifier(
                general_Conv1D,
                criterion = torch.nn.CrossEntropyLoss,
                device = device,
                max_epochs = 2500,
                lr = 1e-4,
                iterator_train__shuffle = True,
                module__device = device,
                module__conv_params = conv_layers,
                module__linear_params = lin_layers,
                module__pool_params = pool_layers,
                )

        net.fit(x,y)
