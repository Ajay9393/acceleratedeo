import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
import os
import re
import sys
sys.path.append('/scratch/e/esargent/ajay15/')
import DataAnalysis as aj
from sklearn.model_selection import train_test_split
from skorch import NeuralNetClassifier, NeuralNet 
sys.path.append('../../')
from model_definitions import ConstantNet, HalvedNet, Conv1D, conv1D_CE, simple_conv2D_CE, general_conv1D_CE, three_channel_conv1D_CE
from sklearn.model_selection import GridSearchCV
import pickle
import itertools
import json
from torchsummary import summary

from sklearn.datasets import fetch_openml



if not os.path.exists('MNIST_features.npy'):

    mnist = fetch_openml('mnist_784', cache=False)
    wd = 'scratch/e/esargent/ajay15/MachineLearning/Robotic_Perovskite/pytorch_code/testing_features/MNIST/'

    x = mnist.data.astype('float32')
    y = mnist.target.astype('int64')
    np.save('MNIST_features.npy',x)
    np.save('MNIST_targets.npy',y)

else:
    x = np.load('MNIST_features.npy')
    y = np.load('MNIST_targets.npy')




x/=255.0

input_shape = x.shape[1]
print (x.shape)

fft = np.fft.fft(x)
mag = np.absolute(x)
ang = np.angle(x)


X = np.array([x, mag, ang]).reshape(-1, 3, input_shape)

output_shape = len(np.unique(y))
x = x.reshape(-1,1, input_shape)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
x = torch.from_numpy(x).float()
y = torch.from_numpy(y).long()


model = three_channel_conv1D_CE(convk = (32,3,1,0), poolk = (4,2,0), input_shape = input_shape).to(device)
#model = conv1D_CE(input_shape = input_shape, output_shape = output_shape).to(device)

summary(model, (3, input_shape))

net_name = three_channel_conv1D_CE

net = NeuralNetClassifier(
        net_name,
        criterion = torch.nn.CrossEntropyLoss,
        device = device,
        max_epochs = 2000,
        lr = 1e-2,
        iterator_train__shuffle = True,
        module__device = device,
        module__input_shape = input_shape,
        module__output_shape = output_shape,
        module__convk = (32,3,1,0),
        module__poolk = (4,2,0),
        )


net.fit(X,y)

net.save_params(
        f_params = r'3_channel_model.pkl', f_optimizer = r'3_channel_model_opt.pkl', f_history = '3_channel_model_history.json',
        )



