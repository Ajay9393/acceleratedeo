
from pymatgen.ext.matproj import MPRester
from pymatgen.symmetry import analyzer
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
sys.path.append('../../../../')
import DataAnalysis as aj
import os
import pickle
key = 'PGaO0oi7Tz4vaTq8px'
from pathlib import Path

mpr = MPRester(key)


## download data if it doesn't exist


if not os.path.isfile('all_NA_data.pickle'):
    data = mpr.query(chunk_size = 50, criteria={'elements':{'$in': ['Na']}}, properties = ['material_id','formula','xrd.Cu.pattern','spacegroup'])
    with open('all_NA_data.pickle','wb') as f:
        pickle.dump(data,f)
else:
    with open('all_NA_data.pickle','rb') as f:
        data = pickle.load(f)



##try different thresholds



peaks_dict = {}
thetas_dict = {}
spacegroups_dict = {}
ids_dict = {}
formulas_dict = {}


thresholds_to_try = [0.05,0.1,0.15,0.2,0.25]

for j, th in enumerate(thresholds_to_try):
    peaks_dict[th] = []
    thetas_dict[th] = []
    spacegroups_dict[th] = []
    formulas_dict[th] = []
    ids_dict[th] = []
    i=0
    for mat in data:
        if mat['xrd.Cu.pattern']:
            peaks_dict[th].append(np.array(mat['xrd.Cu.pattern'])[:,0])
            thetas_dict[th].append(np.array(mat['xrd.Cu.pattern'])[:,2])
            formulas_dict[th].append(mat['formula'])
            ids_dict[th].append(mat['material_id'])
            spacegroups_dict[th].append(mat['spacegroup']['number'])
            peaks_dict[th][i] = peaks_dict[th][i]/np.max(peaks_dict[th][i])
            indices = np.where(peaks_dict[th][i] > th)
            peaks_dict[th][i] = peaks_dict[th][i][indices]
            thetas_dict[th][i] = thetas_dict[th][i][indices]
            i+=1

##Get 2D matrix data
direc = '2DDistanceMatrix'
if not os.path.exists(direc):
    os.mkdir(direc)

for j, th in enumerate(thresholds_to_try):
    max_entries = np.max([len(x) for x in peaks_dict[th]])
    print (max_entries)
    max_entries_ind = np.argmax([len(x) for x in peaks_dict[th]])
    feature_data = np.zeros((max_entries, max_entries, len(peaks_dict[th])))

    for p, peaks in enumerate(peaks_dict[th]):
        num_repl = peaks.shape[0]

        for i in range(num_repl):
            feature_data[i, 0:num_repl, p] = peaks - peaks[i]


    np.save(direc +'/features_{0:.2f}_threshold.npy'.format(th),feature_data)
    np.save(direc + '/targets_{0:.2f}_threshold.npy'.format(th), spacegroups_dict[th])
    np.savetxt(direc + '/formulas_{0:.2f}_threshold.txt'.format(th),formulas_dict[th], fmt = '%s')



##Generate data for sparse 2D network (light up entry if peak exists)
resolution = 10000
two_theta = np.linspace(0,180,resolution)

direc = 'SparseMatrix'
if not os.path.exists(direc):
    os.mkdir(direc)


for j, th in enumerate(thresholds_to_try):
    feature_data_2 = np.zeros((len(peaks_dict[th]), len(two_theta)))
    print(th)
    for p, peaks in enumerate(peaks_dict[th]):
        for peak in peaks:
            ind = aj.find_nearest(peak, two_theta)
            feature_data_2[p, ind] = 1.
 
    np.save(direc +'/features_{0:.2f}_threshold.npy'.format(th),feature_data_2)
    np.save(direc + '/targets_{0:.2f}_threshold.npy'.format(th), spacegroups_dict[th])
    np.savetxt(direc + '/formulas_{0:.2f}_threshold.txt'.format(th),formulas_dict[th], fmt = '%s')


   

def guassian(x,a,b,c):
    return a*np.exp(-(x - b)**2./(2.*c**2.))



direc = 'FourierTransform'
if not os.path.exists(direc):
    os.mkdir(direc)

for j, th in enumerate(thresholds_to_try):
    feature_data_3 = np.zeros((len(peaks_dict[th]), len(two_theta)))
    print (th)
    for p, peaks in enumerate(peaks_dict[th]):
        intens=0
        for i, peak in enumerate(peaks):
            intens+= guassian(two_theta, peak, thetas_dict[th][p][i], 0.11)

        feature_data_3[p,:] = intens
    
    np.save(direc +'/features_{0:.2f}_threshold.npy'.format(th),feature_data_3)
    np.save(direc + '/targets_{0:.2f}_threshold.npy'.format(th), spacegroups_dict[th])
    np.savetxt(direc + '/formulas_{0:.2f}_threshold.txt'.format(th),formulas_dict[th], fmt = '%s')










