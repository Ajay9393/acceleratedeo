import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
import os
import sys
import re
sys.path.append('/scratch/e/esargent/ajay15/')
import DataAnalysis as aj
from sklearn.model_selection import train_test_split
from skorch import NeuralNetClassifier, NeuralNet
from model_definitions import ConstantNet, HalvedNet
from skorch.callbacks import Callback
from sklearn.model_selection import GridSearchCV
import pickle
import itertools
import json
##Manually search a grid of hyperparameters, save after each model is fit-- the gridsearchCV wrapper for sklearn does not finish within 24h, and has no way to save the models during the search


def tweet(msg):
    print( "~"*60)
    print("msg")
    print ("~"*60)



class AccuracyTweet(Callback):
    pass



x = np.load('/scratch/e/esargent/ajay15/MachineLearning/Robotic_Perovskite/Augmented_Data_With_Exp_Theta/features_augmented.npy')
x = x.astype('float')
targets = np.load('/scratch/e/esargent/ajay15/MachineLearning/Robotic_Perovskite/Augmented_Data_With_Exp_Theta/targets_augmented.npy')
##change targets to be (256,) dimension vector with 1's in place of an integer from 1 to 256
targets = targets.astype('int')
##not needed for CrossEntropyLoss--> expects a 1D vector size of batch with value b/w 0 and 255

'''y = np.zeros((targets.size, 256))
y[np.arange(targets.size), targets - 1] = 1 
y = y.astype('float')
'''
params = {
        'lr':[1e-2,1e-3,1e-4],
        'max_epochs':[250,500,750,1000],
        'loss_type':[torch.nn.NLLLoss]
        }


if __name__ == '__main__':   
    id_ = 'halved_'         
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print (device)
    x = torch.from_numpy(x).float()
    y = torch.from_numpy(targets).long()
    print (x.shape, y.shape)

    print (y)
    
    ##assume last net was not finished
    num_folders_done = len([x for x in os.listdir('trained_nets/') if re.findall(id_, x)]) - 1
    print ("Number of folders in the directory are {}".format(num_folders_done))

    for i,combo in enumerate(itertools.product(*params.values())):
        if i < num_folders_done:
            print ("Skipping net number {}, already done".format(str(i)))
            continue 
        print ("Trying a net with {0:.2f} learning rate and {1:.0f} maximum epochs".format(combo[0], combo[1]))
        print ("We are using the {} loss function".format(combo[-1]))
        
        save_folder = 'trained_nets/' + id_ + str(i)

        if not os.path.exists(save_folder):
            os.makedirs(save_folder)
        

        ##has n layers of fully connected layers with a 
        net = NeuralNetClassifier(
            HalvedNet,
            device = device,
            max_epochs = combo[1],
            lr = combo[0],
            iterator_train__shuffle = True,
            module__D_in = x.shape[1],
            module__D_out = 255,
            module__device = device,
            criterion = combo[2],
            )

        net.fit(x,y)

        print ("net has been fit! saving to {}".format(save_folder))
        net.save_params(
                f_params = save_folder+r'/model.pkl', f_optimizer =save_folder+ r'/opt.pkl', f_history = save_folder+r'/history.json'
                )
           
        model_init_params = {'lr':combo[0], 'max_epochs': combo[1]}
        with open(save_folder+ r'/model_init_params.json','w') as fp:
            json.dump(model_init_params, fp)


        




        




