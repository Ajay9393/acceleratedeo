# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 10:24:56 2021

@author: ak4jo
"""


import numpy as np
import tkinter as tk
import os
import tkinter.messagebox as tkMessageBox
import tkinter.filedialog as tkFileDialog
import time
import operator
import sys
import matplotlib
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
matplotlib.use('TkAgg')
import matplotlib.cm as cm
import re
import DataAnalysis as aj
from ColourStuff import xyz_from_xy, ColourSystem
from scipy.signal import savgol_filter
from PIL import Image
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
from mpl_toolkits.axes_grid1.colorbar import colorbar
import pandas as pd



class secondWindow(object):
    def __init__(self, master, x_data, y_data, well, drop, last_image_path, data = None,):
        top=self.top=tk.Toplevel(master)
        self.well = well
        self.drop = drop
        self.last_image_path = last_image_path
        self.data = data
        self.frame = tk.Frame(self.top)
        self.frame.pack(side = "right")
        self.myFigure = Figure(figsize = [14,16,])
        self.canvas = FigureCanvasTkAgg(self.myFigure, master = self.top)
        self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        self.Axes = self.myFigure.subplots(nrows = 2, ncols = 1)
        self.x_data = x_data
        self.y_data = aj.min_max(y_data)
        self.plot_data()
    
    def figure_quality_axes(self, xl, yl, title, legend = True, loc = 0, fontsize = 20):
        self.Axes[0].set_xlabel(xl, fontsize = fontsize, weight = 'bold')
        self.Axes[0].set_ylabel(yl, fontsize = fontsize, weight = 'bold')
        self.Axes[0].set_title(title, fontsize = fontsize, weight = 'bold')
        self.Axes[0].tick_params(axis='x',labelsize = fontsize, width = 3,length =10)
        self.Axes[0].tick_params(axis='y',labelsize = fontsize, width = 3,length =10)
        [i.set_linewidth(4) for i in self.Axes[0].spines.values()]
        if legend:
            h, l = self.Axes[0].get_legend_handles_labels()
            self.Axes[0].legend(h,l, fontsize = 18, loc = loc)
        
    def plot_data(self):
        self.Axes[0].plot(self.x_data, self.y_data)
        self.Axes[0].set_xlabel('Wavelength')
        self.figure_quality_axes('Wavelength','Intensity','Well number {0}, drop {1}'.format(self.well, self.drop), legend = False)
        im = Image.open(self.last_image_path)
        crystal = np.asarray(im)
        self.Axes[1].imshow(crystal)
        
        if self.data is not None:
            self.myFigure.text(0.0,0.2, '\n'.join(list(map(str,self.data.columns))), fontsize = 14,)
            self.myFigure.text(0.125,0.2, '\n'.join(list(map(str,self.data.values[0]))), fontsize = 14,)
        #self.render_mpl_table(ax = self.Axes[2], data = self.data)
        
        

        
class MainWindow(object):
    def __init__(self, master, path):
        self.master = master
        self.frame = tk.Frame(self.master)
        self.frame.pack(side = "right")
        self.pickExpButton = tk.Button(self.frame, text = "Select Experimental ID", command = self.PickExperiment)
        self.pickExpButton.grid(row = 0, column =0, sticky = 'E', padx = 10, pady=0)
    
        self.WminVar = tk.DoubleVar()
        self.WminVar.trace("w", callback = self.SetWminCallback)
        self.WminVar.set(400)
        self.WmaxVar = tk.DoubleVar()
        self.WmaxVar.trace("w", callback = self.SetWmaxCallback)
        self.WmaxVar.set(700)
        
        self.WminLabel = tk.Label(self.frame, text = 'Min Wavelength of Interest')
        self.WminLabel.grid(row = 1, column = 0, sticky = 'E')
        self.WmaxLabel = tk.Label(self.frame, text = 'Max Wavelength of Interest')
        self.WmaxLabel.grid(row = 2, column = 0, sticky = 'E')
        self.WminEntry = tk.Entry(self.frame, textvariable = self.WminVar, width = 5)
        self.WminEntry.grid(row = 1, column = 1, sticky ='W', padx = 5, pady=10 )
        self.WmaxEntry = tk.Entry(self.frame, textvariable = self.WmaxVar, width = 5)
        self.WmaxEntry.grid(row = 2, column = 1, sticky ='W', padx = 5, pady=10 )
        
        self.path = path
        self.title = master.title('Crystal Explorer')
        self.myFigure = Figure(figsize = [15,15])
        self.canvas = FigureCanvasTkAgg(self.myFigure, master = self.master)
        self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        self.Axes = self.myFigure.subplots(nrows = 2, ncols = 1)
        
        self.c1_divider = make_axes_locatable(self.Axes[0]);
        self.cax1 = self.c1_divider.append_axes("right", size="7%", pad="4%",);
        
        self.ax2_divider = make_axes_locatable(self.Axes[1]);
        self.cax2 = self.ax2_divider.append_axes("right", size="7%", pad="4%",);
        
        self.cax2.axis('off')
        
        self.cmap = cm.Reds
        
        
        
        self.norm = matplotlib.colors.Normalize(vmin = 0, vmax = 1)
        self.cb1 = matplotlib.colorbar.ColorbarBase(self.cax1, cmap = self.cmap, norm = self.norm, orientation = 'vertical')
        self.cb1.set_label('Relative Intensity', rotation = 'vertical', fontsize = 16, weight='bold' )
        self.index_x_new = 0
        self.index_y_new = 0
        self.x_points = np.linspace(start=0, stop=9*11, num=12) 
        self.y_points =  [0]
        for i in range(23):
            if i%3 == 2:
                self.y_points.append(self.y_points[-1]+3.65)
            else:
                self.y_points.append(self.y_points[-1]+2.65)
                
        self.y_points = np.array(self.y_points)
        self.xlines = np.linspace(start = 0, stop = 9*11, num = 12)[0:-1] + 99/12/2
        self.ylines = [2.65*2+3.65/2 + (2.65*2+3.65)*i for i in range(0,7) ]

        self.plot_grid_lines()

    def plot_grid_lines(self):
        for x in self.xlines:
            self.Axes[0].axvline(x, color = 'k')
            self.Axes[1].axvline(x, color = 'k')
        for y in self.ylines:
            self.Axes[0].axhline(y, color = 'k')
            self.Axes[1].axhline(y, color = 'k')
        
    def SetWminCallback(self,*dummy):
        print ("replotting data with new wmin of {0:.0f}".format(self.WminVar.get()))
        self.extract_intensities()
        self.update_intensity_plot()
        
    
    def SetWmaxCallback(self,*dummy):
        print ("replotting data with new wmax of {0:.0f}".format(self.WmaxVar.get()))
        self.extract_intensities()
        self.update_intensity_plot()
        
    def PickExperiment(self):
        self.ex_id = tkFileDialog.askdirectory(parent=self.master, initialdir=self.path,
                            title='Select the experimental ID')
        self.load_image_path_data()
        self.load_PL_data()
        self.load_exp_params()
        self.extract_intensities()
        self.extract_colours()
        self.plot_PL_data()
        
    def load_exp_params(self):
        try:
            self.experiment_params = pd.read_csv(self.path+self.ex_id+'/experimental_params.csv')
            self.experiment_params.drop(['Drop 1 Volume (nL)', 'Drop 2 Volume (nL)','Drop 3 Volume (nL)'], inplace = True, axis = 1)
        except IOError:
            self.experiment_params = None
        
    def load_image_path_data(self):
        self.file_list = os.listdir(self.path+self.ex_id+ r'/ef/p01/')
        print (self.file_list)
        
    def load_PL_data(self):
        self.Spectra = np.loadtxt(self.path+self.ex_id+'/Spectra.csv', delimiter = ',')
        print (self.Spectra.shape)
    
    def extract_intensities(self):
        w_min = self.WminVar.get()
        w_max = self.WmaxVar.get()
        w = self.Spectra[:,0]
        
        s = aj.find_nearest(w,w_min)
        e = aj.find_nearest(w,w_max)
        self.Intensities = np.sum(self.Spectra[s:e,1:],axis = 0)
        print (self.Intensities.shape)
               
    def extract_colours(self):
        r = xyz_from_xy(0.7347, 0.2653)
        g = xyz_from_xy(0.1152,0.88264)
        b = xyz_from_xy(0.1566,0.0177)
        w = xyz_from_xy(0.3457,0.3585)
    
        wide_gamut = ColourSystem(r,g,b,w)
        w_min = 400
        w_max = 700
        w = self.Spectra[:,0]
        
        s = aj.find_nearest(w,w_min)
        e = aj.find_nearest(w,w_max)
        self.Colours = np.zeros((self.Spectra.shape[1]- 1,3))
        for i in range(0,self.Spectra[:,1:].shape[1]):
            arr = self.Spectra[s:e, i]
            arr = savgol_filter(arr, 101, 3)
            arr = aj.min_max(arr)
            self.Colours[i,:] = wide_gamut.spec_to_rgb(w[s:e],arr)
        
        
    def update_intensity_plot(self):
        self.Axes[0].clear()
        self.plot_grid_lines()
        tuples = [(x,y) for x in self.x_points for y in self.y_points]
        temp_image = self.Axes[0].scatter(*zip(*tuples), c = self.Intensities, cmap = self.cmap, s = 80)
        self.Axes[0].invert_yaxis()
       

        self.canvas.draw()
        self.canvas.mpl_connect('motion_notify_event',self.on_hover)
        self.click_event = self.canvas.mpl_connect('button_press_event', self.on_click)
        
        
    def plot_PL_data(self):
        ##replace with actual matrix of data points
        
        tuples = [(x,y) for x in self.x_points for y in self.y_points]

        temp_image = self.Axes[0].scatter(*zip(*tuples), c = self.Intensities, cmap = self.cmap, s = 80);
        self.Axes[1].scatter(*zip(*tuples), c = self.Colours.reshape(-1,3), s = 80)
        self.Axes[0].invert_yaxis()
        self.Axes[1].invert_yaxis()

        self.canvas.draw()
        self.canvas.mpl_connect('motion_notify_event',self.on_hover)
        self.click_event = self.canvas.mpl_connect('button_press_event', self.on_click)
        return
    
    
    def need_update(self):
        if self.index_x_new == self.index_x_old and self.index_y_old == self.index_y_new:
            return False
        else:
            return True
    
    def on_click(self, event):
        print ("We have clicked at x= {0} and y = {1} ".format(self.index_x_new, self.index_y_new))
        w = self.Spectra[:,0]
        w_min = self.WminVar.get()
        w_max = self.WmaxVar.get()
        s = aj.find_nearest(w,w_min)
        e = aj.find_nearest(w,w_max)
        
        well = int(1+self.index_y_new//3*(len(self.x_points)) + self.index_x_new)
        drop = int(self.index_y_new%3 + 1)
        r = re.compile('w'+f'{well:02}' +'d'+ str(drop))
        last_image_path = self.path+self.ex_id+ r'/ef/p01/'+list(filter(r.search, self.file_list))[-1]
        I = self.Spectra[:,well]
        
        if self.experiment_params is not None:
            data = self.experiment_params[self.experiment_params['Well Number'] ==well]
            self.New_Window = secondWindow(self.master, w[s:e], I[s:e], well, drop, last_image_path,data)
            
        else:
            self.New_Window = secondWindow(self.master, w[s:e], I[s:e], well, drop, last_image_path)
        self.master.wait_window(self.New_window.top)
            

        
    def on_hover(self, event):
        x,y = event.xdata, event.ydata
        if event.inaxes == None:
            self.master.config(cursor = "arrow")
            print ("Not in axes!")
            pass
        else:
            self.index_y_old = self.index_y_new
            self.index_x_old = self.index_x_new
            self.index_x_new = np.argmin(np.abs(self.x_points - x))
            self.index_y_new = np.argmin(np.abs(self.y_points - y))
            data = np.array([x,y])
            real_points = np.array([self.x_points[self.index_x_new], self.y_points[self.index_y_new]])
            print (real_points)
            if event.inaxes == self.Axes[0]:
                data = self.Axes[0].transData.transform(data)
                real_points= self.Axes[0].transData.transform(real_points)
            elif event.inaxes ==self.Axes[1]:
                data = self.Axes[1].transData.transform(data)
                real_points= self.Axes[1].transData.transform(real_points)
            diff = np.linalg.norm(data - real_points)
            print (diff)
            if diff < np.sqrt(80):
                self.master.config(cursor = 'hand1')
                self.click_event = self.canvas.mpl_connect('button_press_event', self.on_click)
            else:
                self.master.config(cursor = 'arrow')
                self.canvas.mpl_disconnect(self.click_event)
            if self.need_update():
                #print (self.index_x_new, self.index_y_new)
                print (self.index_x_new, self.index_y_new)
                
        
        
    
    
    
if __name__ == "__main__":
    root = tk.Tk()
    first = MainWindow(root, '')
    root.mainloop()
    def _quit():
        root.quit()
        root.destroy()
        
        