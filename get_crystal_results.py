import data_and_calcs as dac
import numpy as np
import os
import pandas as pd
from test_nn import run_nn
from image_parser import parse_images_from_folder
import re
import sys
from scipy.special import softmax
sys.path.append('/scratch/e/esargent/ajay15')
import DataAnalysis as aj
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

try:
    folder = sys.argv[1]

except IndexError:
    sys.exit('Please enter the folder name as the first argument to the script!')


if not os.path.exists(folder + 'results'):
    os.makedirs(folder + 'results')


image_data = parse_images_from_folder(folder + r'th/p01')
meta_data = image_data[1]
images = image_data[2]
input_images = dac.prepare_data2(((images,),), shuffle = False,fft = False, gs = False, Jeff_dumb = False)[0]

best_net = 'f2d9fa28-35e7-4202-80a5-7deb8ae3f075'
directory = 'Jeff_Code_BGQ/trained_nets/2019-05-06--0054_model_b32_preprocessed_fft4_mse_adam_f1/mydb/motif_initialization/train_models/'

targets = run_nn(input_images[0], directory, best_net, test=False, save=True, ext_data = False, save_dir=folder + 'results/')

csv_file = [x for x in os.listdir(folder) if re.findall(r'.csv', x)][0]

csv_df = pd.read_csv(folder+csv_file)

csv_df.set_index('Well Number', inplace = True)
meta_data['Well Number'] = meta_data['Well'].astype(int)
meta_data['Drop'] = meta_data['Drop'].astype(int)

meta_data['Elapsed Time'] = meta_data['dTime']

meta_data.drop('Well', axis = 1,inplace = True)
meta_data.set_index('Well Number', inplace = True)


meta_data = meta_data.merge(csv_df, on = 'Well Number')
useless_cols = ['dTime','Time','Timestep', 'Batch', 'Antisolvent Volume (uL)','Drop 1 Volume (nL)', 'Drop 2 Volume (nL)','Drop 3 Volume (nL)']


for col in useless_cols:
    meta_data.drop(col, inplace = True, axis = 1)

meta_data['Crystal Likelihood'] = targets
meta_data.sort_values('Crystal Likelihood', ascending = False, inplace = True)


cols = list(meta_data)

cols.insert(0, cols.pop(cols.index('Crystal Likelihood')))
meta_data = meta_data[cols]
meta_data['Crystal Likelihood'] = aj.min_max(meta_data['Crystal Likelihood'])

f, ax = plt.subplots(nrows =1, ncols = 1, figsize = [10,10])
f.tight_layout()

ax.hist(meta_data['Crystal Likelihood'])

aj.figure_quality_axes(ax,'Likelihood of Crystallization','Number of Experiments', folder[0:-1], legend = False)

f.savefig(folder+'results/Experiment_Histogram.png', format = 'png')


meta_data.to_pickle(folder + 'results/crystal_results.pickle')

meta_data.to_csv(folder + 'results/crystal_results.csv')

print ("The best 10 crystals as evaluated by the neural network are: ",  "\n")

print(meta_data.head(10))





