from pymatgen.ext.matproj import MPRester
from pymatgen.symmetry import analyzer
import numpy as np
import pandas as pd


key = 'PGaO0oi7Tz4vaTq8px'



mpr = MPRester(key)

test_struc = mpr.get_structure_by_material_id('mp-600089')


sgn = analyzer.SpacegroupAnalyzer(test_struc).get_space_group_number()

sgs = analyzer.SpacegroupAnalyzer(test_struc).get_space_group_symbol()

flag = True

num_blanks = 0

num = 100
i = 0
df_all = pd.DataFrame()

while flag:

    to_query = ['mp-'+ str(x) for x in range(i+1, i+num)]
    data = mpr.query(chunk_size = 1000,criteria = {'material_id':{'$in':to_query}}, properties = ['material_id','xrd.Cu.pattern','spacegroup'])
    
    space_group = {x['material_id']: [x['spacegroup']] for i,x in enumerate(data)}
    two_theta = {x['material_id']:[np.array(x['xrd.Cu.pattern'])[:,2]] for i,x in enumerate(data)}
    intensity = {x['material_id']:[np.array(x['xrd.Cu.pattern'])[:,0]] for i,x in enumerate(data)}
    peaks = {x['material_id']:[np.array(x['xrd.Cu.pattern'])[:,1]] for i,x in enumerate(data)}

    try:
        df_temp = pd.DataFrame.from_dict(two_theta,orient = 'index', columns = ['two_theta'])
        df_temp['intensity'] = pd.DataFrame.from_dict(intensity,orient = 'index')
        df_temp['spacegroup'] = pd.DataFrame.from_dict(space_group,orient = 'index')
        df_temp['peaks'] = pd.DataFrame.from_dict(peaks,orient = 'index')
    

        df_all = df_all.append(df_temp)

        df_all.to_pickle('All_data.pkl')
    
    
    except ValueError:
        print(intensity)
    
    i +=num
    print (i)
    if data==[]:
        num_blanks+=1
        if num_blanks >1000:
            flag = False
            print('More than 100000 entries without a hit')
    if i > 5e5:
        print ('At a million')
        flag = False





