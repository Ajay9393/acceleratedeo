import json
import numpy as np
import glob
import os
import itertools
import re
import sys
sys.path.append('/scratch/e/esargent/ajay15')
import DataAnalysis as aj

from matplotlib import rc
rc('mathtext', default='regular') 
from matplotlib import rcParams
import matplotlib
import matplotlib.pyplot as plt
rcParams.update({'figure.autolayout':True})
r = []

wd = '1D_XRD_Trained_Nets/'

subdirs = [x[0] for x in os.walk(wd)]


for sub in subdirs:
    temp = ([sub+'/'+ x for x in os.listdir(sub) if re.findall(r'json',x)])
    if temp!=[]:
        r.append(temp)


x = list(itertools.chain.from_iterable(r))


'''
import json
import glob
poss = glob.glob('*.json')
recs = []
for var in poss:
    with open(var) as f:
        data = json.load(f)
        recs.append(data['history']['loss']['val_acc'][-1])
print(max(recs))
'''



losses = []
dense_layers = []
dropout = []
batch_s = []
regularization_const = []
patience = []
act_type = []
loss_rate = []
reduction_type = []
train_acc = []
val_acc = []
train_loss = []
val_loss = []
epoch = []



for term in x:
    with open(term) as f:
        data = json.load(f)
        reduction_type.append(data['param']['model']['reduction_type'])
        loss_rate.append(data['param']['model']['lr'])
        losses.append(data['loss'])
        dense_layers.append(data['param']['model']['no_of_dense_layers'])
        dropout.append(data['param']['model']['d_rate'])
        batch_s.append(data['history']['params']['batch_size'])
        regularization_const.append(data['param']['model']['regularization'])
        patience.append(data['param']['fit']['patience'])
        act_type.append(data['param']['model']['act_type'])
        train_acc.append(data['history']['loss']['acc'])
        val_acc.append(data['history']['loss']['val_acc'])
        train_loss.append(data['history']['loss']['loss'])
        val_loss.append(data['history']['loss']['val_loss']) 
        epoch.append(data['history']['loss']['epoch']) 

idx = np.argsort(losses)
idx = idx[::-1]
losses = np.array(losses)[idx]
dense_layers = np.array(dense_layers)[idx]
dropout = np.array(dropout)[idx]
batch_s = np.array(batch_s)[idx]
regularization_const = np.array(regularization_const)[idx]
loss_rate = np.array(loss_rate)[idx]
reduction_type = np.array(reduction_type)[idx]
act_type = np.array(act_type)[idx]
patience = np.array(patience)[idx]

train_acc = np.array(train_acc)[idx]
train_loss = np.array(train_loss)[idx]
epoch = np.array(epoch)[idx]
val_acc = np.array(val_acc)[idx]
val_loss = np.array(val_loss)[idx]



labels = ['losses','number of hidden layers','dropout value','Batch Size','regularization','loss_rate','reduction_type','act_type']
var = [losses,dense_layers,dropout,batch_s,regularization_const, loss_rate, reduction_type, act_type]
j = int(len(var)/2)
print (j)

f, ax = plt.subplots(nrows = 2, ncols =j , figsize = [20,10])

for i in range(0,2*j):
    print (int(i/j))
    print (int(i%j))
    
    ax[int(i/j), int(i%j)].plot(var[i],'r+')
    aj.figure_quality_axes(ax[int(i/j), int(i%j)], 'Trial',labels[i], '', legend = False)


f.savefig('1D_XRD_Trained_Nets/kopt_analyzer_2.png',format = 'png')

f2, ax2 = plt.subplots(nrows = 1, ncols = 1, figsize = [15,15])


ax2.plot(np.array(epoch[-1]), np.array(train_loss[-1]), label = 'Training')
ax2.plot(np.array(epoch[-1]), np.array(val_loss[-1]), label = 'Validation')


aj.figure_quality_axes(ax2, 'Epoch', 'Loss', 'Best',legend = True)



f2.savefig('1D_XRD_Trained_Nets/Loss_graphs_2.png',format = 'png')


np.savetxt('1D_XRD_Trained_Nets/Epochs.npy',np.array(epoch[-1]))
np.savetxt('1D_XRD_Trained_Nets/train.npy',np.array(train_loss[-1]))
np.savetxt('1D_XRD_Trained_Nets/val.npy',np.array(val_loss[-1]))



