#####################################################################################################
# Robotic crystallization code
# By: Mikhail Askerka, Jeffrey Kirman
#####################################################################################################

#####################################################################################################
# Imports
#########

from __future__ import print_function
import os
#os.environ["CUDA_VISIBLE_DEVICES"] = "3"
import time
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error,mean_squared_error, f1_score, recall_score
import tensorflow as tf
from itertools import product
from scipy.interpolate import griddata
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler,StandardScaler
import h5py
import pickle
from collections import Counter
from itertools import product
import keras
import numpy as np
import tensorflow as tf
from keras.models import Sequential, Model
from keras.utils import plot_model
from keras.layers import Lambda,Dense, Dropout, Activation, Flatten, Input, Lambda, Conv2D, MaxPooling2D
from keras.callbacks import ModelCheckpoint
from keras.regularizers import l2
from keras.utils import multi_gpu_model
import data_and_calcs as dac
import json
import sys
from sklearn import metrics
import ast

#######################################################################################################
# Variables and Setup
#####################

nn_path = '/ess01/homebgq/e/esargent/kirmanje/sargent_ML/ml_code/robotic_crystallization/trained_nets/saved_models_good_crystals_b16_oversample_f1_2019-04-03--1722/mydb/motif_initialization/train_models/'
nn_name = '4c27cb20-250b-44af-8864-4a1273aba916' 

json_file = open(nn_path + nn_name + '.json', 'r')
loaded_model_json = json.loads(json_file.read())
json_file.close()

# User changeable variables
model_name = loaded_model_json['path']['results'].split('/')[-7] + '_solo_reg'
repeat_filts = loaded_model_json['param']['model']['repeat_filts']
lr = loaded_model_json['param']['model']['lr']
batch_size = loaded_model_json['param']['fit']['batch_size']
activ = loaded_model_json['param']['model']['activ']
dense_activ = loaded_model_json['param']['model']['dense_activ']
add_reductive = loaded_model_json['param']['model']['add_reductive']
filt_size = tuple(loaded_model_json['param']['model']['filt_size'])
loss = loaded_model_json['param']['model']['loss']
epochs = 500
mode = 'train'
nstart = loaded_model_json['param']['model']['nstart']
nfilters = loaded_model_json['param']['model']['nfilters']
dropout = loaded_model_json['param']['model']['dropout']
regularization = 0.001
data_path = loaded_model_json['param']['data']['data_paths']
target_path = loaded_model_json['param']['data']['target_paths']
over_sample = loaded_model_json['param']['data']['over_sample']
patience = loaded_model_json['param']['fit']['patience']
mode = 'train'

if len(sys.argv) > 1:
    mode_temp = sys.argv[1]
    if mode_temp == 'test':
        mode = mode_temp
    regularization = ast.literal_eval(sys.argv[2])
    model_name = model_name + '_' + sys.argv[2].replace(".","")

print(regularization)
print(model_name)

# Prepare data
data = dac.prepare_data(data_path, target_path, over_sample)

########################################################################################################
# Build neural net and run
##########################

(x_train, y_train), (x_val, y_val), (x_test, y_test) = data[0], data[1], data[2]

# Create NN model
model = dac.setup_model(train_data=data[0], activ=activ, nstart=nstart, repeat_filts=repeat_filts,
                filt_size=filt_size, add_reductive=add_reductive, nfilters=nfilters, dropout=dropout,
                        regularization=regularization, model_name=model_name, lr=lr, loss=loss, dense_activ=dense_activ)

if mode=='train':
    checkpoint = ModelCheckpoint('model_{0}.hdf5'.format(model_name), monitor='val_acc', verbose=1, save_weights_only=False,save_best_only=False, mode='auto')
    callbacks_list = [checkpoint]
    model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(x_val, y_val), shuffle=True,callbacks=callbacks_list)
if mode == 'test':
    model.load_weights('model_{0}.hdf5'.format(model_name))
    print('Successfully loaded the last model')
    target = model.predict(x_val)
    target_bool = np.greater(target, 0.5)
    y_test_bool = np.greater(y_val, 0.5)
    confusion_matrix = metrics.confusion_matrix(y_test_bool, target_bool)
    f1 = metrics.f1_score(y_test_bool, target_bool)
    precision = metrics.precision_score(y_test_bool, target_bool)
    recall = metrics.recall_score(y_test_bool, target_bool)
    
    print('f1: ' + str(f1))
    print('precision ' + str(precision))
    print('recall ' + str(recall))
