#####################################################################################################
# Imports
#########

from __future__ import print_function
import tensorflow as tf
import keras
import numpy as np
from sklearn import metrics
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, Conv2D
from keras.callbacks import ModelCheckpoint
from keras.regularizers import l2
import data_and_calcs as dac
from hyperopt import hp
import sys
import argparse
import datetime as dt

#######################################################################################################
# Variables and Setup
#####################

# Data paths
data_paths = ['/ess01/scratch2/e/esargent/kirmanje/Experiments/2018-10-02-1/image_data.npy', '/ess01/scratch2/e/esargent/kirmanje/Experiments/2018-10-02-2/image_data.npy']
target_paths = ['/ess01/scratch2/e/esargent/kirmanje/Experiments/2018-10-02-1/labels.npy', '/ess01/scratch2/e/esargent/kirmanje/Experiments/2018-10-02-2/labels.npy']

# Model name
model_name= 'fft_b32_bywell_new_transrot_bce_f1'

# Hyperparameters
repeat_filts = (hp.choice("m_repeat_filts", (2,3,4)), hp.choice("m_repeat_filts2", (2,3,4)))
lr = hp.loguniform("m_lr", np.log(1e-4), np.log(1e-2))
batch_size = 32
batch_size_val = 64
dense_activ = 'sigmoid'
activ = (hp.choice("m_activ", ('relu','selu')), hp.choice("m_activ2", ('relu','selu')))
add_reductive = (True, True)
filt_size = (hp.choice("m_filt_size", ((2,2), (3,3), (4,4))), hp.choice("m_filt_size2", ((2,2), (3,3), (4,4))))
epochs = 300
mode = 'train'
nstart = (hp.choice("m_nstart", (5, 6)), hp.choice("m_nstart2", (5, 6)))
nfilters = (hp.choice("m_nfilters", (8, 16)), hp.choice("m_nfilters2", (8, 16)))
dropout = (hp.choice("m_dropout", (0.0, 0.2, 0.4)), hp.choice("m_dropout2", (0.0, 0.2, 0.4)))
regularization = (hp.choice("m_regularization", (1e-4, 1e-3, 1e-2)), hp.choice("m_regularization2", (1e-4, 1e-3, 1e-2)))
loss = 'binary_crossentropy'
over_sample = False
by_well = True
trans_rot = False
fft = True
trans_px = 7
new_transrot = True
patience = 10

if len(sys.argv) > 1:
    data_path = sys.argv[1]
    target_path = sys.argv[2]
    over_sample = eval(sys.argv[3])
    model_name = sys.argv[4]
    presorted = eval(sys.argv[5])

print('Model Name: ' + model_name)

# Prepare data
#data = dac.prepare_data(data_paths, target_paths, over_sample, by_well=by_well, trans_rot=trans_rot, trans_px=trans_px)
#(x_train, y_train), (x_val, y_val), (x_test, y_test) = data[0], data[1], data[2]

########################################################################################################
# KOPT
######
from hyperopt import fmin, tpe, hp, Trials
hyper_params = {
    "data":{
        "data_paths": data_paths,
        "target_paths": target_paths,
        "over_sample": over_sample,
        "by_well": by_well,
        "trans_rot": trans_rot,
        "trans_px": trans_px,
        "fft": fft,
        "new_transrot": new_transrot
        },
    "model": {
        "repeat_filts": repeat_filts,
        "lr": lr,
        "activ": activ,
        "dense_activ": dense_activ,
        "add_reductive": add_reductive,
        "filt_size": filt_size,
        "nstart": nstart,
        "nfilters": nfilters,
        "dropout": dropout,
        "regularization": regularization,
        "loss": loss,
        "model_name": model_name,
        },
    "fit": {
        "epochs": epochs,
        "patience": patience,
        "batch_size": batch_size,
        "batch_size_val": batch_size_val
    }
}

from kopt import CompileFN
objective = CompileFN(db_name="mydb", exp_name="motif_initialization",  # experiment name
    data_fn = dac.prepare_data,
    model_fn = dac.setup_model_with_fft, 
    add_eval_metrics = ["mse", dac.f1], # metrics from concise.eval_metrics, you can also use your own
    optim_metric = "f1", # which metric to optimize for
    optim_metric_mode = "max", # maximum should be searched for
    valid_split = None, # use valid from the data function
    save_model = 'best', # checkpoint the best model
    save_results = True, # save the results as .json (in addition to mongoDB)
    save_dir = "./trained_nets/{1}_model_{0}/".format(model_name,dt.datetime.now().strftime('%Y-%m-%d--%H%M')))  # place to store the models
trials = Trials()
best = fmin(objective, hyper_params, trials=trials, algo=tpe.suggest, max_evals=100)
