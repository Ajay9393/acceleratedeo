import numpy as np

old_array_loc = '/scratch/e/esargent/kirmanje/12It52Xi99YAU/labels_array.npy'
new_array_loc = '/scratch/e/esargent/kirmanje/12It52Xi99YAU/labels_array-2.npy'
per_cycle = 11
per_ind = 3

new_array = np.array([])
old_array = np.load(old_array_loc)
shape = old_array.shape[1]
full_cycle = (shape/per_ind) / per_cycle
extras = (shape/per_ind) - per_cycle*full_cycle

if extras > 0:
	full_cycle = full_cycle + 1
for i in range(0,full_cycle):
	i_c = i*per_cycle*per_ind
	if new_array.shape[0] == 0:
		new_array = old_array[:,i_c:i_c+per_ind]
	else: 
		new_array = np.concatenate((new_array, old_array[:,i_c:i_c+per_ind]), axis=1)

for i in range(0,shape/per_ind):
	i_i = i*per_ind
	if i % per_cycle != 0:
		new_array = np.concatenate((new_array, old_array[:,i_i:i_i+per_ind]), axis=1)

np.save(new_array_loc,new_array)
