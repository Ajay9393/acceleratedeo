#####################################################################################################
# Robotic crystallization code
# By: Mikhail Askerka, Jeffrey Kirman
#####################################################################################################

#####################################################################################################
# Imports
#########

from __future__ import print_function
import os
#os.environ["CUDA_VISIBLE_DEVICES"] = "3"
import time
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error,mean_squared_error, f1_score, recall_score
import tensorflow as tf
from itertools import product
from scipy.interpolate import griddata
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler,StandardScaler
import h5py
import pickle
from collections import Counter
from itertools import product
import keras
import numpy as np
import tensorflow as tf
from keras.models import Sequential, Model
from keras.utils import plot_model
from keras.layers import Lambda,Dense, Dropout, Activation, Flatten, Input, Lambda, Conv2D, MaxPooling2D
from keras.callbacks import ModelCheckpoint
from keras.regularizers import l2
from keras.utils import multi_gpu_model
import data_and_calcs as dac

#######################################################################################################
# Variables and Setup
#####################

# User changeable variables
model_name= 'cnn_test'
repeat_filts = 6
lr=1e-4
batch_size = 16
activ = 'relu'
add_reductive = True
filt_size = (2,2)
epochs = 200 #500
mode = 'train'
nstart = 6
nfilters = 16
dropout = 0.2
regularization = 1e-2
observable='Gap_GGA_SOC'
data_path = '/scratch/e/esargent/kirmanje/12It52Xi99YAU/compiled_array.npy'
target_path = '/scratch/e/esargent/kirmanje/12It52Xi99YAU/labels_array-2.npy'
over_sample = True
patience = 10
mode = 'train'

# Setup tensorflow
config = tf.ConfigProto()
config.gpu_options.allow_growth=True
config.gpu_options.allocator_type ='BFC'
config.gpu_options.per_process_gpu_memory_fraction = 0.8

# Prepare data
data = dac.prepare_data(data_path, target_path, over_sample)

########################################################################################################
# Build neural net and run
##########################

(x_train, y_train), (x_val, y_val), (x_test, y_test) = data[0], data[1], data[2]

# Create NN model
model = dac.setup_model(train_data=data[0], activ=activ, nstart=nstart, repeat_filts=repeat_filts,
                filt_size=filt_size, add_reductive=add_reductive, nfilters=nfilters, dropout=dropout,
                        regularization=regularization, model_name=model_name, lr=lr, config=config)

if mode=='train':
    checkpoint = ModelCheckpoint('model_{0}.hdf5'.format(model_name), monitor='val_acc', verbose=1, save_weights_only=True,save_best_only=False, mode='auto')
    callbacks_list = [checkpoint]
    model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(x_val, y_val), shuffle=True,callbacks=callbacks_list)
if mode == 'test':
    try:
       model.load_weights('model_{0}.hdf5'.format(model_name))
       print('Successfully loaded the last model')
       data_screen = np.load(target_path)
       target = model.predict(data_screen.reshape(list(data_screen.shape) + [1]))
    except:
       print('Could not load the model')
