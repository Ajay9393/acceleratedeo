# -*- coding: utf-8 -*-
"""
Created on Sat May  4 00:48:42 2019

@author: Jeffrey
"""

import numpy as np
import os

def make_labels(label_path_list):
    idxs = []
    labels = []
    for j, path in enumerate(label_path_list):
        files = os.listdir(path)
        for i in range(len(files)):
            if files[i].split('.')[-1] == 'jpg':
                files[i] = int(files[i].split('_')[0])
            else:
                files.pop(i)
        idxs.append(np.asarray(files).astype(np.int))
        labels.append(np.full_like(idxs[j],j))
    
    idxs = np.concatenate(idxs)
    labels = np.concatenate(labels)
    
    return labels[np.argsort(idxs)]
        
#    for folder in exp_folders:
#        files = os.listdir(exp_path + '/' + folder + '/')
#        if ('image_data.npy' not in files) or overwrite_all:
#            (df, df_meta_data, image_data) = parse_images_from_folder(exp_path + '/' + folder + '/' + 'Images')
#            np.save(exp_path + '/' + folder  + '/' + 'image_data.npy', image_data)
#            pickle.dump(df_meta_data, open(exp_path + '/' + folder  + '/' + 'image_meta_data.pickle', "wb"))