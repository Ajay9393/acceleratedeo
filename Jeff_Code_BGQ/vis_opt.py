import pandas as pd
import os
import json
import numpy as np
import matplotlib; matplotlib.use('agg');import matplotlib.pyplot as plt

def consolidate_nn_data(path, save_fig=True, print_results=False):

    model_files = [ i for i in os.listdir(path) if '.json' in i]
    model_files.sort(key=lambda s: os.path.getmtime(os.path.join(path, s)))
    with open(path+model_files[0], 'rb') as f:
        data = json.load(f)
    model_keys = [key + '_model' for key in data['param']['model'].keys()]
    columns = model_keys + list(data['eval'].keys()) + ['name']
    
    df_results = pd.DataFrame(np.zeros([len(model_files), len(columns)])*np.nan, columns = columns)
#    if data['param']['data']['fft']:
#        fft_df = df_results.copy()

    for i_mod,mod in enumerate([ i for i in os.listdir(path) if '.json' in i]):
        with open(path+mod, 'rb') as f:
            data = json.load(f)
        new_filt_size = []
#        for i in range(len(data['param']['model']['filt_size'])):
#            new_filt_size.extend([data['param']['model']['filt_size'][i][0]])
###        data['param']['model']['filt_size'] = new_filt_size
        model_df = pd.DataFrame(data['param']['model']) 
        model_df.rename(columns=lambda x: x+'_model', inplace=True)
        #model_df.drop_duplicates(inplace=True)
        df_results.loc[i_mod,model_keys] = model_df.iloc[0]
        df_results.loc[i_mod,data['eval'].keys()] = pd.DataFrame(data['eval'],index=[0]).iloc[0]
        df_results.loc[i_mod,'name'] = data['path']['model']
        
#        if data['param']['data']['fft']:
#            fft_df.loc[i_mod,model_keys] = model_df.iloc[1]

    df_results['log(loss)'] = np.log(df_results['loss'])/np.log(10)
    df_results['log(mse)'] = np.log(df_results['mse'])/np.log(10)
    
    if print_results:
        print(df_results.sort_values(['loss']).iloc[:])
        best_ind = df_results.sort_values(['mse']).index[0]
        print(df_results.iloc[best_ind])
        print(df_results.iloc[best_ind]['name'])

    if save_fig:    
        plt.close('all')
        fig = df_results.plot(kind='line',ms=2,marker='o', lw=0,fontsize = 6,legend=True,subplots=True,layout=(4,4))
        [ii.legend(fontsize=7,loc='upper center',bbox_to_anchor=(0.5,1.3 )) for i,ii in enumerate(fig.ravel()[:])]
        plt.tight_layout()
        plt.savefig('parameter_opt_{0}.png'.format(path.split('/')[-5]), dpi=300)

    return df_results

#path = '/home/e/esargent/kirmanje/sargent_ML/ml_code/robotic_crystallization/trained_nets/2019-05-05--0254_model_b32_preprocessed_mse_adam_f1/mydb/motif_initialization/train_models/'
#df_results = consolidate_nn_data(path, True, False)
