#####################################################################################################
# Robotic crystallization code
# By: Mikhail Askerka, Jeffrey Kirman
#####################################################################################################

#####################################################################################################
# Imports
#########

from __future__ import print_function
import tensorflow as tf
import keras
import numpy as np
from sklearn import metrics
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, Conv2D
from keras.callbacks import ModelCheckpoint
from keras.regularizers import l2
import data_and_calcs as dac
import sys
import argparse
import datetime as dt
import pickle as pkl
import json

#######################################################################################################
# Variables and Setup
#####################

nn_path = '/ess01/homebgq/e/esargent/kirmanje/sargent_ML/ml_code/robotic_crystallization/trained_nets/2019-05-05--0254_model_b32_preprocessed_mse_rmsprop_f1/mydb/motif_initialization/train_models/'
nn_name = 'b4f0ab59-0ca3-4b3d-9cfd-ff741028eb88'
data_path = '/scratch/e/esargent/kirmanje/Training/60000-best-consolidated.pkl'


json_file = open(nn_path + nn_name + '.json', 'r')
loaded_model_json = json.loads(json_file.read())
json_file.close()

# User changeable variables
model_name = loaded_model_json['path']['results'].split('/')[-7] + '_solo_best'
lr = loaded_model_json['param']['model']['lr']
batch_size = loaded_model_json['param']['fit']['batch_size']
loss = loaded_model_json['param']['model']['loss']
epochs = 500
mode = 'train'
m_json = loaded_model_json['param']['model']
nstart = m_json['nstart']
regularization = m_json['regularization']
repeat_filts = m_json['repeat_filts']
dropout = m_json['dropout']
activ = m_json['activ']
add_reductive = m_json['add_reductive']
nfilters = m_json['nfilters']
filt_size = tuple(m_json['filt_size'])
lr = m_json['lr']
try:
    over_sample = loaded_model_json['param']['data']['over_sample']
except:
    over_sample = False
try:
    data_paths = loaded_model_json['param']['data']['data_paths']
except:
    data_paths = ''
try:
    target_paths = loaded_model_json['param']['data']['target_paths']
except:
    target_paths = ''
try:
    fft = loaded_model_json['param']['data']['fft']
except:
    fft = False
try:
    dense_activ = m_json['dense_activ']
except:
    dense_activ = None

try:
    loss = m_json['loss']
except:
    loss = 'mse'

try:
    by_well=loaded_model_json['param']['data']['by_well']
except:
    by_well=False

try:
    trans_rot=loaded_model_json['param']['data']['trans_rot']
except:
    trans_rot=False

try:
    trans_px=loaded_model_json['param']['data']['trans_px']
except:
    trans_px=2

try:
    pre_concat=m_json['pre_concat']
except:
    pre_concat=10

try:
    gs = loaded_model_json['param']['data']['gs']
except:
    gs = False

try:
    optimizer = m_json['optimizer']
except:
    optimizer='adam'

# Prepare data
data = pkl.load(open(data_path,'rb'))
data2 = dac.prepare_data2(data,fft,gs)

########################################################################################################
# Build neural net and run
##########################

(x_train, y_train), (x_val, y_val), (x_test, y_test) = data2[0], data2[1], data2[2]

# Create NN model
model = dac.setup_model_functional(train_data=data[0], activ=activ, nstart=nstart, repeat_filts=repeat_filts,
                filt_size=filt_size, add_reductive=add_reductive, nfilters=nfilters, dropout=dropout,
                regularization=regularization, model_name=model_name, lr=lr, 
                loss=loss, dense_activ=dense_activ, fft=fft, pre_concat=pre_concat, optimizer=optimizer)

if mode=='train':
    checkpoint = ModelCheckpoint('model_{0}.hdf5'.format(model_name), monitor='val_acc', verbose=1, save_weights_only=False,save_best_only=False, mode='auto')
    csv = keras.callbacks.CSVLogger('model_{0}.csv'.format(model_name))
    callbacks_list = [checkpoint, csv]
    model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(x_val, y_val), shuffle=True,callbacks=callbacks_list,verbose=2)
if mode == 'test':
    try:
        model.load_weights(nn_path + nn_name +'.hdf5')
    except:
        model.load_weights(nn_path + nn_name +'.h5')
    print('Successfully loaded the last model')
    
    for i in range(1,3):
        target = model.predict(data[i][0])
        target_bool = np.greater(target, 0.5)
        y_test_bool = np.greater(data[i][1], 0.5)
        confusion_matrix = metrics.confusion_matrix(y_test_bool, target_bool)
        f1 = metrics.f1_score(y_test_bool, target_bool)
        precision = metrics.precision_score(y_test_bool, target_bool)
        recall = metrics.recall_score(y_test_bool, target_bool)
    
        print('f1: ' + str(f1))
        print('precision ' + str(precision))
        print('recall ' + str(recall))
