# -*- coding: utf-8 -*-
"""
Created on Tue Sep 04 11:30:03 2018

@author: ak4jo
"""

import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
import numpy as np
import pickle
import re
import os
##Name of worksheet for which the parameters should be extracted as well as the ID corresponding to the image folder
worksheet_title = '2018-07-10-2'
tray_key = '12It52Xi99YAU'
image_directory = r'/scratch/e/esargent/ajay15/Machine_Learning/Images/' + tray_key + r'/'
dict_info = pickle.load(open(image_directory+r'/' + 'dict_info'+'.pickle','rb'))

list_of_folders = dict_info['Folders']

number_time_steps = len(list_of_folders)

##Get access to the google sheets document
scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']


##Note: .json file needs to be in same folder as this code
creds = ServiceAccountCredentials.from_json_keyfile_name('Robotic Perovskites-598ce0c18724.json', scope)
gc = gspread.authorize(creds)
sheet = gc.open("Robotic_Feature_Template").worksheet(worksheet_title)

#Extract all cells into a dataframe
df = pd.DataFrame(sheet.get_all_records())

##Formatting: get volumes of three drops and remove column from array
def data_transformmer(df):
    
    list_of_B = ['Pb','Sn']
    list_of_X = ['I','Br','Cl']
    drop_volumes = [int(x) for x in df.loc[:]['Drop Volumes'] if x !=""]
    df.drop('Drop Volumes', axis = 1, inplace = True)

    drop_array = np.reshape(np.tile(drop_volumes,len(df.index)), [len(df.index)*3,1])

    ##Create array that duplicates each row (well number) 3 times, and adds the corresponding drop volume to it
    ##This takes the 96 wells and expands it to 288 subwells
    combined = np.concatenate((np.repeat(df.values,3,axis=0),drop_array),axis = 1)
    columns_list = [x for x in df.columns]
    columns_list.append('Drop Volumes')
    compiled_df = pd.DataFrame(combined, columns = columns_list)
    
    #Convert all "objects" to int64 or float, if possible
    compiled_df = compiled_df.apply(pd.to_numeric, errors = 'ignore')
    compiled_df.dropna(axis = 0, inplace = True)

    #Check to see how many concentraions are listed
    concentration_flag = re.compile('Concentration.+')

    number_concentrations = len([concentration_flag.match(x) for x in df.columns if concentration_flag.match(x) is not None])
    
    elements = re.split(",",compiled_df.loc[0]['Precursor Elements'])
    number_elements = len(elements)
    if number_concentrations != number_elements:
        print "Check the spreadsheet: there isn't one concentration for each element!"

    cleaned_elements = [re.sub(r'(\s+)?(\d+)?',r'',x) for x in elements]
    Elements_B = [x for x in cleaned_elements if x in list_of_B]
    Elements_X = [x for x in cleaned_elements if x in list_of_X]
    Elements_A = [x for x in cleaned_elements if x not in Elements_B and x not in Elements_X]
    
    print "There are {0} A elements, {1} B elements and {2} X elements in this tray".format(\
            len(Elements_A), len(Elements_B), len(Elements_X))


    ##Add dummy concentration columns if necessary, currently takes 3 A, 2 B and 3 X
    for i in range(0, 3 - len(Elements_X)):
       compiled_df['Concentration X' + str(i+2)] = np.zeros(len(compiled_df.index), dtype = float)
    for i in range(0, 3 - len(Elements_A)):
       compiled_df['Concentration A' + str(i+2)] = np.zeros(len(compiled_df.index), dtype = float)
    for i in range(0, 2 - len(Elements_B)):
       compiled_df['Concentration B' + str(i+2)] = np.zeros(len(compiled_df.index), dtype = float)
    
    ##Add elements columns, then delete the 'Precursor Element' columns

    for i in range(0, len(Elements_A)):
        compiled_df['A' + str(i+1)] = Elements_A[i]
    for i in range(0, len(Elements_B)):
        compiled_df['B' + str(i+1)] = Elements_B[i]
    for i in range(0, len(Elements_X)):
        compiled_df['X' + str(i+1)] = Elements_X[i] 

    df.drop('Precursor Elements', axis = 1, inplace = True)
    
    ##Now expand to multiple time steps

    temp_array = compiled_df.values
    extended_array = np.tile(temp_array, (1,number_time_steps))

    extended_df = pd.DataFrame(extended_array, index = range(0,len(compiled_df.index)), \
            columns = pd.MultiIndex.from_product([[os.path.basename(x) for x in list_of_folders],compiled_df.columns],names = ['Time','Parameter']))


    extended_df = extended_df.apply(pd.to_numeric, errors = 'ignore')

    return extended_df


compiled_df = data_transformmer(df)
compiled_df.to_pickle(image_directory+ 'features.pickle')

