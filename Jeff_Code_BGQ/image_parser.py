# -*- coding: utf-8 -*-
"""
Created on Tue Feb 12 14:34:44 2019

@author: Jeffrey
Adapted from code by Andrew Johnston
"""

import os.path
import numpy as np
import imageio
import pandas as pd
import re
import sys
import pickle
from datetime import datetime
import ast
import cv2 as cv
import os

def find_circle_hughes(src):
    r_th = 70
    r_var = 5    
    c_tl_x = 77
    c_tl_y = 57
    c_l = 48
    c_h = 36
    
    radius = r_th
    center = (int(c_tl_x + c_l/2), int(c_tl_y + c_h/2))
    
    gray = cv.cvtColor(src, cv.COLOR_BGR2GRAY)   
    gray = cv.medianBlur(gray, 5)  
    rows = gray.shape[0]
    
    circles = cv.HoughCircles(gray, cv.HOUGH_GRADIENT, 1, rows / 8,
                               param1=100, param2=30,
                               minRadius=r_th-r_var, maxRadius=r_th+r_var)
    
    if circles is not None:
        circles = np.uint16(np.around(circles))
        for i in circles[0, :]:
            if ((i[0], i[1])>(c_tl_x,c_tl_y) and (i[0], i[1])<(c_tl_x+c_l,c_tl_y+c_h)):
                center = (i[0], i[1])
                radius = i[2]
    
    return radius, center

def prepare_images(images, random_noise=False, cut=False):
    
    prepared_images=[]
    
    for src in images:
        radius, center = find_circle_hughes(src)
        
        mask = np.zeros_like(src[...,0]).astype(np.uint8)
        cv.circle(mask, center, radius, (255,255,255), thickness=-1)
        
        if cut:
            masked_data = cv.bitwise_or(src, src, mask=mask)
        else:
            masked_data = src
        
        if random_noise:
            noise_r = np.random.randint(180,195,size=src[...,0].shape).astype(np.uint8)
            noise_g = np.random.randint(180,195,size=src[...,0].shape).astype(np.uint8)
            noise_b = np.random.randint(180,195,size=src[...,0].shape).astype(np.uint8)
            noise = np.stack([noise_r,noise_g,noise_b], axis=-1)
            noise = cv.bitwise_or(noise, noise, mask=cv.bitwise_not(mask))
        else:
            noise = np.zeros_like(src)
        
        noise_and_data = cv.bitwise_or(noise, masked_data)
        
        px_l = max([0, center[0]-75])
        masked_data_cut = noise_and_data[:,px_l:min([px_l+150,200]),:]
        
        prepared_images.append(masked_data_cut)
        
    return prepared_images

def parse_images_from_folder(img_path, trim=True, random_noise=False, cut=False):

    img_names = os.listdir(img_path)
    img_names.sort()

    df = pd.DataFrame({'Well':[], 'Drop':[], 'Time':[], 'dTime': [], 'Timestep':[], 'Batch':[], 'Image':[]}, columns=['Well', 'Drop', 'Time', 'dTime', 'Timestep', 'Batch', 'Image'])
    
    current_drop = 0
    current_img_num = 0
    current_date_time = 0
    for name in img_names:
        print (name)
        values = re.split('[_]?[dtbwde]', name)
        date = re.split('-', values[1])
        date = datetime(int(date[0]), int(date[1]), int(date[2]), int(values[2][0:2]), int(values[2][2:4]))
        batch = int(values[3])
        well = int(values[4])
        drop = int(values[5])
        if current_drop != drop:
            current_drop = drop
            current_img_num = int(values[0])
            current_date_time = date
        timestep = int(values[0]) - current_img_num
        dTime = date - current_date_time
        try:
            image = [imageio.imread(img_path + '/' + name)]
        except ValueError:
            print ("Corrupted image!", name)
            continue
        if trim:
            image = prepare_images(image, random_noise, cut)
        df = df.append({'Well': well, 'Drop': drop, 'Time': date, 'dTime': dTime, 'Timestep': timestep, 'Batch': batch, 'Image':image[0]}, ignore_index = True)
    
    df_meta_data = df.loc[:,'Well':'Batch']
    image_data = np.stack(df.loc[:,'Image'].values)
    
    return (df, df_meta_data, image_data)

def parse_csv(csv_path, all_DFT=True):
    csv_df = pd.read_csv(csv_path)
    dft_df = load_DFT()
    A_df = pd.DataFrame(np.eye(4,3), index=['Cs','FA','MA','N/A'], columns=['Cs','FA','MA'])
    B_df = pd.DataFrame(np.eye(4,3), index=['Pb','Sn','Sb','N/A'], columns=['Pb','Sn','Sb'])
    X_df = pd.DataFrame(np.eye(4,3), index=['Cl','Br','I','N/A'], columns=['Cl','Br','I'])
    
    csv_dft_df = csv_df.copy()
    
    name_list = ['A-Ligand', 'A', 'B', 'X', 'Antisolvent']
    
    for name in name_list:
        if name == 'A' and not all_DFT:
            feature_df = A_df
        elif name == 'B' and not all_DFT:
            feature_df = B_df
        elif name == 'X' and not all_DFT:
            feature_df = X_df
        else:
            feature_df = dft_df
        
        temp_df = pd.DataFrame()
        series_name = csv_dft_df.loc[:,name].values
        if name != 'Antisolvent':
            series_conc = csv_dft_df.loc[:,'Concentration ' + name + ' (M)'].values
        else:
            series_name = "[\'" + series_name + "\']"
            series_conc = np.full(series_name.shape,str([1]))
        for i in range(len(series_name)):
            s_name_list = ast.literal_eval(series_name[i])
            s_conc_list = ast.literal_eval(series_conc[i])
            
            temp_df_well = feature_df.iloc[0].copy()
            temp_df_well[:] = 0
            
            for k in range(len(s_name_list)):
                if not(feature_df.index.contains(s_name_list[k])):
                    s_name_list[k] = 'N/A'
                temp_df_well = temp_df_well + feature_df.loc[s_name_list[k]] * s_conc_list[k]
                
            if name != 'Antisolvent':
                csv_dft_df.loc[i,'Concentration ' + name + ' (M)'] = sum(s_conc_list)
            
            if s_name_list[k] == 'N/A':
                total_conc = 1
            else:
                total_conc = sum(s_conc_list)                
            
            temp_df_well = temp_df_well / total_conc
            temp_df = temp_df.append(temp_df_well, ignore_index=True, sort=False)
            
        temp_df.columns = (name + ' ') + temp_df.columns
        csv_dft_df = csv_dft_df.drop(columns=[name])
        csv_dft_df = pd.concat([csv_dft_df, temp_df], axis=1, sort=False)
        
    return csv_dft_df

def load_DFT(single_file = True):
    
    if not(single_file):
        dft_path = './DFT'
        f_names = os.listdir(dft_path)
        dft_dict = {}
        for f_name in f_names:
            f = open(dft_path + '/' + f_name, 'rb')
            element = re.split('\.', f_name)[0]
            if sys.version_info[0] > 2:
                dft_dict[element] = pickle.load(f, encoding='latin1')
            else:
                dft_dict[element] = pickle.load(f)

        na_values = np.zeros((1,len(dft_dict[element])))
    
        dft_df = pd.DataFrame(dft_dict).T
        dft_df = dft_df.append(pd.DataFrame(na_values, ['N/A'], dft_dict[element].keys()), sort=False)
    else:
        dft_path = 'All_DFT.pickle'
        f = open(dft_path, 'rb')
        if sys.version_info[0] > 2:
            dft_df = pickle.load(f, encoding='latin1')
        else:
            dft_df = pickle.load(f)

        na_values = np.zeros((1,dft_df.iloc[0].size))
        dft_df = dft_df.append(pd.DataFrame(na_values, ['N/A'], dft_df.iloc[0].keys()), sort=False)
    
    f.close()
    
    return dft_df

def seperate_drops(csv_dft_df):
    drop_names = ['Drop 1 Volume (nL)', 'Drop 2 Volume (nL)', 'Drop 3 Volume (nL)']
    dfs = []
    for i in range(0,3):
        df = csv_dft_df.drop(['Well Number'] + drop_names[0:3][:i] + drop_names[0:3][i+1:], axis=1)
        df.rename(columns={drop_names[i]:'Drop Volume (nL)'}, inplace=True)
        new_idx = np.arange(i,288,3)
        idx_map = dict(zip(np.arange(0,96), new_idx))
        df.rename(index=idx_map, inplace=True)
        dfs.append(df)
        
    return pd.concat(dfs).sort_index()

def generate_fingerprint(df_meta_data, csv_dft_df, crystals, dTime=True):
    
    well_drop = df_meta_data.loc[:,'Well':'Drop'].values
    well_drop_idx = (well_drop[:,0]-1)*3 + well_drop[:,1]-1
    well_drop_legend = df_meta_data.loc[:,'Well':'Drop']
 
    if dTime:
        exp_data = seperate_drops(csv_dft_df).loc[well_drop_idx]
        df_meta_data.index = list(df_meta_data.index)
        exp_data.index = df_meta_data.index
        fingerprint = pd.concat([exp_data, df_meta_data.loc[:,'dTime'], pd.DataFrame(crystals==1, columns=['Crystal'])], axis=1, join_axes=[exp_data.index])
    else:
        crystals_df = pd.DataFrame(crystals, index=well_drop_idx, columns=['Crystal'])
        well_drop_legend = well_drop_legend.drop_duplicates()
        single_drop_crystal = []
        for i in range(0,well_drop_idx[-1].astype(np.int32)+1):
            single_drop_crystal.append(np.any(crystals_df.loc[i].values))
        
        fingerprint = pd.concat([seperate_drops(csv_dft_df), pd.DataFrame(single_drop_crystal, columns=['Crystal'])], axis=1)   
    
    return (well_drop_legend, fingerprint)

def generate_fingerprints_for_all_experiments(exp_path, overwrite_all=False, overwrite_images=False):
    generate_image_data_for_all_experiments(exp_path, overwrite_images)
    exp_folders = os.listdir(exp_path)
    for folder in exp_folders:
        files = os.listdir(exp_path + '/' + folder + '/')
        print(folder + '...', end='')
        if (('fingerprint_with_time.pickle' not in files) or overwrite_all) and ('labels.npy' in files) and (folder+'.csv' in files):
            (df, df_meta_data, image_data) = parse_images_from_folder(exp_path + '/' + folder + '/' + 'Images')
            np.save(exp_path + '/' + folder  + '/' + 'image_data.npy', image_data)
            csv_dft_df = parse_csv(exp_path + '/' + folder  + '/' + folder + '.csv')
            crystals = np.load(exp_path + '/' + folder + '/' + 'labels.npy').flatten('F')
            (wdl, fp) = generate_fingerprint(df_meta_data, csv_dft_df, crystals, dTime=True)
            pickle.dump( wdl, open( exp_path + '/' + folder + '/' + "well_drop_legend_with_time.pickle", "wb" ) )
            pickle.dump( fp, open( exp_path + '/' + folder + '/' + "fingerprint_with_time.pickle", "wb" ) )
            
            (wdl, fp) = generate_fingerprint(df_meta_data, csv_dft_df, crystals, dTime=False)
            pickle.dump( wdl, open( exp_path + '/' + folder + '/' + "well_drop_legend_no_time.pickle", "wb" ) )
            pickle.dump( fp, open( exp_path + '/' + folder + '/' + "fingerprint_no_time.pickle", "wb" ) )
        print('Done!')

def generate_image_data_for_all_experiments(exp_path, overwrite_all=False, random_noise=False, cut=False):
    exp_folders = os.listdir(exp_path)
    for folder in exp_folders:
        print(folder)
        files = os.listdir(exp_path + '/' + folder + '/')
        if ('image_data.npy' not in files) or overwrite_all:
            (df, df_meta_data, image_data) = parse_images_from_folder(exp_path + '/' + folder + '/' + 'Images', random_noise=random_noise, cut=cut)
            np.save(exp_path + '/' + folder  + '/' + 'image_data.npy', image_data)
            pickle.dump(df_meta_data, open(exp_path + '/' + folder  + '/' + 'image_meta_data.pickle', "wb"))
           
def generate_chemical_space(csv_dft_df):
    df = csv_dft_df.sort_values('Concentration B (M)').round(8)
    df.drop_duplicates(['A-Ligand Fermi Energy','A Fermi Energy','B Fermi Energy','X Fermi Energy','Antisolvent Fermi Energy'], keep='last', inplace=True)
    dfs = []
    for i in np.arange(0.0125,1,0.0125):
        for dmso in np.arange(0,1.01,0.05):
            df_temp = df.copy()
            df_temp['Concentration B (M)'] = i
            df_temp['Ratio DMSO'] = dmso
            df_temp['Ratio DMF'] = 1-dmso
            df_temp['Ratio GBL'] = 0
            dfs.append(df_temp)
    dfs = pd.concat(dfs, ignore_index=True)
    return seperate_drops(dfs)

if __name__ == "__main__":
#   img_path = 'C:/Users/Jeffrey/Desktop/Plate 1211 YG/th/p01'
#   img_path2 = 'C:/Users/Jeffrey/Desktop/Ims/th/p01'
#   csv_path =  'C:/Users/Jeffrey/Documents/GitLab/sargent_ML/ml_code/robotic_crystallization/test.csv'
#   csv_path2 = 'C:/Users/Jeffrey/Desktop/2018-10-02-2.csv'
#   crystals2 = 'C:/Users/Jeffrey/Desktop/labels_array-2.npy'
#   
#   img_path_bgq = '/ess01/scratch2/e/esargent/kirmanje/Experiments/2018-10-02-2/Images'
#   csv_path_bgq = '/ess01/scratch2/e/esargent/kirmanje/Experiments/2018-10-02-2/2018-10-02-2.csv'
#   crystals_bgq = '/ess01/scratch2/e/esargent/kirmanje/Experiments/2018-10-02-2/labels.npy'
#   
#   (df, df_meta_data, image_data) = parse_images_from_folder(img_path_bgq)
#   csv_dft_df = parse_csv(csv_path_bgq)
#   crystals = np.load(crystals_bgq).flatten('F')
#   (_,fingerprint_with_time) = generate_fingerprint(df_meta_data, csv_dft_df, crystals, dTime=True)
#   (_,fingerprint_no_time) = generate_fingerprint(df_meta_data, csv_dft_df, crystals, dTime=False)
   
   exp_path = '/ess01/scratch2/e/esargent/kirmanje/Experiments/'
#   generate_fingerprints_for_all_experiments(exp_path, overwrite_all=False)
