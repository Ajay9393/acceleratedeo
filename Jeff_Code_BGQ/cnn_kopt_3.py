#####################################################################################################
# Imports
#########

from __future__ import print_function
import tensorflow as tf
import keras
import numpy as np
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler,StandardScaler
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, Conv2D
from keras.callbacks import ModelCheckpoint
from keras.regularizers import l2
import data_and_calcs as dac
from hyperopt import hp
import sys
import argparse

#######################################################################################################
# Variables and Setup
#####################

# Data paths
#data_path = '/scratch/e/esargent/kirmanje/12It52Xi99YAU/compiled_array.npy'
#target_path = '/scratch/e/esargent/kirmanje/12It52Xi99YAU/labels_array-2.npy'
data_path = '/ess01/scratch2/e/esargent/kirmanje/Training/training_data.npy'
target_path = '/ess01/scratch2/e/esargent/kirmanje/Training/training_labels.npy'

# Model name
model_name= 'exp2_train_exp1_val_b16_bce_f1'
save_path = '/home/e/esargent/kirmanje/sargent_ML/ml_code/robotic_crystallization/trained_nets/split_data_set/'

# Hyperparameters
repeat_filts = hp.choice("m_repeat_filts", (4,5,6))
lr = hp.loguniform("m_lr", np.log(1e-4), np.log(1e-2))
batch_size = 32
batch_size_val = 64
dense_activ = 'sigmoid'
activ = hp.choice("m_activ", ('relu','selu'))
add_reductive = True
filt_size = hp.choice("m_filt_size", ((2,2), (3,3), (4,4)))
epochs = 300
mode = 'train'
nstart = hp.choice("m_nstart", (5, 6))
nfilters = hp.choice("m_nfilters", (8, 16))
dropout = hp.choice("m_dropout", (0.0, 0.2, 0.4))
regularization = hp.choice("m_regularization", (1e-4, 1e-3, 1e-2))
loss = 'binary_crossentropy'
over_sample = False
patience = 10

if len(sys.argv) > 1:
    data_path = sys.argv[1]
    target_path = sys.argv[2]
    over_sample = eval(sys.argv[3])
    model_name = sys.argv[4]
    presorted = eval(sys.argv[5])

print('Model Name: ' + model_name)

# Prepare data
data = dac.prepare_data(data_path, target_path, over_sample)
(x_train, y_train), (x_val, y_val), (x_test, y_test) = data[0], data[1], data[2]

def prep_data():
    train_data = np.load('/scratch/e/esargent/kirmanje/Experiments/2018-10-02-2/image_data.npy')
    train_labels = np.load('/scratch/e/esargent/kirmanje/Experiments/2018-10-02-2/labels.npy')
    val_data = np.load('/scratch/e/esargent/kirmanje/Experiments/2018-10-02-1/image_data.npy')
    val_labels = np.load('/scratch/e/esargent/kirmanje/Experiments/2018-10-02-1/labels.npy')

    # Prepare data (X) and target (y) vectors
    Xt = train_data.reshape(-1,train_data.shape[-3],train_data.shape[-2],train_data.shape[-1]).copy()
    Data_point = Xt[0].copy()
    yt = train_labels.reshape(-1,1)
    target_scaler = MinMaxScaler([0,1])
    yt = target_scaler.fit_transform(yt)

    # Prepare data (X) and target (y) vectors
    Xv = train_data.reshape(-1,val_data.shape[-3],val_data.shape[-2],val_data.shape[-1]).copy()
    Data_point = Xv[0].copy()
    yv = val_labels.reshape(-1,1)
    target_scaler = MinMaxScaler([0,1])
    yv = target_scaler.fit_transform(yv)

    # Shuffle data and target vectors
    np.random.seed(42)
    np.random.shuffle(Xt)
    np.random.seed(42)
    np.random.shuffle(yt)
    np.random.seed(42)
    np.random.shuffle(Xv)
    np.random.seed(42)
    np.random.shuffle(yv)

    v_slice = int(Xv.shape[0]*0.6)
    x_train, x_val, x_test, y_train, y_val, y_test = Xt, Xv[:v_slice], Xv[v_slice:],  yt, yv[:v_slice], yv[v_slice:]

    y_train = target_scaler.fit_transform(y_train)
    y_test  = target_scaler.transform(y_test)
    y_val   = target_scaler.transform(y_val)

    return (x_train.astype(np.float32), y_train.astype(np.float32)), \
                (x_val.astype(np.float32), y_val.astype(np.float32)), \
                (x_test.astype(np.float32), y_test.astype(np.float32))


########################################################################################################
# KOPT
######
from hyperopt import fmin, tpe, hp, Trials
hyper_params = {
    "data":{
        },
    "model": {
        "repeat_filts": repeat_filts,
        "lr": lr,
        "activ": activ,
        "dense_activ": dense_activ,
        "add_reductive": add_reductive,
        "filt_size": filt_size,
        "nstart": nstart,
        "nfilters": nfilters,
        "dropout": dropout,
        "regularization": regularization,
        "loss": loss,
        "model_name": model_name,
        },
    "fit": {
        "epochs": epochs,
        "patience": patience,
        "batch_size_val": batch_size_val,
    }
}

from kopt import CompileFN
objective = CompileFN(db_name="mydb", exp_name="motif_initialization",  # experiment name
    data_fn = prep_data,
    model_fn = dac.setup_model_functional, 
    add_eval_metrics = ["mse", dac.f1], # metrics from concise.eval_metrics, you can also use your own
    optim_metric = "f1", # which metric to optimize for
    optim_metric_mode = "max", # maximum should be searched for
    valid_split = None, # use valid from the data function
    save_model = 'best', # checkpoint the best model
    save_results = True, # save the results as .json (in addition to mongoDB)
save_dir = save_path + "saved_models_{0}_{1}/".format(model_name,x_train.shape[0]))  # place to store the models
trials = Trials()
best = fmin(objective, hyper_params, trials=trials, algo=tpe.suggest, max_evals=100)
