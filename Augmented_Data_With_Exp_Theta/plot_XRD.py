import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams

rcParams.update({'figure.autolayout':True})


features = np.load('features_augmented.npy') 
targets = np.load('targets_augmented.npy')
two_theta = np.load('../two_theta.npy')


f, ax = plt.subplots(nrows = 1, ncols = 1)

##visually looks good, has several peaks
ax.plot(two_theta, features[2,:])

for side in ['left','right','top','bottom']:
    ax.spines[side].set_visible(False)


ax.tick_params(bottom = False, left = False, labelbottom = False, labelleft = False)

f.tight_layout()

f.savefig('XRD_pattern_input.png',format = 'png')



