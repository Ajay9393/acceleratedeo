ta manipualtion and calculation helper functions #
######################################################

# Data manipulation imports
import numpy as np
from sklearn import metrics
from collections import Counter
import pickle as pkl
import pandas as pd
import sys
import cv2


# ML imports
import tensorflow as tf
import keras
from keras import backend as K; K.clear_session()
from keras.models import Sequential, Model
from keras.layers import Lambda, Dense, Dropout, Activation, Flatten, Input, Conv2D, concatenate
from keras.callbacks import ModelCheckpoint
from keras.regularizers import l2
from keras.initializers import glorot_uniform


# Setup tensorflow
config = tf.ConfigProto(log_device_placement=True)
config.gpu_options.allow_growth=True
config.gpu_options.allocator_type ='BFC'
config.gpu_options.per_process_gpu_memory_fraction = 0.8

MASK_VALUE = -1

def _mask_nan(y_true, y_pred):
    mask_array = ~np.isnan(y_true)
    if np.any(np.isnan(y_pred)):
        print("WARNING: y_pred contains {0}/{1} np.nan values. removing them...".
            format(np.sum(np.isnan(y_pred)), y_pred.size))
        mask_array = np.logical_and(mask_array, ~np.isnan(y_pred))
    return y_true[mask_array], y_pred[mask_array]

def _mask_value(y_true, y_pred, mask=MASK_VALUE):
    mask_array = y_true != mask
    return y_true[mask_array], y_pred[mask_array]

def _mask_value_nan(y_true, y_pred, mask=MASK_VALUE):
    y_true, y_pred = _mask_nan(y_true, y_pred)
    return _mask_value(y_true, y_pred, mask)

def calc_recall(y_test, y_predict):
    unique, counts = np.unique((y_predict==1) * (y_test==1), return_counts=True)
    d = dict(zip(unique, counts))
    if True in unique:
        true_pos = d[True]
    else:
        true_pos = 0

    unique, counts = np.unique((y_predict==0) * (y_test==1), return_counts=True)
    d = dict(zip(unique, counts))
    if True in unique:
        false_neg = d[True]
    else:
        false_neg = 0
    
    all_pos = true_pos + false_neg
    if all_pos == 0:
        recall = 0
    else:
        recall = float(true_pos) / float(all_pos)

    return recall

def recall(y_true, y_pred):
    true_positives = np.sum(np.round(np.clip(y_true * y_pred, 0, 1)))
    possible_positives = np.sum(np.round(np.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    print(recall)
    return recall

def f1(y_true, y_pred, round=True):
    y_true, y_pred = _mask_value_nan(y_true, y_pred)
    if round:
        y_pred = np.maximum(y_pred,0)
        y_pred = np.minimum(y_pred,1)
        y_true = np.round(y_true)
        y_pred = np.round(y_pred)
    return metrics.f1_score(y_true, y_pred)

def confusion_matrix(y_true, y_pred, round=True):
    y_true, y_pred = _mask_value_nan(y_true, y_pred)
    if round:
        y_true = np.round(y_true)
        y_pred = np.round(y_pred)
    return metrics.confusion_matrix(y_true, y_pred)

def calc_F1_score(y_test, y_predict):
    unique, counts = np.unique(np.equal(y_predict,y_test), return_counts=True)
    d = dict(zip(unique, counts))
    true_pos = d[True]
   
    unique, counts = np.unique(y_predict, return_counts=True)
    d = dict(zip(unique, counts))
    true_pred = d[True]

    precision = float(true_pos) / float(true_pred)
    recall = calc_recall(y_predict, y_test)
    F1 = 2 * precision * recall / (precision + recall)
    return (F1, precision, recall)

def over_sample_dataset(X, y, bins=2):
    # Do oversampling
    df_y = pd.DataFrame(y, columns=['target'])
    count = Counter(pd.cut(df_y['target'], bins=bins))
    max_count = max([count[i] for i in count])
    ind_res=[]
    for key in count.keys():
        ind = list(df_y[(df_y['target'] > key.left) & (df_y['target'] <= key.right)].index)
        if len(ind) == max_count:
            ind_res += ind
        else:
            np.random.seed(42)
            ind_res += list(np.random.choice(ind, max_count))
    return X[ind_res], y[ind_res]

def shift2D(arr, n0, n1):
    shape = arr.shape
    arr_z = np.zeros_like(arr)
    arr_r = np.roll(arr,n0,0)
    arr_r = np.roll(arr_r,n1,1)
    if n0>=0:
        n0l = n0
        n0r = shape[0]
    else:
        n0l = 0
        n0r = n0
    if n1>=0:
        n1l = n1
        n1r = shape[1]        
    else:
        n1l = 0
        n1r = n1
    arr_z[n0l:n0r, n1l:n1r] = arr_r[n0l:n0r, n1l:n1r]
    return arr_z

def ind_choice_gen(arr, bin_size, random_seed=42, replace=False):
    np.random.seed(random_seed)
    choice = np.random.choice(arr, int(arr.shape[0]*bin_size), replace=replace)
    return choice

def rotate_image(image, angle):
    image_center = tuple(np.array(image.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
    return result

def transrot_oversample(data, targets, wells, by_well=False, rotation_split=24):
    
    # Seperate the images by their targets
    true_idxs = np.argwhere(targets>0.5)
    false_idxs = np.argwhere(targets<0.5)

    data_true = data[true_idxs[:,0]]
    if by_well: wells_true = wells[true_idxs[:,0]]

    data_false = data[false_idxs[:,0]]
    if by_well: wells_false = wells[false_idxs[:,0]]

    # Rotate the images marked as 1
    rotations_list = [data_true]
    for i in range(rotation_split-1):
        rotations = []
        for j in range(data_true.shape[0]):
            rotations.append(rotate_image(data_true[j], (360/rotation_split)*(i+1)))
        rotations_list.append(np.stack(rotations))
    data_true_rot = np.concatenate(rotations_list,axis=0)
    if by_well: wells_true_tiled = np.tile(wells_true, rotation_split)

    # Randomly translate the images marked as 1
    data_true_trans = [data_true_rot]
    trans_rot_idx = np.arange(data_true_trans[0].shape[0])
    idxs = ind_choice_gen(trans_rot_idx, 1, 42, replace=True)
    np.random.seed(42)
    trans_px = np.random.randint(-int(data_true.shape[1]*0.25),int(data_true.shape[1]*0.25), (idxs.shape[0],2))
    shifted=[]
    for i in range(trans_px.shape[0]):
        shifted.append(shift2D(data_true_rot[idxs[i]], trans_px[i,0], trans_px[i,1]))
    data_true_trans.append(np.stack(shifted))
    data_true_all = np.concatenate(data_true_trans, axis=0)
    if by_well: wells_true_tiled = np.concatenate((wells_true_tiled,wells_true_tiled[idxs]), axis=0)

    # Rotate and translate randomly leftover images marked as 0
    difference = data_true_all.shape[0] - data_false.shape[0]
    trans_rot_idx=np.arange(data_false.shape[0])

    data_false_all = [data_false]
    if by_well: wells_false_tiled = [wells_false]
    
    # 90 degree rotation
    idxs = ind_choice_gen(trans_rot_idx, (difference/6)/data_false.shape[0], 42)
    data_false_all.append(np.rot90(data_false[idxs], k=1, axes=(1,2)))
    if by_well: wells_false_tiled.append(wells_false_tiled[0][idxs])

    # 180 degree rotation
    idxs = ind_choice_gen(trans_rot_idx, (difference/6)/data_false.shape[0], 43)
    data_false_all.append(np.rot90(data_false[idxs], k=2, axes=(1,2)))
    if by_well: wells_false_tiled.append(wells_false_tiled[0][idxs])

    # 270 degree rotation
    idxs = ind_choice_gen(trans_rot_idx, (difference/6)/data_false.shape[0], 44)
    data_false_all.append(np.rot90(data_false[idxs], k=3, axes=(1,2)))
    if by_well: wells_false_tiled.append(wells_false_tiled[0][idxs])
    
    idxs = ind_choice_gen(trans_rot_idx, (difference/2)/data_false.shape[0], 45)
    np.random.seed(42)
    trans_px = np.random.randint(-int(data_true.shape[1]*0.25),int(data_true.shape[1]*0.25), (idxs.shape[0],2))
    shifted=[]
    for i in range(trans_px.shape[0]):
        shifted.append(shift2D(data_false[i], trans_px[i,0], trans_px[i,1]))
    if len(shifted)>0:
        data_false_all.append(np.stack(shifted))
    data_false_all = np.concatenate(data_false_all, axis=0)
    if by_well:
        wells_false_tiled.append(wells_false_tiled[0][idxs])
        wells_false_tiled = np.concatenate(wells_false_tiled, axis=0)
   
    data_all = np.concatenate((data_true_all, data_false_all), axis=0)
    if by_well:
        wells_all = np.concatenate((wells_true_tiled, wells_false_tiled), axis=0)
    else:
        wells_all = 0
    targets_all = np.concatenate((np.ones(data_true_all.shape[0]),np.zeros(data_false_all.shape[0])),axis=0)

    return (data_all, targets_all, wells_all)

# This function loads data along with its targets 
def prepare_data(data_paths, target_paths, over_sample=False, os_bins=2, val_frac=0.2, test_frac=0.1, by_well=False, trans_rot=False, trans_px=2, trans_rot_frac=0.3, fft=False, gs=False, new_transrot=False):
    
    # Imports
    from sklearn.preprocessing import MinMaxScaler
    
    # Load data
    if isinstance(data_paths,(list,)) or isinstance(data_paths,tuple):
        data_batch=[]
        target_batch=[]
        wells=[]
        well_iter=0
        for data_path in data_paths:
            data_batch.append(np.load(data_path))
            if by_well:
                f = open(data_path.replace('image_data.npy', 'image_meta_data.pickle'), 'rb')
                if sys.version_info[0] > 2:
                    dft_df = pkl.load(f, encoding='latin1')
                else:
                    dft_df = pkl.load(f)
                wells_temp = dft_df['Well'].values
                wells.append(wells_temp + np.max(wells_temp)*well_iter) # Increment well numbers so wells in different trays have different numbers
                well_iter += 1
        for target_path in target_paths:
            target_batch.append(np.load(target_path))
        data_batch = np.vstack(data_batch)
        target_batch = np.concatenate(target_batch)
        if by_well:
            wells = np.concatenate(wells)
            crystals = np.unique(wells*target_batch)[1:]
    else:
        data_batch = np.load(data_paths)
        target_batch = np.load(target_paths)
        if by_well:
            f = open(data_path.replace('image_data.npy', 'image_meta_data.pickle'), 'r')
            if sys.version_info[0] > 2:
                dft_df = pkl.load(f, encoding='latin1')
            else:
                dft_df = pkl.load(f)
            wells = dft_df['Well'].values
            crystals = np.unique(np.nonzero(wells*target_batch))
    
    # Rotate then translate
    if trans_rot:

        trans_rot_idx=np.arange(data_batch.shape[0])
        
        data_batch = [data_batch]
        target_batch = [target_batch]
        if by_well: wells_tiled = [wells]
        
        # 90 degree rotation
        idxs = ind_choice_gen(trans_rot_idx, trans_rot_frac)
        data_batch.append(np.rot90(data_batch[0][idxs], k=1, axes=(1,2)))
        target_batch.append(target_batch[0][idxs])
        if by_well: wells_tiled.append(wells_tiled[0][idxs])

        # 180 degree rotation
        idxs = ind_choice_gen(trans_rot_idx, trans_rot_frac)
        data_batch.append(np.rot90(data_batch[0][idxs], k=2, axes=(1,2)))
        target_batch.append(target_batch[0][idxs])
        if by_well: wells_tiled.append(wells_tiled[0][idxs])

        # 270 degree rotation
        idxs = ind_choice_gen(trans_rot_idx, trans_rot_frac)
        data_batch.append(np.rot90(data_batch[0][idxs], k=3, axes=(1,2)))
        target_batch.append(target_batch[0][idxs])
        if by_well: wells_tiled.append(wells_tiled[0][idxs])

        data_batch = np.concatenate(data_batch, axis=0)
        target_batch = np.concatenate(target_batch, axis=0)
        if by_well: wells_tiled = np.concatenate(wells_tiled, axis=0)
        
        trans_rot_idx=np.arange(data_batch.shape[0])
        
        data_batch = [data_batch]
        target_batch = [target_batch]
        if by_well: wells_tiled = [wells_tiled]

        # First shift
        idxs = ind_choice_gen(trans_rot_idx, trans_rot_frac)
        data_batch.append(np.moveaxis(shift2D(np.moveaxis(data_batch[0][idxs], 0, -1), 0, trans_px), -1, 0))
        target_batch.append(target_batch[0][idxs])
        if by_well: wells_tiled.append(wells_tiled[0][idxs])

        # Second shift
        idxs = ind_choice_gen(trans_rot_idx, trans_rot_frac)
        data_batch.append(np.moveaxis(shift2D(np.moveaxis(data_batch[0][idxs], 0, -1), 0, -trans_px), -1, 0))
        target_batch.append(target_batch[0][idxs])
        if by_well: wells_tiled.append(wells_tiled[0][idxs])

        # Third shift
        idxs = ind_choice_gen(trans_rot_idx, trans_rot_frac)
        data_batch.append(np.moveaxis(shift2D(np.moveaxis(data_batch[0][idxs], 0, -1), trans_px, 0), -1, 0))
        target_batch.append(target_batch[0][idxs])
        if by_well: wells_tiled.append(wells_tiled[0][idxs])

        # Fourth shift
        idxs = ind_choice_gen(trans_rot_idx, trans_rot_frac)
        data_batch.append(np.moveaxis(shift2D(np.moveaxis(data_batch[0][idxs], 0, -1), -trans_px, 0), -1, 0))
        target_batch.append(target_batch[0][idxs])
        if by_well: wells_tiled.append(wells_tiled[0][idxs])

        data_batch = np.concatenate(data_batch, axis=0)
        target_batch = np.concatenate(target_batch, axis=0)
        if by_well: wells_tiled = np.concatenate(wells_tiled, axis=0)
    elif new_transrot:
        data_batch, target_batch, wells_tiled = transrot_oversample(data_batch, target_batch, wells, by_well=by_well, rotation_split=24)
    else:
        if by_well:
            wells_tiled = wells
        
    if gs:
        data_batch = np.average(data_batch / 255., axis=-1)[...,np.newaxis]
    if fft:
        data_batch_gs = np.average(data_batch / 255., axis=-1)
        data_batch_fft = np.fft.fft2(data_batch_gs)
        data_batch_fft_mag = np.absolute(data_batch_fft)[...,np.newaxis]
        data_batch_fft_ang = np.angle(data_batch_fft)[...,np.newaxis]
        data_batch = np.concatenate((data_batch, data_batch_fft_mag, data_batch_fft_ang), axis=-1)
    
    # Prepare data (X) and target (y) vectors
    X = data_batch.reshape(-1,data_batch.shape[-3],data_batch.shape[-2],data_batch.shape[-1]).copy()
    y = target_batch.reshape(-1,1)
    target_scaler = MinMaxScaler([0,1])
    y = target_scaler.fit_transform(y)

    # Dump targets mapper
    pkl.dump(target_scaler, open(('target_scaler.pkl'), 'wb'))

    # Split data by well
    if by_well:
        idx_images=np.arange(X.shape[0])
        wells_list = np.arange(np.max(wells))
        non_crystals = np.setdiff1d(wells_list, crystals)
        np.random.seed(42)
        np.random.shuffle(wells_list)

        # Split the wells into training, validation, and test sets
        train_val_frac = 1.0 - test_frac
        train_frac = (train_val_frac - val_frac) / train_val_frac

        nc_slice_train_test = int(non_crystals.shape[0]*train_val_frac)
        nc_slice_train_val = int(nc_slice_train_test*train_frac)

        c_slice_train_test = int(crystals.shape[0]*train_val_frac)
        c_slice_train_val = int(c_slice_train_test*train_frac)

        wells_train = np.sort( np.concatenate((crystals[:c_slice_train_val], non_crystals[:nc_slice_train_val]),axis=0) )
        wells_val = np.sort( np.concatenate((crystals[c_slice_train_val:c_slice_train_test], non_crystals[nc_slice_train_val:nc_slice_train_test]),axis=0) )
        wells_test = np.sort( np.concatenate((crystals[c_slice_train_test:], non_crystals[nc_slice_train_test:]),axis=0) )

        # Find the image indeces of the wells in each set
        train_idxs = idx_images[np.isin(wells_tiled, wells_train)]
        val_idxs = idx_images[np.isin(wells_tiled, wells_val)]
        test_idxs = idx_images[np.isin(wells_tiled, wells_test)]

        # Oversampling
        if over_sample:
            x_train, y_train = over_sample_dataset(X[train_idxs],y[train_idxs],os_bins)
            x_val, y_val = over_sample_dataset(X[val_idxs],y[val_idxs], os_bins)
            x_test, y_test = over_sample_dataset(X[test_idxs],y[test_idxs], os_bins)
        else:
            x_train = X[train_idxs]
            y_train = y[train_idxs]
            x_val = X[val_idxs]
            y_val = y[val_idxs]
            x_test = X[test_idxs]
            y_test = y[test_idxs]

        # Shuffle

        np.random.seed(42)
        np.random.shuffle(x_train)

        np.random.seed(42)
        np.random.shuffle(y_train)
       
        np.random.seed(42)
        np.random.shuffle(x_val)

        np.random.seed(42)
        np.random.shuffle(y_val)

        np.random.seed(42)
        np.random.shuffle(x_test)

        np.random.seed(42)
        np.random.shuffle(y_test)

    else:

        # Shuffle data and target vectors
        np.random.seed(42)
        np.random.shuffle(X)
        np.random.seed(42)
        np.random.shuffle(y)

        # Split the entire dataset into training and test sets
        train_val_frac = 1.0 - test_frac
        d_slice = int(X.shape[0]*train_val_frac)
        x_train, x_test, y_train, y_test = X[:d_slice],X[d_slice:], y[:d_slice],y[d_slice:]
        
        # Split the training set into training and validation sets
        train_frac = (train_val_frac - val_frac) / train_val_frac
        v_slice = int(x_train.shape[0]*train_frac)
        x_train, x_val, y_train, y_val = x_train[:v_slice],x_train[v_slice:], y_train[:v_slice],y_train[v_slice:]

        # Oversampling
        if over_sample:
            x_train, y_train = over_sample_dataset(x_train,y_train,os_bins)
            x_val, y_val = over_sample_dataset(x_val,y_val, os_bins)
            x_test, y_test = over_sample_dataset(x_test,y_test, os_bins)      
        
            np.random.seed(42)
            np.random.shuffle(x_train)

            np.random.seed(42)
            np.random.shuffle(y_train)
           
            np.random.seed(42)
            np.random.shuffle(x_val)

            np.random.seed(42)
            np.random.shuffle(y_val)

            np.random.seed(42)
            np.random.shuffle(x_test)

            np.random.seed(42)
            np.random.shuffle(y_test)


    y_train = target_scaler.fit_transform(y_train)
    y_test  = target_scaler.fit_transform(y_test)
    y_val   = target_scaler.fit_transform(y_val)
    
    return (x_train.astype(np.float32), y_train.astype(np.float32)), \
                (x_val.astype(np.float32), y_val.astype(np.float32)), \
                (x_test.astype(np.float32), y_test.astype(np.float32))

def prepare_data2(data, fft=False, gs=False, shuffle=True):
    
    # Convert tuple data to list
    data_processed = []
    for d in data:
        data_processed.append(list(d))

    # Normalization
    for i in range(len(data_processed)):
        mean = np.mean(data_processed[i][0], axis=(-3,-2,-1), keepdims=True)
        data_processed[i][0] = data_processed[i][0] - mean

    # Greyscale conversion / FT
    if gs:
        for i in range(len(data)):
            data_processed[i][0] = np.average(data_processed[i][0] / 255., axis=-1)[...,np.newaxis]
    if fft:
        for i in range(len(data)):
            data_batch_gs = np.average((data[i][0]) / 255., axis=-1)
            data_batch_fft = np.fft.fft2(data_batch_gs)
            data_batch_fft_mag = np.absolute(data_batch_fft)[...,np.newaxis]
            data_batch_fft_ang = np.angle(data_batch_fft)[...,np.newaxis]
            data_processed[i][0] = np.concatenate((data_processed[i][0], data_batch_fft_mag, data_batch_fft_ang), axis=-1)
            
    # Shuffle and float32 conversion    
    for i in range(len(data_processed)):
        for j in range(len(data_processed[i])):
            np.random.seed(42)
            data_processed[i][j] = data_processed[i][j].astype(np.float32)
            if shuffle:
                np.random.shuffle(data_processed[i][j])
   
    output=()
    for d in data_processed:
        output = output + (tuple(d),)

    return output    

def setup_model(train_data,activ,dense_activ,nstart,repeat_filts,filt_size,add_reductive,nfilters,dropout,regularization,loss,model_name,lr,fft=False,pre_concat=10):
    if fft:
        return setup_model_with_fft(train_data,activ,dense_activ,nstart,repeat_filts,filt_size,add_reductive,nfilters,dropout,regularization,loss,model_name,lr,pre_concat)
    keras.backend.clear_session()
    sess = tf.Session(config=config)
    model = Sequential()
    for i_step, step in enumerate(range(nstart,int(np.log(train_data[0].shape[1])/np.log(2)))[::-1]):
        for repeat_filt in range(repeat_filts):
            model.add(Conv2D(nfilters, filt_size, strides=(1,1), padding='same', input_shape=train_data[0][0].shape, kernel_regularizer=l2(regularization), data_format='channels_last'))
            model.add(Dropout(dropout))
            model.add(Activation(activ))
        if add_reductive == True:
            model.add(Conv2D(nfilters, filt_size, strides=(2,2), padding='same', input_shape=train_data[0][0].shape, kernel_regularizer=l2(regularization), data_format='channels_last'))
            model.add(Dropout(dropout))
            model.add(Activation(activ))                                                                                        
    model.add(Flatten())
    model.add(Dense(1, activation=dense_activ))
    checkpoint = ModelCheckpoint('model_{0}.hdf5'.format(model_name), monitor='val_acc', verbose=1, save_best_only=False, mode='auto')
    callbacks_list = [checkpoint]
    opt = keras.optimizers.adam(lr)
    model_serial = model
    model_serial.compile(loss=loss, optimizer=opt, metrics=None)
    model_serial.summary()
    return model_serial

def setup_model_functional(train_data,activ,dense_activ,nstart,repeat_filts,filt_size,add_reductive,nfilters,dropout,regularization,loss,model_name,lr,fft=False, pre_concat=10, optimizer='adam'):
    if fft:
        model = setup_model_with_fft(train_data,activ,dense_activ,nstart,repeat_filts,filt_size,add_reductive,nfilters,dropout,regularization,loss,model_name,lr,pre_concat)
    else:
        keras.backend.clear_session()
        sess = tf.Session(config=config)
        
        inputs = Input(shape=train_data[0][0].shape)
        x = inputs
        
        for i_step, step in enumerate(range(nstart,int(np.log(train_data[0].shape[1])/np.log(2)))[::-1]):
            for repeat_filt in range(repeat_filts):
                x = Conv2D(nfilters, filt_size, strides=(1,1), padding='same', input_shape=train_data[0][0].shape, kernel_regularizer=l2(regularization), kernel_initializer=glorot_uniform(),bias_initializer=glorot_uniform(), data_format='channels_last')(x)
                x = Dropout(dropout)(x)
                x = Activation(activ)(x)
            if add_reductive == True:
                x = Conv2D(nfilters, filt_size, strides=(2,2), padding='same', input_shape=train_data[0][0].shape, kernel_regularizer=l2(regularization), kernel_initializer=glorot_uniform(), bias_initializer=glorot_uniform(), data_format='channels_last')(x)
                x = Dropout(dropout)(x)
                x = Activation(activ)(x)
        x = Flatten()(x)
        predictions = Dense(1, activation=dense_activ, kernel_initializer=glorot_uniform(), bias_initializer=glorot_uniform(),)(x)
        
        model = Model(inputs=inputs, outputs=predictions)

    checkpoint = ModelCheckpoint('model_{0}.hdf5'.format(model_name), monitor='val_loss', verbose=1, save_best_only=False, mode='auto')
    callbacks_list = [checkpoint, PlotLosses(model_name=model_name)]
    if optimizer == 'adam':
        opt = keras.optimizers.adam(lr)
    elif optimizer == 'sgd':
        opt = keras.optimizers.SGD(lr)
    elif optimizer == 'rmsprop':
        opt = keras.optimizers.RMSprop(lr)
    model.compile(loss=loss, optimizer=opt, metrics=['accuracy'])
    model.summary()
    return model

def setup_model_with_fft(train_data,activ,dense_activ,nstart,repeat_filts,filt_size,add_reductive,nfilters,dropout,regularization,loss,model_name,lr,pre_concat=10):
    keras.backend.clear_session()
    sess = tf.Session(config=config)
    
    # Setup inputs for the model
    shp = train_data[0][0].shape
    inputs = Input(shape=shp)
    
    # Image net
    x = Lambda(lambda x: x[...,:shp[-1]-2], output_shape=shp[:-1]+(shp[-1]-2,))(inputs)
    
    for i_step, step in enumerate(range(nstart[0],int(np.log(train_data[0].shape[1])/np.log(2)))[::-1]):
        for repeat_filt in range(repeat_filts[0]):
            x = Conv2D(nfilters[0], filt_size[0], strides=(1,1), padding='same', input_shape=train_data[0][0].shape, kernel_regularizer=l2(regularization[0]), data_format='channels_last')(x)
            x = Dropout(dropout[0])(x)
            x = Activation(activ[0])(x)
        if add_reductive[0] == True:
            x = Conv2D(nfilters[0], filt_size[0], strides=(2,2), padding='same', input_shape=train_data[0][0].shape, kernel_regularizer=l2(regularization[0]), data_format='channels_last')(x)
            x = Dropout(dropout[0])(x)
            x = Activation(activ[0])(x)
    x = Flatten()(x)
    x = Dense(pre_concat)(x)
    
    # FFT net
    y = Lambda(lambda x: x[...,-2:], output_shape=shp[:-1]+(2,))(inputs)
    
    for i_step, step in enumerate(range(nstart[1],int(np.log(train_data[0].shape[1])/np.log(2)))[::-1]):
        for repeat_filt in range(repeat_filts[1]):
            y = Conv2D(nfilters[1], filt_size[1], strides=(1,1), padding='same', input_shape=train_data[0][0].shape, kernel_regularizer=l2(regularization[1]), data_format='channels_last')(y)
            y = Dropout(dropout[1])(y)
            y = Activation(activ[1])(y)
        if add_reductive[1] == True:
            y = Conv2D(nfilters[1], filt_size[1], strides=(2,2), padding='same', input_shape=train_data[0][0].shape, kernel_regularizer=l2(regularization[1]), data_format='channels_last')(y)
            y = Dropout(dropout[1])(y)
            y = Activation(activ[1])(y)
    y = Flatten()(y)
    y = Dense(pre_concat)(y)   

    # Combined net
    z = concatenate([x,y])
    predictions = Dense(1, activation=dense_activ)(z)

    model = Model(inputs=inputs, outputs=predictions)

    return model


def parse_performance():
    loss = []
    val_loss=[]
    for i in range(0,len(content)-1):
        if content[i:i+7]==' loss: ':
            temp = content[i+7:i+13]
            if content[i+13] == 'e':
                temp = temp + content[i+13:i+17]
            loss.append(float(temp))
        if content[i] == 'v':
            temp = content[i+10:i+16]
            val_loss.append(float(temp))
    loss = np.asarray(loss)
    val_loss = np.asarray(val_loss)
    all_loss = np.stack((loss,val_loss))
    epoch = np.arange(1,len(loss)+1)
    
    return all_loss, epoch

def parse_performance_kopt():
    loss = []
    all_loss = []
    val_loss=[]
    all_val_loss=[]
    for i in range(0,len(content)-1):
        if content[i:i+11]=='Evaluate...':
            all_loss.append(loss)
            loss=[]
            all_val_loss.append(val_loss)
            val_loss=[]
        if content[i:i+7]==' loss: ':
            temp = content[i+7:i+13]
            if content[i+13] == 'e':
                temp = temp + content[i+13:i+17]
            loss.append(float(temp))
        if content[i] == 'v':
            temp = content[i+10:i+16]
            val_loss.append(float(temp))
    loss = np.asarray(all_loss)
    val_loss = np.asarray(all_val_loss)
    all_loss = np.stack((loss,val_loss))
    
    return all_loss

def graph_performance(epoch, all_loss, loss_names, save_loc_and_name):
    import matplotlib as mpl
    mpl.use("Agg")
    import matplotlib.pyplot as plt
    
    fig = plt.figure(figsize=(8, 6), dpi=80)

    ax = fig.add_subplot(111)
    fig.subplots_adjust(top=0.85, left = .15, right = .85, bottom = .15)

    ax.set_xlabel('Epoch')
    ax.set_ylabel('Loss')

    ax.plot(epoch, all_loss)
    ax.legend(['training loss', 'validation loss'], loc='upper right', prop={'size': 8})
    fig.savefig(save_loc_and_name)

class PlotLosses(keras.callbacks.Callback):
    import matplotlib.pyplot as plt
    import numpy as np
    def __init__(self,model_name):
        self.model_name=model_name
        self.validation_data = None
        self.model = None
    def on_train_begin(self, logs={}):
        self.i = 0
        self.x = []
        self.losses = []
        self.val_losses = []
        self.fig = plt.figure()
        self.logs = []
    def on_epoch_end(self, epoch, logs={}):
        self.logs.append(logs)
        self.x.append(self.i)
        self.losses.append(logs.get('loss'))
        self.val_losses.append(logs.get('val_loss'))
        self.i += 1
        #clear_output(wait=True)
        plt.close('all')
        plt.plot(self.x, np.log(self.losses)/np.log(10), label="Training Data")
        plt.plot(self.x, np.log(self.val_losses)/np.log(10), label="Validation Data")
        plt.xlabel('Epochs')
        plt.ylabel('Log Loss / a.u.')
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        plt.legend()
        plt.savefig('loss_plot_{0}.png'.format(self.model_name), dpi=300)

class MyCbk(keras.callbacks.Callback):
    def __init__(self, model, model_name):
        self.model_to_save = model
        self.model_name = model_name
    def on_epoch_end(self, epoch, logs=None):
        self.model_to_save.save_weights('serial_model_{0}.h5'.format(self.model_name))


