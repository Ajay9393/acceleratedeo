
m __future__ import print_function
import os
import time
import pandas as pd
from sklearn.model_selection import train_test_split 
from sklearn.metrics import mean_absolute_error,mean_squared_error, r2_score
import tensorflow as tf
from itertools import product
from scipy.interpolate import griddata
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from matplotlib import cm
from sklearn.preprocessing import MinMaxScaler,StandardScaler
import h5py
import pickle
from collections import Counter
from ednn_routines import *
from itertools import product
import keras
import numpy as np
import tensorflow as tf
from keras.models import Sequential, Model
from keras.utils import plot_model
from keras.layers import Lambda,Dense, Dropout, Activation, Flatten, Input, Lambda, Conv2D, MaxPooling2D, Conv3D, MaxPooling3D,Reshape,UpSampling3D,Concatenate, LSTM
from keras.callbacks import ModelCheckpoint
from keras.regularizers import l2
from keras.utils import multi_gpu_model
from sklearn.decomposition import PCA
from mpl_toolkits.mplot3d import Axes3D
config = tf.ConfigProto()
config.gpu_options.allow_growth=True
config.gpu_options.allocator_type ='BFC'
config.gpu_options.per_process_gpu_memory_fraction = 0.7
def my_data_all():
    data = pd.read_pickle('df_all.pkl')
    X = np.array(data)
    Data_point = X[0].copy()
    y = X.copy()
    slice = int(X.shape[0]*0.9) ;x_train, x_test, y_train, y_test = X[:slice],X[slice:], y[:slice],y[slice:]
    slice = int(x_train.shape[0]*0.85) ;x_train, x_val, y_train, y_val = x_train[:slice],x_train[slice:], y_train[:slice],y_train[slice:]
    tr_shape = np.array( x_train.shape[1:]).prod()
    target_scaler = MinMaxScaler([0,1])
    x_train = target_scaler.fit_transform(x_train);x_val = target_scaler.transform(x_val);x_test = target_scaler.transform(x_test);
    y_train = target_scaler.fit_transform(y_train);y_val = target_scaler.transform(y_val);y_test = target_scaler.transform(y_test);
    pickle.dump(target_scaler, open('target_scaler_vae.pkl', 'wb'))
    #x_train, y_train = over_sample(x_train,y_train, bins=4)
    #x_val, y_val = over_sample(x_val,y_val, bins=4)
    #x_test, y_test = over_sample(x_test,y_test, bins=4)
    return (x_train, y_train),(x_val, y_val),(x_test, y_test)
train, val,test = my_data_all()
(x_train, y_train),(x_val, y_val),(x_test, y_test)  = train, val,test
def my_model_vae(train_data,activ,dropout,regularization,model_name,lr, hidden_space,nmin,nmax, repeat_dense):
    from keras import backend as K; K.clear_session()
    sess = tf.Session(config=config)
    inputs = Input(shape=train_data[0].shape[1:])
    layer = Reshape(train_data[0].shape[1:])(inputs)
    for i_step, step in enumerate(range(nmin,nmax)[::-1]):
        for repeat_d in range(repeat_dense):
            layer =Dense(2**step,input_shape = list(train_data[0][0].shape),kernel_regularizer=l2(regularization))(layer)
            layer = Dropout(dropout)(layer); layer = Activation(activ)(layer)
    layer = Dense(hidden_space)(layer);layer = Dropout(dropout)(layer);layer =Activation(activ)(layer)
    for i_step, step in enumerate(range(nmin,nmax)):
        for repeat_d in range(repeat_dense):
            layer =Dense(2**step,input_shape = list(train_data[0][0].shape),kernel_regularizer=l2(regularization))(layer)
            layer = Dropout(dropout)(layer); layer = Activation(activ)(layer)
    layer = Dense(train_data[0].shape[1])(layer)#;layer = Dropout(dropout)(layer)#;layer = Activation(activ)(layer)
    model_serial = Model(inputs = inputs, outputs = layer)
    checkpoint = ModelCheckpoint('model_{0}.hdf5'.format(model_name), monitor='val_acc', verbose=1, save_best_only=False, mode='auto')
    callbacks_list = [checkpoint, PlotLosses(model_name=model_name)]
    opt = keras.optimizers.adam(lr=lr); model_serial.compile(loss= 'mean_squared_error' , optimizer=opt, metrics=None)
    model_serial.summary()
    return model_serial
model_name_vae= 'vae_22'; nmin = 2;nmax = 6;regularization = 0.0001;repeat_dense = 1;hidden_space = 2; dropout = 0.0;activ = 'relu'; add_reductive = True; lr=0.0001;batch_size = 16;epochs = 500 
model_vae= my_model_vae(train_data=train,activ=activ,dropout=dropout,regularization=regularization,model_name=model_name_vae,lr=lr, hidden_space=hidden_space,nmin=nmin,nmax=nmax,repeat_dense=repeat_dense)
model_vae.load_weights('serial_model_{0}.h5'.format(model_name_vae))
#plot_model(model_vae); plt.savefig('nn_arch_{0}'.format(model_name_vae), dpi=300)
print([(i,ii.name) for i, ii in enumerate(model_vae.layers)])
h_layer_ind = 16
new_model_vae = Model(model_vae.inputs, model_vae.layers[h_layer_ind].output)
x_train = new_model_vae.predict(x_train);x_val=new_model_vae.predict(x_val); x_test = new_model_vae.predict(x_test)
X = np.concatenate([x_train,x_val,x_test])

def my_data_lstm(X, memory=1):
    data = pd.read_pickle('df_all.pkl')
    X = X[:-1]
    X_roll = np.zeros([X.shape[0], memory, X.shape[1]])
    for i_mem, mem in enumerate(range(memory)[::-1]):
        X_roll[:,i_mem,:] = np.roll(X,mem, axis=0)
        for i_memm in range(mem)[::-1]:
            X_roll[i_memm,:,:] = X_roll[i_memm+1,:,:]
    X=X_roll 
    Data_point = X[0].copy()
    y = np.array(data['Last_GC1']).reshape(-1,1)[1:]
    slice = int(X.shape[0]*0.9) ;x_train, x_test, y_train, y_test = X[:slice],X[slice:], y[:slice],y[slice:]
    slice = int(x_train.shape[0]*0.85) ;x_train, x_val, y_train, y_val = x_train[:slice],x_train[slice:], y_train[:slice],y_train[slice:]
    #slice = int(X.shape[0]*0.9) ; y_train, y_test = y[:slice],y[slice:]
    #slice = int(x_train.shape[0]*0.85) ;y_train, y_val = y_train[:slice],y_train[slice:]
    target_scaler = MinMaxScaler([0,1])
    y_train = target_scaler.fit_transform(y_train);y_val = target_scaler.transform(y_val);y_test = target_scaler.transform(y_test);
    pickle.dump(target_scaler, open('target_scaler_lstm.pkl', 'wb'))
    return (x_train, y_train),(x_val, y_val),(x_test, y_test)

def my_model_lstm(train_data,activ,dropout,regularization,model_name,lr,nmin,nmax, repeat_dense,memory):
    from keras import backend as K; K.clear_session()
    sess = tf.Session(config=config)
    inputs = Input(shape=train_data[0].shape[1:])
    layer = Reshape(list(train_data[0].shape[1:]))(inputs)
    for i_step, step in enumerate(range(nmin,nmax)[::-1]):
        for repeat_d in range(repeat_dense):
            layer =LSTM(units=step,return_sequences=True,input_shape = (list(train_data[0][0].shape)+[1]),kernel_regularizer=l2(regularization))(layer)
            layer = Dropout(dropout)(layer); layer = Activation(activ)(layer)
            layer =Dense(2**step,input_shape = list(train_data[0][0].shape),kernel_regularizer=l2(regularization))(layer)
            layer = Dropout(dropout)(layer); layer = Activation(activ)(layer)
    layer = Flatten()(layer)
    layer = Dense(1)(layer) 
    model_serial = Model(inputs = inputs, outputs = layer)
    checkpoint = ModelCheckpoint('model_{0}.hdf5'.format(model_name), monitor='val_acc', verbose=1, save_best_only=False, mode='auto')
    callbacks_list = [checkpoint, PlotLosses(model_name=model_name)]
    opt = keras.optimizers.adam(lr=lr); model_serial.compile(loss= 'mean_squared_error' , optimizer=opt, metrics=None)
    model_serial.summary()
    return model_serial
data_name = 'vae_22'
#model_name= 'lstm_2'; nmin = 4;nmax = 6;regularization = 0.0001;repeat_dense = 1;hidden_space = 2; dropout = 0.0;activ = 'relu'; add_reductive = True; lr=0.0001;batch_size = 1;epochs = 500 
#model_name= 'lstm_4'; nmin = 4;nmax = 6;regularization = 0.0001;repeat_dense = 1; dropout = 0.2;activ = 'relu'; add_reductive = True;memory=5; lr=0.0001;batch_size = 1;epochs = 100 
#model_name= 'lstm_5'; nmin = 4;nmax = 6;regularization = 0.0001;repeat_dense = 1; dropout = 0.2;activ = 'relu'; add_reductive = True;memory=5; lr=0.0001;batch_size = 16;epochs = 500 
#model_name= 'lstm_6'; nmin = 3;nmax = 5;regularization = 0.0001;repeat_dense = 1; dropout = 0.2;activ = 'relu'; add_reductive = True;memory=5; lr=0.0001;batch_size = 16;epochs = 500 
#model_name= 'lstm_7'; nmin = 4;nmax = 6;regularization = 0.0001;repeat_dense = 1; dropout = 0.4;activ = 'relu'; add_reductive = True;memory=5; lr=0.0001;batch_size = 16;epochs = 500 
#model_name= 'lstm_8'; nmin = 4;nmax = 6;regularization = 0.0001;repeat_dense = 1; dropout = 0.4;activ = 'relu'; add_reductive = True;memory=5; lr=0.0001;batch_size = 16;epochs = 500 
#model_name= 'lstm_9'; nmin = 4;nmax = 6;regularization = 0.0001;repeat_dense = 1; dropout = 0.0;activ = 'relu'; add_reductive = True;memory=5; lr=0.0001;batch_size = 16;epochs = 500 
#model_name= 'lstm_10'; nmin = 4;nmax = 6;regularization = 0.001;repeat_dense = 1; dropout = 0.0;activ = 'relu'; add_reductive = True;memory=5; lr=0.0001;batch_size = 16;epochs = 500 
#model_name= 'lstm_11'; nmin = 4;nmax = 6;regularization = 0.001;repeat_dense = 1; dropout = 0.0;activ = 'relu'; add_reductive = True;memory=10; lr=0.0001;batch_size = 16;epochs = 500 
#model_name= 'lstm_10_full'; nmin = 4;nmax = 6;regularization = 0.001;repeat_dense = 1; dropout = 0.0;activ = 'relu'; add_reductive = True;memory=5; lr=0.0001;batch_size = 16;epochs = 500 
#model_name= 'lstm_12'; nmin = 4;nmax = 6;regularization = 0.001;repeat_dense = 1; dropout = 0.0;activ = 'relu'; add_reductive = True;memory=2; lr=0.0001;batch_size = 16;epochs = 500 
#model_name= 'lstm_14_full'; nmin = 4;nmax = 7;regularization = 0.001;repeat_dense = 1; dropout = 0.0;activ = 'relu'; add_reductive = True;memory=5; lr=0.0001;batch_size = 16;epochs = 500 
#model_name= 'lstm_16_full'; nmin = 4;nmax = 6;regularization = 0.001;repeat_dense = 1; dropout = 0.2;activ = 'relu'; add_reductive = True;memory=5; lr=0.0001;batch_size = 16;epochs = 500 
#model_name= 'lstm_17_full'; nmin = 4;nmax = 6;regularization = 0.005;repeat_dense = 1; dropout = 0.0;activ = 'relu'; add_reductive = True;memory=5; lr=0.0001;batch_size = 16;epochs = 500 
model_name= 'lstm_18_full'; nmin = 4;nmax = 6;regularization = 0.0001;repeat_dense = 1; dropout = 0.0;activ = 'relu'; add_reductive = True;memory=5; lr=0.0001;batch_size = 16;epochs = 500 
#model_name= 'lstm_19_full'; nmin = 4;nmax = 6;regularization = 0.001;repeat_dense = 1; dropout = 0.0;activ = 'relu'; add_reductive = True;memory=5; lr=0.0001;batch_size = 8;epochs = 500 
#model_name= 'lstm_20_full'; nmin = 4;nmax = 6;regularization = 0.000;repeat_dense = 1; dropout = 0.0;activ = 'relu'; add_reductive = True;memory=5; lr=0.0001;batch_size = 16;epochs = 500 
#model_name= 'lstm_21_full'; nmin = 4;nmax = 6;regularization = 0.00001;repeat_dense = 1; dropout = 0.0;activ = 'relu'; add_reductive = True;memory=5; lr=0.0001;batch_size = 16;epochs = 500 
train, val,test = my_data_lstm(X, memory=memory)
(x_train, y_train),(x_val, y_val),(x_test, y_test)  = train, val,test
model= my_model_lstm(train_data=train,activ=activ,dropout=dropout,regularization=regularization,model_name=model_name,lr=lr,nmin=nmin,nmax=nmax, repeat_dense=repeat_dense, memory=memory)
#plot_model(model, show_shapes=True, to_file='nn_arch_{0}.png'.format(model_name))
try:
    model.load_weights('serial_model_{0}.h5'.format(model_name))
    print('Successfully loaded the last model')
except:
    print('Could not load the model, training from scratch')
mode='train'
if mode=='train':
    checkpoint = ModelCheckpoint('model_{0}.hdf5'.format(model_name), monitor='val_acc', verbose=1, save_weights_only=True,save_best_only=True, mode='auto')
    callbacks_list = [checkpoint, PlotLosses(model_name=model_name),MyCbk(model, model_name) ]
    model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(x_val, y_val), shuffle=True,callbacks=callbacks_list)
mode='test'
if mode == 'test':
    target_scaler = pickle.load(open('target_scaler_lstm.pkl','rb'))
    pred_train = model.predict(x_train);pred_val = model.predict(x_val);pred_test = model.predict(x_test)
    pred_train = target_scaler.inverse_transform(pred_train); pred_val = target_scaler.inverse_transform(pred_val);pred_test = target_scaler.inverse_transform(pred_test);
    true_train = target_scaler.inverse_transform(y_train);true_val = target_scaler.inverse_transform(y_val);true_test = target_scaler.inverse_transform(y_test)
    plt.close('all')
    #fig, axes = plt.subplots(nrows=3, ncols=7,figsize=(20,12))
    plt.scatter(true_train,pred_train, label=r'$r^2$={0:.2f}; MAE={1:.1f}'.format(r2_score(true_train,pred_train),mean_absolute_error(true_train,pred_train)),s=0.5)
    plt.scatter(true_val,pred_val, label=r'$r^2$={0:.2f}; MAE={1:.1f}'.format(r2_score(true_val,pred_val),mean_absolute_error(true_val,pred_val)),s=0.5)
    plt.scatter(true_test,pred_test, label=r'$r^2$={0:.2f}; MAE={1:.1f}'.format(r2_score(true_test,pred_test),mean_absolute_error(true_test,pred_test)),s=0.5)
    plt.plot(np.linspace(true_train.min(),true_train.max()),np.linspace(true_train.min(),true_train.max()), lw=0.2,color='black')
    plt.legend()
    plt.xlabel('True GC1 Closing Price / USD per t oz')
    plt.xlabel('Predicted GC1 Closing Price / USD per t oz')
    plt.savefig('true_pred_vae_ann_target_{0}.png'.format(model_name), dpi=300)

    plt.close('all')
    data = pd.read_pickle('df_all.pkl')
    plt.plot(data.index[1:],np.concatenate([true_train, true_val, true_test]), label=r'True',lw=0.5)
    plt.plot(data.index[1:true_train.shape[0]+1],pred_train.reshape(-1), label=r'Train',lw=1.5) 
    plt.plot(data.index[true_train.shape[0]+1:true_train.shape[0]+true_val.shape[0]+1],pred_val.reshape(-1), label=r'Validation',lw=1.5) 
    plt.plot(data.index[true_train.shape[0]+true_val.shape[0]+1:],pred_test.reshape(-1), label=r'Test',lw=1.5) 
    plt.legend()
    plt.xlabel('Date')
    plt.xlabel('GC1 Closing Price / USD per t oz')
    plt.savefig('true_pred_vae_ann_time_series_{0}.png'.format(model_name), dpi=300)
   
'''
from matplotlib.dates import DateFormatter, date2num 
data = pd.read_pickle('df_all.pkl')
plt.close('all'); 
plt.scatter(X[:,0],X[:,1],c=[date2num(i.date()) for i in data.index], cmap=cm.gist_rainbow)
plt.xlabel('X');plt.ylabel('Y')
#plt.clim(1., 2.6)
#plt.ylim(-400,400)
plt.colorbar(label='Date', format=DateFormatter('%y-%m-%d')); 
plt.savefig('hidden_space_{0}.png'.format(model_name_vae), dpi=300)
X_expl_1 = np.zeros([1000,2]); X_expl_1[:,0]=np.linspace(0.5,6.5,1000);X_expl_1[:,1] =np.linspace(0,0,1000)
X_expl_2 = np.zeros([1000,2]); X_expl_2[:,0]=np.linspace(0.5,0.5,1000);X_expl_2[:,1] =np.linspace(0,1.5,1000)
model_name_vae= 'vae_22'; nmin = 2;nmax = 6;regularization = 0.0001;repeat_dense = 1;hidden_space = 2; dropout = 0.0;activ = 'relu'; add_reductive = True; lr=0.0001;batch_size = 16;epochs = 500
model_vae= my_model_vae(train_data=train,activ=activ,dropout=dropout,regularization=regularization,model_name=model_name_vae,lr=lr, hidden_space=hidden_space,nmin=nmin,nmax=nmax,repeat_dense=repeat_dense)
model_vae.load_weights('serial_model_{0}.h5'.format(model_name_vae))
#plot_model(model_vae); plt.savefig('nn_arch_{0}'.format(model_name_vae), dpi=300)
print([(i,ii.name) for i, ii in enumerate(model_vae.layers)])
h_layer_ind = 16
input_dec = Input(shape=(2,)
layer_dec = model_vae.layers[17](input_dec)
for i_l in range(18,30):
    layer_dec = model_vae.layers[i_l](layer_dec)
decoder_vae = Model(input_dec, layer_dec)


'''

