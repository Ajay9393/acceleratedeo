import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
import os
import sys
import re
sys.path.append('/scratch/e/esargent/ajay15/')
import DataAnalysis as aj
from sklearn.model_selection import train_test_split
from skorch import NeuralNetClassifier, NeuralNet


class ConstantNet(torch.nn.Module):
    def __init__(self, D_in, D_out,act_fun = torch.nn.functional.sigmoid, dense_fun=torch.nn.functional.softmax):
        
        super(HalvedNet,self).__init__()
        
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        self.act_fun = act_fun
        self.dense_fun = dense_fun

        self.all_layers = []
        self.D_out = D_out
        self.D_in = D_in
        if D_out > 10:
            self.number_of_layers = int(-1*np.log2(D_out/D_in))
        else:
            self.number_of_layers = int(-1*np.log2(10/D_in))

        self.first_layer = torch.nn.Linear(int(D_in), int(D_in/2))

        for i in range(0,self.number_of_layers):
            self.all_layers.append(torch.nn.Linear(int(D_in/(2**(i+1))), int(D_in/(2**(i+2)))))

        self.second_last_layer = torch.nn.Linear(int(D_in/2**(self.number_of_layers+1)), int(D_out))

        if int(D_out) != 1:
            self.output_layer = torch.nn.Linear(int(D_out), int(D_out))



    def forward(self, x):
        x.to(self.device)
        h = self.act_fun(self.first_layer(x))
        h.to(self.device)
        for layer in self.all_layers:
            h = self.act_fun(layer(h.to(self.device)))
            h.to(self.device)

        if int(self.D_out)!=1:
            h = self.act_fun(self.second_last_layer(h.to(self.device))).to(self.device)
            y_pred = self.dense_fun(self.output_layer(h.to(self.device))).to(self.device)

        else:
            y_pred = self.dense_fun(self.second_last_layer(h.to(self.device)))
        
        return y_pred


