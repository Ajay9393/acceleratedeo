import tensorflow as tf
import keras
import numpy as np
from sklearn import metrics
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, Conv2D
from keras.callbacks import ModelCheckpoint
from keras.regularizers import l2
import sys
import argparse
from sklearn.model_selection import train_test_split
from keras.backend import int_shape
from keras.layers import Lambda,Dense, Dropout, Activation, Flatten, Input, Lambda, Conv2D, MaxPooling2D, Conv3D, MaxPooling3D,Reshape,UpSampling3D,Concatenate, Conv1D
from keras.models import Sequential, Model
from kopt import CompileFN, KMongoTrials, test_fn
from keras.optimizers import Adam
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
import h5py

#######################################################################################################
# Variables and Setup
#####################

# Data paths
data_path = '/scratch/e/esargent/ajay15/AcceleratedEO/Augmented_Data_With_Exp_Theta/features_augmented.npy'
target_path =  '/scratch/e/esargent/ajay15/AcceleratedEO/Augmented_Data_With_Exp_Theta/targets_augmented.npy'


# Model name
model_name= 'aj_test_model'

# Hyperparameters, currently unused as they are defined later
'''
lr = hp.loguniform("m_lr", np.log(1e-4), np.log(1e-2))
dense_activ = 'sigmoid'
activ = hp.choice("m_activ", ('relu','selu'))
add_reductive = True
filt_size = hp.choice("m_filt_size", ((2,2), (3,3), (4,4)))
epochs = 300
mode = 'train'
nstart = hp.choice("m_nstart", (5, 6))
nfilters = hp.choice("m_nfilters", (8, 16))
dropout = hp.choice("m_dropout", (0.0, 0.2, 0.4))
regularization = hp.choice("m_regularization", (1e-4, 1e-3, 1e-2))
loss = 'binary_crossentropy'
over_sample = False
patience = 10

if len(sys.argv) > 1:
    data_path = sys.argv[1]
    target_path = sys.argv[2]
    over_sample = eval(sys.argv[3])
    model_name = sys.argv[4]
    presorted = eval(sys.argv[5])

print('Model Name: ' + model_name)i
'''
def data(data_path, target_path):
    ##to one-hot-encode target vector
    target_vect=np.load(target_path)
    target_vect = target_vect.astype('int')
    targets = np.zeros((target_vect.size, 256))
    ##this encodes so that the first column (0) is the spacegroup1--> actual space group is +1
    targets[np.arange(target_vect.size), target_vect - 1] = 1
    features = np.load(data_path)
    maxes = np.max(features, axis = 1)
    features[maxes == 0,:] = 0
    maxes[maxes==0] = 1.
    features = features/maxes.reshape(features.shape[0],1)


    x_train, x_test, y_train, y_test = train_test_split(features, targets, test_size = 0.2 , random_state = 42)

    x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size = 0.2, random_state = 42)
    
    return (x_train.astype('int'),y_train.astype('int')), (x_test.astype('int'),y_test.astype('int')),(x_val.astype('int'),y_val.astype('int'))



def create_model(train_data, reduction_type,act_type,no_of_dense_layers,d_rate, regularization, lr, model_name):
    start = train_data[0].shape[1]
    end = 256
    model = Sequential()
    if reduction_type == 'gradual':
        diff = (start-end)/(no_of_dense_layers+1)
        for i in range(0, no_of_dense_layers):
            if i==0:
                model.add(Dense(int(start-diff*(i+1)), activation = act_type, input_shape = (start,)))
                model.add(Dropout(d_rate))
            else:
                model.add(Dense(int(start-diff*(i+1)), activation = act_type))
                model.add(Dropout(d_rate))
    elif reduction_type == 'halved':
        for i in range(0, no_of_dense_layers):
            if i==0:
                model.add(Dense(int(start/2**i), activation = act_type, input_shape = (start,)))
                model.add(Dropout(d_rate))
            else:
                if start/2**i < 255:
                    break
                else:
                    model.add(Dense(int(start/2**i), activation = act_type))
                    model.add(Dropout(d_rate))

    elif reduction_type == 'constant':
        for i in range(0,no_of_dense_layers):
            model.add(Dense(int(start), activation = act_type))
            model.add(Dropout(d_rate))

    model.add(Dense(end, activation = 'softmax'))
    model.compile(loss='categorical_crossentropy',
                  optimizer=Adam(lr=lr),
                  metrics=['accuracy'])

    return model



hyper_params = {
    "data":{"data_path":data_path,"target_path":target_path},
    
    "model":{
        "reduction_type":'gradual',
        "no_of_dense_layers": hp.choice('m_no_dense_layers',(2,4,6,8,10)),
        "act_type": 'relu',
        "d_rate": hp.choice('m_d_rate',(0.0,0.2,0.4)),
        "regularization": hp.choice('m_regularization',(0, 1e-4, 1e-3, 1e-2)),
	"lr": hp.loguniform("m_lr", np.log(1e-4), np.log(1e-2)),
        "model_name": model_name
        },

    "fit": {
        "epochs":200,
        "shuffle":True,
        "batch_size": hp.choice("m_batch_size", (4,8,16,32,64,128)),
        "patience":hp.choice('m_patience',(20,30,60))
	}
    }



Objective = CompileFN(db_name="1D_XRD_nn_fixed_Dense_normalized_temp", exp_name="1D_XRD_nn",  # experiment name
                      data_fn=data,
                      model_fn=create_model, 
                      loss_metric="accuracy", # which metric to optimize for
                      loss_metric_mode = 'max',
                      valid_split=.2, # use valid from the data function
                      #cv_b_folds=None,
                      save_model='best', # checkpoint the best model
                      save_results=True, # save the results as .json (in addition to mongoDB)
                      save_dir='.')  # place to store the models




trials = Trials()
best = fmin(Objective, hyper_params, trials = trials, algo = tpe.suggest, max_evals = 100)
