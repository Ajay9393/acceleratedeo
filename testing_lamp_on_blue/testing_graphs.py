import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append(r'../../../')
import DataAnalysis as aj
import re
from matplotlib import cm
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
from mpl_toolkits.axes_grid1.colorbar import colorbar
from matplotlib import gridspec


max_int = np.load('5 ms intIntensities.npy')
RGB = np.load('5 ms intColours.npy')

x_points = np.linspace(-10,10,int(np.sqrt(500)))
y_points = np.linspace(-10,10,int(np.sqrt(500)))

# max_int = np.zeros([10,10])
# max_int[0,:] = 10
# max_int[:,0] = 10
#
# RGB = np.zeros([10,10,3])
# RGB[0,:,:] = [255,255,255]
# RGB[:,0,:] = [255,255,255]
#
# x_points = np.arange(10)
# y_points = np.arange(10)

f, ax = plt.subplots(nrows = 2, ncols = 1, figsize = [10,20], sharex = True)
f.tight_layout()
f.subplots_adjust(hspace = 0.2, top = 0.9, bottom = 0.1, left = 0.1, right = 0.9)

a1 = ax[0]
a2 = ax[1]

im = a1.imshow(RGB, extent = [np.min(x_points), np.max(x_points), np.min(y_points), np.max(y_points)], origin = 'lower',aspect = 'auto')
contour = a2.contourf(x_points, y_points, max_int, cmap = 'plasma')
c1_divider = make_axes_locatable(a1)
ca1 = c1_divider.append_axes("right", size="7%", pad="4%",)
ca1.axis('off')
ax2_divider = make_axes_locatable(a2)
cax2 = ax2_divider.append_axes("right", size="7%", pad="4%",)
cb2 = colorbar(contour, cax = cax2)

a2.set_aspect(1)
a1.set_aspect(1)

aj.figure_quality_axes(a1, '', 'YPos (mm)', 'RBG',legend  = False)
aj.figure_quality_axes(a2, 'XPos (mm)', 'YPos (mm)', 'PL Intensity',legend  = False)
aj.figure_quality_axes(cb2.ax, '', 'Intensity (A.U.)', '',legend  = False)


f.savefig('5 ms intMapped.png', format = 'png', dpi= 600)
