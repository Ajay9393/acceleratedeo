struc = AseAtomsAdaptor.get_structure(mod_cell)
                vasp_input = MPRelaxSet(struc)
                vasp_input.user_incar_settings = {
                    'ALGO': 'All',
                    'EDIFF': 0.0015,
                    'ENCUT': 520,
                    'IBRION': 2,
                    'ICHARG': 1,
                    'ISIF': 3,  # 3 for relaxation
                    'ISMEAR': 0,
                    'ISPIN': 1,
                    'ISTART': 0,
                    'LREAL': 'Auto',
                    'LORBIT': 11,
                    'LVTOT': '.FALSE.',
                    'ADDGRID': '.FALSE.',
                    'LSORBIT': '.FALSE.',
                    'LHFCALC': '.FALSE.',  # for Relax TRUE for hybrid
                    'PREC': 'Accurate',
                    'NWRITE': 1,
                    'NPAR': 16,
                    'SIGMA': 0.05,
                    'NELMIN': 4,
                    'MAXMIX': 40,
                    'ISYM': 0,
                    'NSW': 200
                }
vasp_input.write_input(path)
os.system('cp KPOINTS {0}.'.format(path))
with open(path+'INCAR', 'r') as infile:
    lines = infile.readlines()
with open(path+'INCAR', 'w') as outfile:
    for line in lines:
        if not line.startswith('MAGMOM'):
            outfile.write(line)
