# -*- coding: utf-8 -*-
"""
Created on Tue Feb  5 11:11:30 2019

@author: Jeffrey
"""

import xml.etree.ElementTree as ET
from xml.dom import minidom
import numpy as np
import pandas as pd
from Precursor import Precursor

def set_stations(parent_node, station_list):
    deck_node = ET.SubElement(parent_node, 'deck')
    for i in range(len(station_list)):
        ET.SubElement(deck_node, 'station',{
                'id':str(i+1),
                'containerName':station_list[i]
                })

def set_protein_trays(parent_node,
                      volume_list=[-1,-1,-1,0,0,0,0,0,0,0,0,0,0,0,0]):
    protein_tray_node = ET.SubElement(parent_node, 'proteinTray')
    for i in range(len(volume_list)):
        ET.SubElement(protein_tray_node, 'column', {
                'index':str(i+1),
                'proteinVolume':str(volume_list[i])
                })


def aspirate_task(parent_node, head="LowVolumeTip", stationId="2",
                  columnIndex="1", repeat="False", repeatRange="1",
                  wellIndex="0", volume="450", deltaVolume="0",
                  reverseRepeatColumnOrder="False", smartAspirateIndex="0",
                  zAdjustment="0.5", liquidClass="High"):

    ET.SubElement(parent_node, 'aspirateTask', {
            'head':head,
            'stationId':stationId,
            'columnIndex':columnIndex,
            'repeat':repeat,
            'repeatRange':repeatRange,
            'wellIndex':wellIndex,
            'volume':volume,
            'deltaVolume':deltaVolume,
            'reverseRepeatColumnOrder':reverseRepeatColumnOrder,
            'smartAspirateIndex':smartAspirateIndex,
            'zAdjustment':zAdjustment,
            'liquidClass':liquidClass
            })

def dispense_task(parent_node, head="LowVolumeTip", stationId="2",
                  columnIndex="1", repeat="False", repeatRange="1",
                  wellIndex="0", volume="450", reverseRepeatColumnOrder="False",
                  mixingCycle="0", zAdjustment="0.5",
                  useFlexibleFinger="True", liquidClass="High"):

    ET.SubElement(parent_node, 'dispenseTask', {
            'head':head,
            'stationId':stationId,
            'columnIndex':columnIndex,
            'repeat':repeat,
            'repeatRange':repeatRange,
            'wellIndex':wellIndex,
            'volume':volume,
            'reverseRepeatColumnOrder':reverseRepeatColumnOrder,
            'mixingCycle':mixingCycle,
            'zAdjustment':zAdjustment,
            'useFlexibleFinger':useFlexibleFinger,
            'liquidClass':liquidClass
            })

def pick_tip(parent_node, head="LowVolumeTip", stationId="5",
             columnIndex="Auto", wellIndex="-1"):

    ET.SubElement(parent_node, 'pickTipTask', {
            'head':head,
            'stationId':stationId,
            'columnIndex':columnIndex,
            'wellIndex':wellIndex
            })

def eject_tip(parent_node, head="LowVolumeTip", stationId="6",
             columnIndex="0"):

    ET.SubElement(parent_node, 'ejectTipTask', {
            'head':head,
            'stationId':stationId,
            'columnIndex':columnIndex
            })

def set_properties(parent_node, ProbeLcpPlateBeforeDispense="False",
                   LCPDispensePlungerSpeed="10", LCPPredispenseVolume="0",
                   LCPPredispenseCycle="3", LCPSyringeType=""):
    properties_node = ET.SubElement(parent_node, 'properties')
    ET.SubElement(properties_node, 'LCPDispenseZOffset')
    ET.SubElement(properties_node, 'ProbeLcpPlateBeforeDispense',{'value':ProbeLcpPlateBeforeDispense})
    ET.SubElement(properties_node, 'LCPDispensePlungerSpeed',{'value':LCPDispensePlungerSpeed})
    ET.SubElement(properties_node, 'LCPPredispenseVolume',{'value':LCPPredispenseVolume})
    ET.SubElement(properties_node, 'LCPPredispenseCycle',{'value':LCPPredispenseCycle})
    ET.SubElement(properties_node, 'LCPSyringeType',{'value':LCPSyringeType})

def mix_and_dispense(parent_node, tasks, isGradient="False", trange="1"):
    #pick_tip(parent_node)
    rep_node = ET.SubElement(parent_node, 'transferRepetitiveTask', {
            'isGradient':isGradient,
            'range':trange
            })
    for i in range(len(tasks)-1):
        aspirate_task(rep_node,
                      stationId=str(tasks[i][0]),
                      columnIndex=str(tasks[i][1]),
                      volume=str(tasks[i][2]))

    dn = len(tasks)-1
    dispense_task(rep_node,
                  stationId=str(tasks[dn][0]),
                  columnIndex=str(tasks[dn][1]),
                  wellIndex=str(tasks[dn][2]),
                  volume=str(tasks[dn][3]))

    #eject_tip(parent_node)

# TODO find out how the robot handles more that 2 trays ID-wise
def generate_station_list():
    stations = ["96 3 ArtRobbins IntelliPlate LVR",
                "96 0 Greiner Assay Black",
                "Protein Tray"]

    return stations

def step_triplet_2(loc1=(2,1), loc2=(2,1), locd=(1,1), ratio=1, levels=[900,800,400]):
    # loc1 = (station, column) of first source
    # loc2 = (station, column) of second source
    # locd = (station, column) of dispensing location
    # ratio = vol1/vol2 (ratio=0 takes all the liquid from second source)
    # levels = volume levels for each drop (int)

    step_triplet = []
    for i in range(len(levels)):
        steps = []
        level1 = int(levels[i]*(ratio)/(ratio+1))

        if ratio != 0:
            steps.append( loc1+(level1,) )
        steps.append( loc2+(levels[i]-level1,) )
        steps.append( locd+(i+1, levels[i]) )

        step_triplet.append(steps)

    return step_triplet

def step_triplet(locs, locd, ratio, levels=[900,800,400]):
    # locs = [(station, column), ...] of each source
    # locd = (station, column) of dispensing location
    # ratio = ratio of liquid from each source
    # levels = volume levels for each drop (int)

    step_triplet = []
    for i in range(len(levels)):
        steps = []
        total_vol = 0
        for j in range(len(ratio)):
            if ratio[j] != 0:
                vol = int(round(levels[i]*ratio[j]/sum(ratio)))
                total_vol += vol
                steps.append( locs[j] + (vol,) )
        steps.append( locd+(i+1, total_vol) )

        step_triplet.append(steps)

    return step_triplet

def generate_steps(locs, ratios, drop_vols, station_loc_d=1):
    steps=[]

    for i in range(len(ratios)):
        steps += step_triplet(locs, (station_loc_d,i+1), ratios[i], drop_vols)
#        print (len(steps[i]))
    return steps

def generate_robot_xml(fname, stations, steps, tip_change=3):
    root = ET.Element('dispenseProject')
    set_stations(root, stations)
    set_protein_trays(root)
    tasks_node = ET.SubElement(root, 'tasks')

    for i in range(len(steps)):
        if i%tip_change == 0: pick_tip(tasks_node)
        mix_and_dispense(tasks_node, steps[i])
        if i%tip_change == tip_change-1: eject_tip(tasks_node)


    set_properties(root)

    xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent="   ")
    with open(fname, "w") as f:
        f.write(xmlstr)

def csv_generator(precursors, staggering, init_conc, init_solv,
                  ratios, ratio_legend, antisolvents, antisolvent_vols,
                  drop_vols, experiment_name):

    solvent_legend = np.asarray(['DMSO','DMF','GBL'])
    precursor_names = np.asarray(precursors)[:,0]
    
    # Generate Precursor objects for each precursor solution and solvent in order of the ratio legend
    ps = []

    ##This forloop creates num(stag)*num(starting solvents) precursor objects--> it only accounts for the INITIAL
    ##solvent ratio and then the concentration dictated by staggering

    ##the first half will be at concentration of staggering[0], the second half will be the initial concentrations
    ##multipled by staggering[1].

    ##If the starting precursor solution is only dissolved in one solvent, then the
    for stag in staggering:
        for ratio_name in ratio_legend:


            solv_idx = np.argwhere(solvent_legend==ratio_name)
            prec_idx = np.argwhere(precursor_names==ratio_name)

            if prec_idx.size > 0:

                pi = prec_idx[0,0]

                names = np.asarray(precursors[pi])[1:]
                print (names)
                init_conc_indv = np.asarray(init_conc[pi])
                init_solv_indv = np.asarray(init_solv[pi])
                ps.append(Precursor(names[0],names[1],names[2],names[3],
                                    init_conc_indv[0]*stag, init_conc_indv[1]*stag, init_conc_indv[2]*stag, init_conc_indv[3]*stag,
                                    init_solv_indv[0], init_solv_indv[1], init_solv_indv[2]))


            if solv_idx.size > 0:

                si = solv_idx[0,0]

                ps.append(Precursor('N/A','N/A','N/A','N/A',
                                    0, 0, 0, 0,
                                    int(ratio_name=='DMSO'), int(ratio_name=='DMF'), int(ratio_name=='GBL')))

    for p in ps:
        print ("\n")

    print('########################################' + '\n')
    pc_list=[]
    pc_list_stag=[]

    ##This for loop goes through each of the ratios by which the precursors are being diluted. it starts from a
    ##blank precursor, adds the amount of precursor solution, then dilutes it with the amount of solvent added. This
    ##only works for a len(staggering) == 2, because it adds ps[i+len(ratio)]--> this can be changed if necessary
    ## to allow for more ratios of dilution, but not really necessary right now.
    for ratio in ratios:
        precursor_temp = Precursor('N/A','N/A','N/A','N/A', 0, 0, 0, 0, 0, 0, 0)
        precursor_stag_temp = Precursor('N/A','N/A','N/A','N/A', 0, 0, 0, 0, 0, 0, 0)
        for i in range(len(ratio)):
            precursor_temp = Precursor.add(precursor_temp, ps[i].multiply(ratio[i]))
            precursor_stag_temp = Precursor.add(precursor_stag_temp, ps[i+len(ratio)].multiply(ratio[i]))
        pc_list.append(precursor_temp.normalize())
        pc_list_stag.append(precursor_stag_temp.normalize())

    for i,p in enumerate(pc_list):
        print (p.get_data().iloc[0])
        print ('\n')
        print (pc_list_stag[i].get_data().iloc[0])
        print ('\n' + '\n')


    print (len(pc_list))
    print(len (pc_list_stag))
    pc_df_list=[]
    for pc in pc_list:
        pc_df_list.append(pc.get_data())
    for pc in pc_list_stag:
        pc_df_list.append(pc.get_data())

    pc_df = pd.concat(pc_df_list*4, ignore_index=True)

    wellNum_df = pd.DataFrame({'Well Number': np.arange(1,97)})
    antisolvent_df = pd.DataFrame({
            'Antisolvent': np.asarray(antisolvents).repeat(12),
            'Antisolvent Volume (uL)': np.asarray(antisolvent_vols).repeat(12)})
    drop_vol_df = pd.DataFrame(np.tile(np.array(drop_vols),(96,1)), columns=['Drop 1 Volume (nL)', 'Drop 2 Volume (nL)', 'Drop 3 Volume (nL)'])

    df = pd.concat([wellNum_df, pc_df, antisolvent_df, drop_vol_df],axis=1)

    df.to_csv(experiment_name+'.csv', encoding='utf-8', index=False)

    return df

if __name__ == "__main__":

    name = '20200108-3PLAinDMF'

    ##List in order of "precursor solution, A-Ligand, A-site, B-site, X-site

    precursors =  [('Sol3', '3-PLA','N/A','Pb', 'Cl')]

    ##Controls concentration variance--> i.e to get 24 well repitition (two rows of identical AS), you would input
    ##12 ratios, and two concentrations. there are then 12 different solvent ratios and two concetrations for each ratio
    ##The different ratios will ALSO give different concentrations, but then each of those will be divided in 2

    ##It is currently hard-coded to 2
    staggering =  [1., 0.5]

    ##Initial concentration of A-ligand, A-site, B-site and X-site
    init_conc = [(2, 0, 1, 4)]

    init_solv = [(0, 1, 0)]

    ##For columns A-->H
    antisolvents = ['IPA', 'IPA', 'EtOH','EtOH','CB', 'CB', 'Tol','Tol']
    antisolvent_vols = np.array([40]).repeat(8)

    ##Location of solution in robot tray--> 2 is fixed (place that the tray is), second ratio is which column
    ##the solvent is in--> in order of ratio_legend
    locs = [(2,4),(2,1),(2,2)]
    ratio_legend = ['Sol3','DMF', 'DMSO']

    ##Different ratios of solvent
    ratios = [[1,0,0], [1,0,1], [1,0,4],
              [1,1,0], [4,2,1], [1,4,0],
              [2,1,0], [4,1,2], [1,3,1],
              [1,1,1], [2,1,1], [1,2,2,]]

#    ##For 3 diff concentrations--> doesn't work right now
#    ratios = [[1,0,0], [1,0,1], [1,0,4],
#              [1,1,0], [4,2,1], [1,4,0],
#              [2,1,0], [4,1,2],]

    drop_vols = [900,800,400]
    steps = generate_steps(locs, ratios, drop_vols)

    stations = generate_station_list()
    generate_robot_xml(name+".xml", stations, steps)
    csv_generator(precursors, staggering, init_conc, init_solv, ratios, ratio_legend, antisolvents, antisolvent_vols, drop_vols, name)
