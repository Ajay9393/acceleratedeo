# Data obtained from materials project using the following filter
# data = a.query(criteria={'elements':{'$in': ['Sr','Ca','Sn','Ge','Pb','Cd','Ni','Fe','Cu']}},properties=['structure','e_above_hull','band_gap','final_energy']) 
import numpy as np
import pandas as pd
from collections import Counter
import torch
from torch_geometric.data import Data, DataLoader
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch_geometric
import random
import matplotlib.pyplot as plt
from torch_geometric.nn.inits import reset 
from torch_geometric.nn.conv import MessagePassing
from pymatgen.io.ase import *
import warnings
from sklearn.metrics import *
warnings.simplefilter('error',RuntimeWarning)

df = pd.read_pickle('hull_energies.pkl')

props = pd.read_pickle('props.pkl')
# one-hot encoding the column by the name 'Column'
props = pd.get_dummies(props,columns=['Column'])

def mapper(var):
    return props.loc[var].values.astype('float')
    
#del x[[a.index for a in x if a.symbol in [H']]]

# List of grpahs representing each crystal structure 
base_graphs = []  

# Cut off radius to consider neighbors - in Angstroms(A) 
cut_off = 4 

base_graphs = []  
for i in range(0,len(df)): 
    
    try:
        obj = df.loc[i,'atoms']  
        del obj[[a.index for a in obj if a.symbol in ['H']]]
        
        # Finding all the neighbors within cut off radius
        edge_idx = np.argwhere(obj.get_all_distances() < cut_off)
        # Getting rid of self edges
        edge_idx = np.array([edge_idx[i] for i in range(len(edge_idx)) if edge_idx[i,0] != edge_idx[i,1]])
        
        # Edge features are reciprocal of the distance between the atoms
        edge_feats = (1. / obj.get_all_distances()[tuple(edge_idx.T)]).reshape(-1,1)
        edge_index = torch.tensor(np.transpose(edge_idx), dtype=torch.long)
        edge_attr = torch.tensor(edge_feats, dtype=torch.float)
        nodal_feats = np.array(list(map(mapper,obj.get_chemical_symbols())))
        x = torch.tensor(nodal_feats, dtype=torch.float)   
        
        base_graphs.append(Data(x=x, edge_index=edge_index, y = df.loc[i,'final_energy'], edge_attr=edge_attr))
        
    except:
        pass



class Net(torch.nn.Module):   

    def __init__(self):   
        super(Net, self).__init__()   
        self.gconv1 = torch_geometric.nn.EdgeConv(torch.nn.Sequential(torch.nn.Linear(2*64,32),torch.nn.ReLU()))
        self.gconv2 = torch_geometric.nn.EdgeConv(torch.nn.Sequential(torch.nn.Linear(2*32,32),torch.nn.ReLU()))
        self.gconv3 = torch_geometric.nn.EdgeConv(torch.nn.Sequential(torch.nn.Linear(2*32,32),torch.nn.ReLU()))    
        self.gconv4 = torch_geometric.nn.EdgeConv(torch.nn.Sequential(torch.nn.Linear(2*32,32),torch.nn.ReLU()))    
        self.nn1 = torch.nn.Linear(32,16)
        self.nn2 = torch.nn.Linear(16,1)

    def forward(self,data):   
        x, edge_index, graph_assignment = data.x, data.edge_index, data.batch   
        out1 = self.gconv1(x,edge_index)   
        out1 = self.gconv2(out1,edge_index)   
        out1 = self.gconv3(out1,edge_index)   
        out1 = self.gconv4(out1,edge_index)   

        out1 = torch_geometric.nn.global_add_pool(out1,graph_assignment)   
        out = self.nn1(out1)
        out = self.nn2(F.relu(out))
        return out 



device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')   
loader = DataLoader(base_graphs[0:40000], shuffle=True, batch_size=32)  
val_loader = DataLoader(base_graphs[40000:], shuffle=False, batch_size=32) 
model = Net().to(device) 
criterion = torch.nn.BCELoss() 
optimizer = optim.Adam(model.parameters(),lr=0.001) 
loss_train = [] 
loss_val = [] 
for epoch in range(100): 
    for batch in loader: 
        batch = batch.to(device) 
        optimizer.zero_grad() 
        out = model(batch) 
        #print(out[0]) 
        loss = criterion(out,batch.y.reshape((-1,1))) 
        loss.backward() 
        optimizer.step() 
    
    tmp_train_loss = []
    for batch in loader: 
        batch = batch.to(device) 
        tmp_train_loss.append(float(criterion(model(batch),batch.y.reshape((-1,1))).data)) 
    loss_train.append(np.average(tmp_train_loss))
    
    tmp_val_loss = []
    for batch in val_loader: 
        batch = batch.to(device) 
        tmp_val_loss.append(float(criterion(model(batch),batch.y.reshape((-1,1))).data)) 
    loss_val.append(np.average(tmp_val_loss))
    
    print('Epoch:', epoch, '; Loss:', loss_train[-1], ': Val loss:', loss_val[-1]) 

