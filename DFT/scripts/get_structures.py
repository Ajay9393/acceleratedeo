# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 11:09:56 2021

@author: ak4jo
"""


from ase import Atoms
from ase.visualize import view
from ase.io import write
import json
import numpy as np
import os
import re

def get_atoms_object(filename):
    with open(filename, 'rb') as f:
        data = json.load(f)
        
    elements = data['PC_Compounds'][0]['atoms']['element']
    
    x = np.array(data['PC_Compounds'][0]['coords'][0]['conformers'][0]['x']).T
    y = np.array(data['PC_Compounds'][0]['coords'][0]['conformers'][0]['y']).T
    z = np.array(data['PC_Compounds'][0]['coords'][0]['conformers'][0]['z']).T
    
    a = Atoms(elements, positions = np.array([x,y,z]).T)
    
    write(filename[0:-5] +'.png', a)
    a = Atoms(elements, positions = np.array([x,y,z]).T, cell = [10,10,10])
    return a
    
    
files = [x for x in os.listdir('../') if re.findall('.json',x)]

atoms = []

for file in files:
    atoms.append(get_atoms_object('../'+ file))