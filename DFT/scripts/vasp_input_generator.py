from pymatgen.io.vasp.sets import MPRelaxSet
from ase.io import read
from pymatgen.io.ase import AseAtomsAdaptor as AAA
import re
import os
import numpy as np
from ase import Atoms
import json


def get_atoms_object(filename):
    with open(filename, 'rb') as f:
        data = json.load(f)
        
    elements = data['PC_Compounds'][0]['atoms']['element']
    
    x = np.array(data['PC_Compounds'][0]['coords'][0]['conformers'][0]['x']).T
    y = np.array(data['PC_Compounds'][0]['coords'][0]['conformers'][0]['y']).T
    z = np.array(data['PC_Compounds'][0]['coords'][0]['conformers'][0]['z']).T
    
    a = Atoms(elements, positions = np.array([x,y,z]).T, cell = [10,10,10])
    return a
    
    
files = [x for x in os.listdir('../ligands_2/') if re.findall('.json',x)]



for file in files:
    a = get_atoms_object('../ligands_2/'+ file)
    
    
    mp = MPRelaxSet(AAA.get_structure(a),user_incar_settings={'ALGO':'Fast','NPAR':4, 'ISMEAR':0, 'NSW':199})
    mp.write_input('../VASP_inputs/'+ file[0:-5])
