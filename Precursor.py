# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 14:21:43 2019

@author: Jeffrey
"""
import numpy as np
import pandas as pd
import pickle
import re
import os.path

class Precursor: 
    def __init__(self, AL, A, B, X, AL_conc, A_conc, B_conc, X_conc, DMSO, DMF, GBL):
        self.AL = np.array(AL,ndmin=1)
        self.A = np.array(A,ndmin=1)
        self.B = np.array(B,ndmin=1)
        self.X = np.array(X,ndmin=1)
        self.AL_conc = np.array(AL_conc,ndmin=1,dtype=np.float64)
        self.A_conc = np.array(A_conc,ndmin=1,dtype=np.float64)
        self.B_conc = np.array(B_conc,ndmin=1,dtype=np.float64)
        self.X_conc = np.array(X_conc,ndmin=1,dtype=np.float64)
        self.DMSO = DMSO
        self.DMF = DMF
        self.GBL = GBL
    
    def from_df(df):
        return Precursor(df.loc[0,'A-Ligand'].tolist(), df.loc[0,'A'].tolist(), df.loc[0,'B'].tolist(), df.loc[0,'X'].tolist(),
                         df.loc[0,'Concentration A-Ligand (M)'].tolist(), df.loc[0,'Concentration A (M)'].tolist(),
                         df.loc[0,'Concentration B (M)'].tolist(), df.loc[0,'Concentration X (M)'].tolist(),
                         df.loc[0,'Ratio DMSO'].tolist(), df.loc[0,'Ratio DMF'].tolist(), df.loc[0,'Ratio GBL'].tolist())
    
    def add_element(E1, E2, E1_c, E2_c):
        for i in range(E2.size):
            E_idx = np.argwhere(E1 == E2[i])
            if E_idx.size == 0:
                if np.any(E1_c == 0) and np.any(E2_c != 0):
                    E1 = E2[i]
                    E1_c = E2_c[i]
                elif np.any(E1_c != 0) and np.any(E2_c != 0):
                    E1 = np.append(E1, E2[i])
                    E1_c = np.append(E1_c, E2_c[i])
            else:
                E1_c[E_idx[0]] += E2_c[i]
        return (E1, E1_c)
    
    def add(P1, P2):
        (AL, AL_conc) = Precursor.add_element(P1.AL, P2.AL, P1.AL_conc, P2.AL_conc)
        (A, A_conc) = Precursor.add_element(P1.A, P2.A, P1.A_conc, P2.A_conc)
        (B, B_conc) = Precursor.add_element(P1.B, P2.B, P1.B_conc, P2.B_conc)
        (X, X_conc) = Precursor.add_element(P1.X, P2.X, P1.X_conc, P2.X_conc)
        DMSO = P1.DMSO + P2.DMSO
        DMF = P1.DMF + P2.DMF
        GBL = P1.GBL + P2.GBL
        return Precursor(AL, A, B, X, AL_conc, A_conc, B_conc, X_conc, DMSO, DMF, GBL)
        
    def normalize(self):
        total_solvent = self.DMSO + self.DMF + self.GBL
        AL_conc = self.AL_conc / total_solvent
        A_conc = self.A_conc / total_solvent
        B_conc = self.B_conc / total_solvent
        X_conc = self.X_conc / total_solvent
        DMSO = self.DMSO / total_solvent
        DMF = self.DMF / total_solvent
        GBL = self.GBL / total_solvent
        return Precursor(self.AL, self.A, self.B, self.X, AL_conc, A_conc, B_conc, X_conc, DMSO, DMF, GBL)
    
    def multiply(self, scalar):
        AL_conc = self.AL_conc * scalar
        A_conc = self.A_conc * scalar
        B_conc = self.B_conc * scalar
        X_conc = self.X_conc * scalar
        DMSO = self.DMSO * scalar
        DMF = self.DMF * scalar
        GBL = self.GBL * scalar
        return Precursor(self.AL, self.A, self.B, self.X, AL_conc, A_conc, B_conc, X_conc, DMSO, DMF, GBL)
    
    def get_data(self):
        return pd.DataFrame({
            'A-Ligand': [self.AL],
            'A': [self.A],
            'B': [self.B],
            'X': [self.X],
            'Concentration A-Ligand (M)': [self.AL_conc],
            'Concentration A (M)': [self.A_conc],
            'Concentration B (M)': [self.B_conc],
            'Concentration X (M)': [self.X_conc],
            'Ratio DMSO': self.DMSO,
            'Ratio DMF': self.DMF,
            'Ratio GBL': self.GBL
            })

if __name__ == "__main__":
    test1 = Precursor('PEA', 'Cs', 'Sn', 'I', 2, 1, 1, 4, 0, 1, 0)
    test2 = Precursor('PEA', 'N/A', 'Pb', 'I', 2, 0, 1, 4, 1, 0, 0)
    test3=Precursor.add(test1,test2)
    d1 = test3.get_data()
    test4=test3.normalize()
    d2 = test4.get_data()