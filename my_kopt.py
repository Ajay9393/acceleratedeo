from __future__ import print_function
import numpy as np
import pandas as pd
from keras.datasets import imdb
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.optimizers import Adam
# kopt and hyoperot imports
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_validate
from keras.layers import Dense, Conv2D, Flatten, Dropout, BatchNormalization
import matplotlib.pyplot as plt
import keras.callbacks
import logging
import pickle
import time
import random
from kopt import CompileFN, KMongoTrials, test_fn

import os
#os.environ["CUDA_VISIBLE_DEVICES"] = "3"
import time
import pandas as pd
from sklearn.model_selection import train_test_split 
from sklearn.metrics import mean_absolute_error,mean_squared_error
import tensorflow as tf
from itertools import product
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler,StandardScaler
import h5py
import pickle
from collections import Counter
#from ednn_routines import *
from itertools import product
import keras
import numpy as np
import tensorflow as tf
from keras.models import Sequential, Model
from keras.utils import plot_model
#from keras.layers import Lambda,Dense, Dropout, Activation, Flatten, Input, Merge, Lambda, Conv2D, MaxPooling2D, Conv3D, MaxPooling3D
from keras.layers import Lambda,Dense, Dropout, Activation, Flatten, Input, Lambda, Conv2D, MaxPooling2D, Conv3D, MaxPooling3D,Reshape,UpSampling3D,Concatenate, Conv1D
from keras.callbacks import ModelCheckpoint
from keras.regularizers import l2
from keras.utils import multi_gpu_model
from sklearn.decomposition import PCA
from keras import backend as K

import pandas as pd
import numpy as np
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_validate
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Flatten, Conv1D, MaxPooling1D, GlobalAveragePooling1D
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Flatten, concatenate
import keras.callbacks
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import KFold
from keras.backend import int_shape
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler

#function to calculate r^2 error
def coeff_determination(y_true, y_pred):
    from keras import backend as K
    SS_res =  K.sum(K.square( y_true-y_pred ))
    SS_tot = K.sum(K.square( y_true - K.mean(y_true) ) )
    return ( 1 - SS_res/(SS_tot + K.epsilon()) )

X1 = pd.read_pickle('inputs.gzip')
X2 = pd.read_pickle('corrected_rdf.gzip')
X4 = pd.read_pickle('relaxed_rdf.gzip')
y1 = pd.read_pickle('single_deepness_linear.gzip')
y2 = pd.read_pickle('final_dos.gzip')
X2 = pd.read_pickle('corrected_rdf.gzip')
X6 = pd.read_pickle('composite_rdf.gzip')
X7 = pd.read_pickle('prdf.gzip')

y3 = pd.read_pickle('fe_values.gzip')
indices = X7.index.values

X1 = X1.values
y1 = y1.values
X2 = X2.values
y2 = y2.values
X4 = X4.values
X6 = X6.values
X7 = X7.values
y3 = y3.values



X1 = X1[:,0]
X2 = X2[:,0]
X6 = X6[:,0]
X4 = X4[:,0]

y2 = y2[:,2]


idx = np.argwhere(y1>=0)
idx = idx[:,0]

idx2 = np.argwhere(y3[:,1]==True)
idx2 = idx2[:,0]

idx = np.intersect1d(idx,idx2)

data_X1 = X1[idx]
data_X2 = X2[idx]
data_X4 = X4[idx]
data_X6 = X6[idx]
data_X7 = X7[idx]

data_indices = indices[idx]

#Creating compatible input for pRDF as a 2D matrix
data_X3 = []
for i in range(0,len(data_X2)):
		tmp = data_X2[i]
		tmp = np.reshape(tmp,(50,13))
		data_X3.append(tmp)
data_X3 = np.array(data_X3)


data_X5 = []
for i in range(0,len(data_X4)):
		tmp = data_X4[i]
		tmp = np.reshape(tmp,(50,13))
		data_X5.append(tmp)
data_X5 = np.array(data_X5)

data_y1 = y1[idx]
data_y2 = y2[idx]
data_y2 = np.array(list(data_y2))

data_X1 = np.array(list(data_X1))
data_X2 = np.array(list(data_X2))
data_X4 = np.array(list(data_X4))
data_X6 = np.array(list(data_X6))

data_X7 = np.array(list(data_X7)) #Complete pRDF - all pairwise included

data_X6 = data_X6[:,0,:]

#needed for voronoi representation and not for pRDF
data_X1 = data_X1[:,0,:]

print('Shape of x1: ', data_X1.shape)
print('Shape of x2: ', data_X2.shape)
print('Shape of y1: ', data_y1.shape)
print('Shape of y2: ', data_y2.shape)

train_X,test_X,v_train,v_test,train_y,test_y = train_test_split(data_X7,data_X1,data_y2,test_size=0.2,random_state=20)

def data(pca_size,mix):
	#mix=0: No voronoi, mix=1: Voronoi padded, mix=2: Voronoi PCAed to 60 and padded
	
	pca = PCA(n_components=pca_size)
	train_X_n = pca.fit_transform(train_X)
	test_X_n = pca.transform(test_X)
	
	pca_v = PCA(n_components=60)
	v_train_n = pca_v.fit_transform(v_train)
	v_test_n = pca_v.transform(v_test)
	
	if mix==1:
		train_X_n = np.concatenate((train_X_n,v_train),axis=1)
		test_X_n = np.concatenate((test_X_n,v_test),axis=1)
	elif mix==2:
		train_X_n = np.concatenate((train_X_n,v_train_n),axis=1)
		test_X_n = np.concatenate((test_X_n,v_test_n),axis=1)
	
	return (train_X_n,train_y),(test_X_n,test_y)

def my_model(train_data,ks1,ks2,no_of_dense_layers,d_rate,regularization,model_name):
#def my_model(train_data,no_of_dense_layers,d_rate,regularization,model_name):
	start = train_data[0].shape[1]
	end = 60
	diff = (start-end)/(no_of_dense_layers+1)
	
	model = Sequential()
	
	for i in range(0,no_of_dense_layers):
		if i==0:
			model.add(Dense(int(start-diff*(i+1)),activation='relu',input_shape=(start,)))
			model.add(Dropout(d_rate))
		else:
			model.add(Dense(int(start-diff*(i+1)),activation='relu'))
			model.add(Dropout(d_rate))
	
	model.add(Dense(60,activation='relu'))
	
	model.compile(optimizer='rmsprop', loss='mse')
	
	return model

#ks1 = 3
#ks2 = 3
#no_of_dense_layers = 3
#d_rate = 0
#model_name = 'defects_nn'

#keras_model = my_model(ks1=ks1,ks2=ks2,no_of_dense_layers=no_of_dense_layers,d_rate = d_rate,model_name=model_name)
	
hyper_params = {
    "data":{"pca_size": hp.choice('d_pca_size',(100,300,600,900)),"mix": hp.choice('d_mix',(0,1,2))},
    "model": {
        "ks1": hp.choice("m_ks1", (10,7,4)),
        "ks2": hp.choice("m_ks2", (9,5,3)),
        "no_of_dense_layers": hp.choice("m_no_of_dense_layers", (1,2,3,4,5,6)),
        "d_rate": hp.choice("m_d_rate", (0.0, 0.2,0.4)),
		"regularization": hp.choice("m_regularization",(0,1e-4,1e-3,1e-2)),
        "model_name": "my_test_model",
        },
    "fit": {
        "epochs": 200,
		"shuffle": True,
		"batch_size": hp.choice("m_batch_size",(4,8,16,32,64,128))	
		}
}


objective = CompileFN(db_name="comp_prdf_nn", exp_name="comp_prdf_model_search",  # experiment name
                      data_fn=data,
                      model_fn=my_model, 
                      add_eval_metrics=["mse"], # metrics from concise.eval_metrics, you can also use your own
                      optim_metric="mse", # which metric to optimize for
                      optim_metric_mode="min", # maximum should be searched for
                      valid_split=None, # use valid from the data function
                      #cv_b_folds=None,
                      save_model='best', # checkpoint the best model
                      save_results=True, # save the results as .json (in addition to mongoDB)
                      save_dir='/scratch/e/esargent/mxyptlkr/defect_calcs/misha_defects_data/vasprun_data')  # place to store the models
#data = get_data(objective.data_fn, hyper_params)
#test_fn(objective, hyper_params)
trials = Trials()
best = fmin(objective, hyper_params, trials=trials, algo=tpe.suggest, max_evals=100)

